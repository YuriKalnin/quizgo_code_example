<?php
Route::get('service/rate', 'Rate\MainController@get');
Route::post('service/rate/change', 'Rate\MainController@change');
Route::post('service/rate/coupon/make', 'Rate\CouponController@make');

Route::get('service/pay/methods/get', 'Pay\MethodsController@get');
Route::any('service/pay/methods/bill/create', 'Pay\MethodsController@createBill');
