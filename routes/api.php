<?php
Route::post('services/quiz/{quiz}/move/to/company', 'Quiz\QuizController@moveToCompany');
Route::post('services/quiz/{quiz}/move/to/project', 'Quiz\QuizController@moveToProject');

Route::resource('services/crm/lead', 'Crm\ResourceController', [
    'only' => ['index', 'show', 'destroy', 'update']
]);

Route::resource('services/crm/lead/{lead}/comment', 'Crm\Comment\ResourceController', [
    'only' => ['index', 'destroy', 'store', 'update']
]);
