#!/bin/bash

date >> /var/www/storage/app/tmp/stat-mem.txt
printf "@date" >> /var/www/storage/app/tmp/stat-mem.txt

free -m >> /var/www/storage/app/tmp/stat-mem.txt
printf "@free" >> /var/www/storage/app/tmp/stat-mem.txt

printf "\n\n" >> /var/www/storage/app/tmp/stat-mem.txt
