#!/bin/bash

cp /var/www/storage/quiz-setup-app/public/.htaccess /var/www/public/.htaccess

chmod 600 /etc/crontab
service cron start
redis-server /etc/redis/redis.conf --daemonize yes
service fail2ban start

rm /etc/nginx/sites-enabled/default

ln -s /etc/nginx/sites-available/quiz-domains.ssl.conf /etc/nginx/sites-enabled/quiz-domains.ssl.conf
ln -s /etc/nginx/sites-available/quizgo.me.ssl.conf /etc/nginx/sites-enabled/quizgo.me.ssl.conf
ln -s /etc/nginx/sites-available/quizgo.ru.ssl.conf /etc/nginx/sites-enabled/quizgo.ru.ssl.conf
ln -s /etc/nginx/sites-available/default.conf /etc/nginx/sites-enabled/default.conf

service nginx start
service php7.4-fpm start

git config --global user.email "kalnin.yuri@gmail.com"
git config --global user.name "Kalnin Yuri"

cd /var/www && ln -s ../storage/app/public/upload ./public/upload

php -r "file_exists('/var/www/resources/assets/dashboard/env.app.js')                   || copy('/var/www/storage/quiz-setup-app/dashboard/env.app.js', '/var/www/resources/assets/dashboard/env.app.js');"
php -r "file_exists('/var/www/resources/assets/dashboard/tpl.env.app.js')               || copy('/var/www/storage/quiz-setup-app/dashboard/tpl.env.app.js', '/var/www/resources/assets/dashboard/tpl.env.app.js');"
php -r "file_exists('/var/www/resources/assets/quizwidget_v2/build.conf.js')            || copy('/var/www/storage/quiz-setup-app/quizwidget_v2/build.conf.js', '/var/www/resources/assets/quizwidget_v2/build.conf.js');"
php -r "file_exists('/var/www/resources/assets/quizwidget_v2/app/widget/env.widget.js') || copy('/var/www/storage/quiz-setup-app/quizwidget_v2/widget/env.widget.js', '/var/www/resources/assets/quizwidget_v2/app/widget/env.widget.js');"
php -r "file_exists('/var/www/resources/assets/clips-widget/clips.conf.js')             || copy('/var/www/storage/quiz-setup-app/clips-widget/clips.conf.js', '/var/www/resources/assets/clips-widget/clips.conf.js');"
php -r "is_dir('/var/www/storage/logs/supervisor')  || mkdir('/var/www/storage/logs/supervisor');"
php -r "is_dir('/var/www/storage/app/tmp')          || mkdir('/var/www/storage/app/tmp');"
php -r "is_dir('/var/www/storage/app/tmp/clerking') || mkdir('/var/www/storage/app/tmp/clerking');"
php -r "is_dir('/var/www/storage/app/tmp/user-segments') || mkdir('/var/www/storage/app/tmp/user-segments');"
php -r "is_dir('/var/www/storage/app/tmp/virtual-hosts') || mkdir('/var/www/storage/app/tmp/virtual-hosts');"
php -r "is_dir('/var/www/storage/framework/views')  || mkdir('/var/www/storage/framework/views');"
php -r "is_dir('/var/www/storage/amo')              || mkdir('/var/www/storage/amo');"
php -r "is_dir('/var/www/storage/app/backup-temp')  || mkdir('/var/www/storage/app/backup-temp');"
php -r "is_dir('/var/www/storage/app/errors')       || mkdir('/var/www/storage/app/errors');"
php -r "is_dir('/var/www/storage/app/public/upload')|| mkdir('/var/www/storage/app/public/upload');"
php -r "is_dir('/var/www/storage/app/test')         || mkdir('/var/www/storage/app/test');"

rm /var/www/storage/app/tmp/asset.version.txt
echo $(date +"%s") >> /var/www/storage/app/tmp/asset.version.txt
chown quizgo:quizgo /var/www/storage/app/tmp/asset.version.txt

#cd /var/www && composer install

#php /var/www/artisan up:laravel-passport
#php /var/www/artisan up:install-node
#php /var/www/artisan quiz:deploy

#rm /var/www/html/index.html
#rmdir /var/www/html

#chown quizgo:quizgo -R /var/www

service nginx stop
service php7.4-fpm stop
service redis-server stop

/usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf
