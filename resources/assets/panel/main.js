import 'regenerator-runtime';
import '@src/js/modules/object.assign';
import '@src/js/modules/array.insert';

import jQuery from 'jquery';
import Popper from 'popper.js';
import 'bootstrap';
import 'jquery.cookie';

import intlTelInput from 'intl-tel-input';
import 'intl-tel-input/build/js/utils';
import 'intl-tel-input/build/css/intlTelInput.css';

import './js/old/vendor/ldb/plugins/bootstrap-notify';
import './scss/app.scss';

import '@modules/vue/config';
import '@modules/vue/plugins';
import '@modules/vue/design.system';
import '@modules/vue/components';

import CreateApp from '@modules/vue/create.app';

window.Popper = Popper;
window.$ = jQuery;
window.jQuery = window.$;
window.intlTelInput = intlTelInput;

window.appMarket = CreateApp();

console.log('build 912323');
