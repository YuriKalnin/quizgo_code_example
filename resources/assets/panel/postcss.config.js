module.exports = {
    plugins: {
        'postcss-preset-env': {
            browsers: 'last 7 versions',
        },
    },
}
