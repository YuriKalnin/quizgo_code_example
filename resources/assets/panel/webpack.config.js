const path = require('path');
const webpack = require('webpack');
const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const MergeIntoSingleFilePlugin = require('webpack-merge-and-include-globally');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const mode = process.env.NODE_ENV === 'production' ? 'production' : 'development';
const runServer = process.env.NODE_SERVER === 'run';
const modeWatch = process.env.NODE_MODE === 'watch';
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const EventHooksPlugin = require('event-hooks-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const UglifyJs = require('uglify-js');
const fs = require('fs-extra');

let config = {};

config = {
    mode,
    target: ['web', 'es5'],
    entry: './panel/main.js',
    resolve: {
        extensions: ['.js', '.vue'],
        alias: {
            vue: 'vue/dist/vue.js',
            '@src': __dirname,
            '@design': path.join(__dirname, 'js/components/design-system'),
            '@pages': path.join(__dirname, 'js/components/pages'),
            '@modules': path.join(__dirname, 'js/modules'),
        },
    },
    output: {
        path: path.resolve(__dirname, '../../../public/assets/panel-dist'),
        filename: 'main.js',
        chunkFilename: '[name].[chunkhash].js',
        publicPath: mode === 'production' ? '//cdn-cloud.quizgo.ru/assets/panel/' : '/assets/panel/',
        // publicPath: mode === 'production' ? '/assets/panel/' : '/assets/panel/',
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.(png|gif|jpg)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.json/,
                type: 'asset/resource',
                include: [
                    path.resolve(__dirname, './http'),
                ],
                generator: {
                    filename: '[path][name][ext]?v=[hash]&[query]',
                },
            },
            {
                test: /\.tpl\.less/,
                type: 'asset/source',
            },
            {
                test: /\.(html)$/,
                include: [
                    path.resolve(__dirname, 'js/components/pages/frontend/design-system/examples'),
                ],
                use: 'raw-loader',
            },
            {
                test: /\.(woff(2)?|eot|ttf|otf|svg)$/,
                type: 'asset/resource',
            },
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        babelrc: true,
                    },
                },
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin({
            dangerouslyAllowCleanPatternsOutsideProject: true,
        }),
        new VueLoaderPlugin(),
        new webpack.DefinePlugin({
            NODE_SERVER: JSON.stringify(process.env.NODE_SERVER),
        }),
        new webpack.ProvidePlugin({
            Promise: 'es6-promise-promise',
        }),
        new ESLintPlugin({}),
    ],
};

if (modeWatch) {
    config.output = {
        path: path.resolve(__dirname, '../../../public/assets/panel'),
        filename: 'main.js',
    };
}

if (runServer) {
    config.devtool = 'inline-source-map';
    config.output = {
        path: path.resolve(__dirname, '../dist/panel'),
        filename: 'js/main.js',
    };

    config.module.rules.push({
        test: /\.(scss|css)$/,
        use: ['vue-style-loader', 'css-loader', 'postcss-loader', {
            loader: 'sass-loader',
            options: {
                additionalData: '@import "./panel/scss/design-system/variables.scss";',
            },
        }],
    });

    config.plugins.push(new webpack.HotModuleReplacementPlugin());

    config.plugins.push(new HtmlWebpackPlugin({
        template: path.resolve(__dirname, './template.html'),
        filename: 'index.html',
        scriptLoading: 'blocking',
        title: 'QuizGo frontend builder',
        inject: false,
    }));

    config.devServer = {
        static: './',
        client: {
            reconnect: true,
        },
        liveReload: false,
        allowedHosts: 'all',
        hot: true,
        port: 8080,
        historyApiFallback: true,
        proxy: [
            {
                context: ['**', '/'],
                bypass(req) {
                    console.log('bypass', {
                        m: req.method,
                        u: req.url,
                    });
                    const { url } = req;
                    req.method = 'GET';
                    return url;
                },
            },
        ],
    };
} else {
    config.cache = {
        type: 'filesystem',
    };

    config.module.rules.push({
        test: /\.(scss|css)$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', {
            loader: 'sass-loader',
            options: {
                additionalData: '@import "./panel/scss/design-system/variables.scss";',
            },
        }],
    });

    config.plugins.push(new MiniCssExtractPlugin());

    if (!modeWatch) {
        config.devtool = 'source-map';
        config.plugins.push(new EventHooksPlugin({
            done: (compilation, done) => {
                if (mode === 'production') {
                    fs.remove(path.resolve(__dirname, '../../../public/assets/panel')).then(() => {
                        console.log('OLD BUILD CLEAN SUCCESS');
                        fs.copy(path.resolve(__dirname, '../../../public/assets/panel-dist'), path.resolve(__dirname, '../../../public/assets/panel'), done)
                            .then(() => {
                                console.log('NEW BUILD SUCCESS COPY');
                            });
                    });

                    fs.remove(path.resolve(__dirname, '../../../public/assets/dashboard/build/img')).then(() => {
                        console.log('IMAGE FOLDER SUCCESS CLEAN');
                        fs.copy(path.resolve(__dirname, '../dashboard/app/img'), path.resolve(__dirname, '../../../public/assets/dashboard/build/img'), done)
                            .then(() => {
                                console.log('IMAGE FOLDER SUCCESS COPY');
                            });
                    });
                } else {
                    fs.copy(path.resolve(__dirname, '../../../public/assets/panel-dist'), path.resolve(__dirname, '../../../public/assets/panel'), done)
                        .then(() => {
                            console.log('NEW BUILD SUCCESS COPY');
                        });
                }
            },
        }));
    }
}

if (mode === 'production') {
    config.plugins.push(new MergeIntoSingleFilePlugin({
        files: {
            'vendor.js': [
                './panel/js/old/script.js',
            ],
        },
        transform: {
            'vendor.js': (code) => UglifyJs.minify(code).code,
        },
    }));
}

module.exports = config;
