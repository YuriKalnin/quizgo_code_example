module.exports = {
    root: true,
    env: {
        browser: true,
        commonjs: true,
        es6: true,
        jquery: true,
    },
    extends: [
        'airbnb/base',
        'plugin:vue/essential',
        'eslint:recommended',
    ],
    globals: {
        WebFontConfig: true,
        demo: true,
        window: true,
        NODE_SERVER: true,
    },
    ignorePatterns: [
        '/',
        '/js/old/vendor/',
    ],
    rules: {
        'import/no-unresolved': 'off',
        'guard-for-in': 'off',
        'no-param-reassign': 'off',
        'no-useless-escape': 'off',
        'class-methods-use-this': 'off',
        indent: ['error', 4],
        'max-len': 'off',
    },
};
