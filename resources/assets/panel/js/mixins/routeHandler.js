export default {
    methods: {
        openNewWindow(href) {
            const link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a');
            if (typeof href === 'string') {
                link.href = href;
            } else {
                link.href = this.$router.resolve(href).href;
            }
            link.target = '_blank';
            const event = new MouseEvent('click', {
                view: window,
                bubbles: false,
                cancelable: true,
            });
            link.dispatchEvent(event);
        },
        routeHandlerClick($event, navigate, href) {
            if (window.targetBlank) {
                this.openNewWindow(href);
            } else {
                navigate($event);
            }
        },
        routeHandlerMousedown($event, href) {
            if ($event.which === 2) {
                this.openNewWindow(href);
            }
        },
    },
};
