import Vue from 'vue';
import targetBlankChecked from '@src/js/modules/targetBlankChecked';
import { router, nprogress } from '@src/js/routes/index';
import i18n from '@src/js/modules/i18n';
import store from '@src/js/store';
import axios from '@modules/axios';
import userAccount from '@modules/user.accounts';

export default function createApp() {
    if (!document.getElementById('market')) {
        return false;
    }
    return new Vue({
        i18n,
        store,
        router,
        nprogress,
        data: () => ({
            isAdmin: false,
            user: false,
            currentRoute: window.location.pathname,
        }),
        methods: {
            async getUser() {
                return new Promise((resolve) => {
                    if (this.user) {
                        resolve(this.user);
                    } else {
                        axios.get(this.$getRoute('get-user')).then((response) => {
                            this.user = response.data;
                            resolve(response.data);
                        });
                    }
                });
            },
        },
        async mounted() {
            $('.js-vue-loader').hide();
            targetBlankChecked();
            userAccount.userDetected();
        },
    }).$mount('#market');
}
