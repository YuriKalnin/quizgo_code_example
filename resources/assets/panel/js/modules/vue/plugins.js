import Vue from 'vue';
import quizgo from '@src/js/plugins/quizgo';
import Notifications from 'vue-notification';
import VModal from 'vue-js-modal';
import VTooltip from 'v-tooltip';
import VueAxios from 'vue-axios';
import axios from '@modules/axios';
import VueEcho from 'vue-echo-laravel';
import io from 'socket.io-client';

window.io = io;

Vue.use(VueEcho, {
    broadcaster: 'socket.io',
    host: `${window.location.hostname}:6001`,
    auth: {
        headers: {
            Authorization: axios.defaults.headers.common.Authorization,
        },
    },
});

Vue.use(quizgo);
Vue.use(Notifications);
Vue.use(VModal);
Vue.use(VTooltip);
Vue.use(VueAxios, axios);

VTooltip.options.defaultTemplate = '<div class="tooltip-vue" role="tooltip"><div class="tooltip-vue-arrow"></div><div class="tooltip-vue-inner"></div></div>';
VTooltip.options.defaultArrowSelector = '.tooltip-vue-arrow, .tooltip-vue__arrow';
VTooltip.options.defaultInnerSelector = '.tooltip-vue-inner, .tooltip-vue__inner';

export default Vue;
