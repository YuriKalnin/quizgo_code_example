import Vue from 'vue';

Vue.component('seo-api-google-indexes', () => import('@src/js/components/seo-api-google-indexes/index.vue'));
Vue.component('segmented-control', () => import('@src/js/components/design-system/SegmentedControl.vue'));
Vue.component('template-sidebar', () => import('@src/js/components/pages/template/sidebar.vue'));
Vue.component('template-navbar', () => import('@src/js/components/pages/template/navbar.vue'));
Vue.component('product-detail', () => import('@src/js/components/market/product-detail/index.vue'));
Vue.component('product-list', () => import('@src/js/components/market/product-list/index.vue'));

Vue.component('modal-nps', () => import('@src/js/components/main/nps/index.vue'));
Vue.component('page-payment', () => import('@src/js/components/pages/payment/index.vue'));
Vue.component('clips-builder', () => import('@src/js/components/clips/builder/index.vue'));
Vue.component('backend-moderate', () => import('@src/js/components/backend/moderate/index.vue'));
Vue.component('backend-romi-stat', () => import('@src/js/components/backend/romi-stat/index.vue'));
Vue.component('backend-user-dashboard', () => import('@src/js/components/backend/user-dashboard/index.vue'));
Vue.component('billing-info-transactions', () => import('@src/js/components/billing/info-transactions/index.vue'));
Vue.component('subscription-products-list', () => import('@src/js/components/market/subscription-products-list/index.vue'));
Vue.component('backend-clerking-cost-accounting', () => import('@src/js/components/backend/clerking/cost-accounting/index.vue'));

export default Vue;
