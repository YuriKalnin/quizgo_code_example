export default function targetBlankChecked() {
    document.addEventListener('keydown', (event) => {
        if (event.keyCode === 91 || event.keyCode === 17) {
            window.targetBlank = true;
        }
    });
    document.addEventListener('keyup', (event) => {
        if (event.keyCode === 91 || event.keyCode === 17) {
            window.targetBlank = false;
        }
    });
}
