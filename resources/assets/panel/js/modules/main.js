import MobileDetect from 'mobile-detect';

export function isMobile() {
    return new MobileDetect(window.navigator.userAgent).mobile();
}

export function exist(obj, key) {
    const keys = key.split('.');
    let exist = true;

    if (!obj || typeof obj === 'undefined') {
        return false;
    }

    keys.forEach((K) => {
        if (!exist) {
            return false;
        }

        if (!obj[K]) {
            exist = false;
            return false;
        }

        obj = obj[K];
    });

    return exist;
}

export function removeArElement(arr, indexes) {
    const arrayOfIndexes = [].slice.call(arguments, 1); // (1)
    return arr.filter((item, index) => // (2)
        arrayOfIndexes.indexOf(index) == -1, // (3)
    );
}

export function validURL(str) {
    const pattern = new RegExp('^(https?:\\/\\/)?' // protocol
        + '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' // domain name
        + '((\\d{1,3}\\.){3}\\d{1,3}))' // OR ip (v4) address
        + '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' // port and path
        + '(\\?[;&a-z\\d%_.~+=-]*)?' // query string
        + '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
}

export function notyResponse(vue, response) {
    vue.$notify({
        group: 'panel',
        title: response.data.message,
        type: response.data.status ? 'success' : 'error',
    });
}
export function notyFail(vue) {
    vue.$notify({
        group: 'panel',
        title: 'Произошла ошибка при удалении',
        type: 'error',
    });
}

export function rand(min, max) {
    const rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

export function notify(params) {
    const option = {
        title: '',
        text: '',
        group: 'main',
        duration: 10000,
        type: 'success',
        notify: 'demo',
        vm: false,
        ...params,
    };
    if (option.vm?.$notify || option.notify === 'vue') option.vm.$notify(option);
    else if (option.notify === 'demo') demo.noty(option.text, option.type);
}

export function copy(text, params = {}) {
    const option = { notify: 'demo', ...params };

    if (!navigator.clipboard) {
        notify({
            type: 'danger',
            text: params.vm ? params.vm.$t('default.copyFail') : 'Не удалось скопировать',
            ...option,
        });
        return false;
    }

    navigator.clipboard.writeText(text).then(
        () => {
            notify({
                type: 'secondary',
                text: params.vm ? params.vm.$t('default.copyOk') : 'Скопировано',
                ...option,
            });
        },
        () => {
            notify({
                type: 'danger',
                text: params.vm ? params.vm.$t('default.copyFail') : 'Не удалось скопировать',
                ...option,
            });
        },
    );
    return true;
}

export async function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, ms);
    });
}

export function randHash() {
    return `${Date.now()}${rand(1, 999999)}`;
}
