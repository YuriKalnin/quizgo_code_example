import axios from 'axios';
import i18n from '@modules/i18n';

export function detectLocale() {
    if (localStorage.getItem('locale')) {
        return localStorage.getItem('locale');
    }
    return document.documentElement.lang;
}

export function setToken(token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
}

export function setLocale(locale) {
    axios.defaults.headers.common.locale = locale;
    i18n.locale = locale;
}

const quizgoRtg = localStorage.getItem('quizgo-rtg');

if (quizgoRtg) { setToken(quizgoRtg); }

setLocale(detectLocale());

axios.defaults.headers.common.Accept = 'application/json';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').content;

export default axios;
