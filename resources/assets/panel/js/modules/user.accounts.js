import axios from '@modules/axios';
import cookie from './cookie';

const userAccount = {

    rand(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    },

    getRemoteCID() {
        return new Promise((resolve, reject) => {
            axios.get('/system/user-account/cid/get')
                .then((rsp) => resolve(rsp))
                .catch((e) => reject(e));
        });
    },

    userDetected() {
        if (NODE_SERVER === 'run') {
            return false;
        }

        const notSendQuery = cookie.get('user_quizgo_cid__send_query');

        if (!notSendQuery) {
            this.getRemoteCID().then((rsp) => {
                let cid = false;

                // Если из БД уже получен CID
                // тогда его и берем за основу
                if (rsp.data.status) {
                    cid = rsp.data.account.cid;
                // Если не удалось найти cid по текущему
                // Авторизованному имейлу пользователя
                } else {
                    // Получаю cid из локалсторадж
                    cid = localStorage.getItem('user_quizgo_cid');

                    // Если в локасторадж ничего нет, создам
                    // Уникальный идетификатор браузера пользователя
                    if (!cid) { cid = Date.now() + this.rand(20); }
                }

                // В любом случае запоминаю идентификатор пользователя
                localStorage.setItem('user_quizgo_cid', cid);

                // Делаю запрос чтобы пометить пользователя
                // eslint-disable-next-line no-shadow
                axios.get(`/system/user-account/save?cid=${cid}`).then((rsp) => {
                    // Если пользователь помечен успешно,
                    // На час запишу в куке, что такой запрос делать не нужно
                    if (rsp.data.status) {
                        cookie.set('user_quizgo_cid__send_query', 'y', { expires: 3600 });
                    }
                });
            });
        }

        return true;
    },
};

export default userAccount;
