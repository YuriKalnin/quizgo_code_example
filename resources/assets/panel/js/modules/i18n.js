import Vue from 'vue';
import VueI18n from 'vue-i18n';
import { detectLocale } from '@modules/axios';

Vue.use(VueI18n);

const locale = detectLocale();

export default new VueI18n({
    locale,
    fallbackLocale: locale,
    messages: {
        en: {
            default: {
                saveAndClose: 'Save and Close',
                upload: 'Upload photo',
                create: 'Create',
                open: 'Open',
                close: 'Close',
                cancel: 'Cancel',
                save: 'Save',
                no: 'No',
                delete: 'Delete',
                start: 'Start',
                wait: 'Please wait...',
                change: 'Change',
                hide: 'Hide',
                off: 'Off',
                on: 'On',
                button: 'Button',
                disabled: 'Disabled',
                included: 'Included',
                question: 'Question',
                answers: 'Answers',
                connect: 'To plug',
                second: 'seconds',
                minutes: 'minutes',
                error: 'Error :(',
                redirect: 'Redirect...',
                'error-text': 'An unexpected error has occurred. Please write to us in the support chat.',
                'wait-one': 'Please, wait...',
                'tern-on': 'Turn on',
                'save-and-close': 'Save and close',
                'error-remove': 'Error',
                'change-photo': 'Change photo',
                'additional-options': 'Additional options',
                badRequest403Description: 'Access to the requested resource is restricted',
                badRequest404Description: 'The requested page was not found.',
                moveHome: 'Try to go to',
                homePage: 'home page',
                copyFail: 'Failed to copy',
                copyOk: 'Copied',
                readonly: 'Readonly',
            },
        },
        ru: {
            default: {
                saveAndClose: 'Сохранить и закрыть',
                upload: 'Загрузить фото',
                create: 'Создать',
                open: 'Открыть',
                close: 'Закрыть',
                cancel: 'Отмена',
                save: 'Сохранить',
                no: 'Нет',
                delete: 'Удалить',
                start: 'Начать',
                wait: 'Пожалуйста, подождите...',
                change: 'Выбрать',
                hide: 'Скрыть',
                off: 'Выключен',
                on: 'Включен',
                button: 'Кнопку',
                disabled: 'Отключено',
                included: 'Включено',
                question: 'Вопрос',
                answers: 'Ответы',
                connect: 'Подключить',
                second: 'секунд',
                minutes: 'минут',
                error: 'Error :(',
                redirect: 'Перенаправление...',
                'error-text': 'Возникла непредвиденная ошибка. Пожалуйста, напишите нам в чат поддержки.',
                'wait-one': 'Подождите',
                'tern-on': 'Включить',
                'save-and-close': 'Сохранить и выйти',
                'error-remove': 'Произошла ошибка при удалении',
                'change-photo': 'Выбрать фото',
                'additional-options': 'Дополнительные параметры',
                badRequest403Description: 'Доступ к запрашиваемому ресурсу ограничен',
                badRequest404Description: 'Запрашиваемая страница не найдена.',
                moveHome: 'Попробуйте перейти на ',
                homePage: 'главную страницу',
                copyFail: 'Не удалось скопировать',
                copyOk: 'Скопировано',
                readonly: 'Только чтение',
            },
        },
    },
});
