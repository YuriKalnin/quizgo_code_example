export default {
    props: [
        'jsonQuiz',
        'question',
        'index',
    ],
    data() {
        return {
            quiz: this.jsonQuiz,
            q: this.question,
            i: this.index,
        };
    },
    updated() {
        if (this.jsonQuiz) this.quiz = this.jsonQuiz;
        if (this.question) this.q = this.question;
        if (this.index) this.i = this.index;
    },
    computed: {
        questions() {
            return this.quiz.questions;
        },
        type() {
            return this.q && this.q.type;
        },
        isLast() {
            return (this.index + 1) === this.quiz.questions.length;
        },
        questionImages() {
            const images = [];
            if (!this.question) return images;
            if (this.checkImage(this.question?.image)) {
                images.push(this.question.image);
            }
            this.question.values.forEach((value) => {
                if (this.checkImage(value?.image)) {
                    images.push(value.image);
                }
            });
            return images;
        },
        questionImagesIds() {
            return this.questionImages.map((image) => image.id);
        },
    },
    methods: {
        checkImage(image) {
            return image?.src && image?.id;
        },
        findQuestionBySlug(slug) {
            return this.quiz.questions.find((question) => question.slug === slug);
        },
        generateQuestionSlug() {
            return `q${this.quiz.questions.length + 1}`;
        },
        getQuestionData() {
            return {
                title: 'Название вопроса',
                questionView: 1,
                slug: this.generateQuestionSlug(),
                type: 'text',
                placeholder: 'Пример ответа:',
                collapse: 'on',
                required: true,
                fileUploadType: 'all',
                values: [],
                when: {
                    logic: 'or',
                    rules: [],
                },
                cardScroll: 'vertical',
                multiple: true,
                dateRange: false,
                dateTime: false,
                dateBefore: false,
                dateDefaultFirst: '',
                dateDefaultSecond: '',
                rangeType: 'single',
                multipleRows: false,
                image: {
                    id: 0,
                    src: '',
                    enable: false,
                },
                rangeFormat: {
                    prefix: '',
                    suffix: '',
                    decimals: 0,
                },
            };
        },
        removeImage(files) {
            if (files.length) return this.$httpPost(this.$getRoute('quiz.upload.remove'), { files_ids: files });
            return false;
        },
        copyImage(files) {
            if (files.length) return this.$httpPost(this.$getRoute('quiz.upload.copy'), { files_ids: files });
            return false;
        },
    },
};
