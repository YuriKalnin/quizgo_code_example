import axios from '@src/js/modules/axios';

const quizgo = {

    install(Vue) {
        Vue.prototype.$httpGet = function (url, params = {}) {
            return this.$request('get', url, {}, params);
        };

        Vue.prototype.$httpDelete = function (url, params = {}) {
            return this.$request('delete', url, {}, params);
        };

        Vue.prototype.$httpPost = function (url, data = {}, params = {}) {
            return this.$request('post', url, data, params);
        };

        Vue.prototype.$httpPut = function (url, data = {}, params = {}) {
            return this.$request('put', url, data, params);
        };

        Vue.prototype.$request = function (method, url, data = {}, params = { }) {
            params = {
                ...{
                    progress: true,
                    notifySuccess: false,
                    notifyError: 'demo',
                },
                ...params,
            };

            if (params.progress) this.$nprogress.start();

            let request;

            if (method === 'get') request = axios.get(url);
            if (method === 'post') request = axios.post(url, data);
            if (method === 'put') request = axios.put(url, data);
            if (method === 'delete') request = axios.delete(url);

            return new Promise((resolve, reject) => {
                request.then((response) => {
                    if (params.progress) this.$nprogress.done();
                    if (params.notifySuccess && response.status === 200) {
                        this.$notify({
                            title: response.statusText,
                            text: response.data?.message || ' ',
                            group: 'main',
                            duration: 5000,
                            type: 'secondary',
                        });
                    }
                    resolve(response);
                }).catch((error) => {
                    if (params.notifyError === 'demo') {
                        if (error?.response?.data?.message) {
                            demo.noty(error?.response?.data?.message, 'danger');
                        } else {
                            demo.noty('Произошла ошибка. Пожалуйста обратитесь в поддержку.', 'danger');
                        }
                    } else if (params.notifyError) {
                        let message = '';
                        if (error?.response?.data?.message) {
                            message = error.response.data.message;
                        } else {
                            message = 'Произошла ошибка. Пожалуйста обратитесь в поддержку.';
                        }
                        this.$notify({
                            title: `${error.response.status} ${error.response.statusText}`,
                            text: message,
                            group: 'main',
                            duration: 10000,
                            type: 'danger',
                        });
                    }
                    if (params.progress) this.$nprogress.done();
                    reject(error);
                });
            });
        };

        Vue.prototype.$getRoute = function (name, params = true) {
            if (params === true) {
                params = this.$route.params;
            }

            const route = this.$router.resolve({
                name,
                params,
            });

            const devUrl = route?.route?.meta?.devUrl;

            return NODE_SERVER === 'run' && devUrl ? devUrl : route.href;
        };

        Vue.prototype.$setLocaleMessages = function (messages) {
            this.$i18n.mergeLocaleMessage('en', messages.en);
            this.$i18n.mergeLocaleMessage('ru', messages.ru);
        };

        Vue.prototype.$isRu = function () {
            return this.$i18n.locale === 'ru';
        };
    },
};

export default quizgo;
