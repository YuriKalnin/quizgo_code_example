export default {
    namespaced: true,
    state() {
        return {
            comments: [],
        };
    },
    mutations: {
        setComments(state, comments) { state.comments = comments; },
    },
    actions: {
        getComments({ commit }, params) {
            commit('setComments', []);
            params.vm.$httpGet(params.vm.$getRoute('crm.lead.comment.store', params))
                .then(({ data }) => commit('setComments', data.comments));
        },
        destroy({ commit, getters }, comment) {
            if (getters.comments) commit('setComments', getters.comments.filter((c) => c.id !== comment.id));
        },
        edit({ commit, getters }, params) {
            if (getters.comments) {
                const comments = getters.comments.map((c) => {
                    if (c.id === params.comment.id) c.message = params.message;
                    return c;
                });
                commit('setComments', comments);
            }
        },
        create({ commit, getters }, params) {
            const url = params.vm.$getRoute('crm.lead.comment.store', params);
            params.vm
                .$httpPost(url, { message: params.message }, { notifyError: 'vue' })
                .then(({ data }) => commit('setComments', getters.comments.concat([data.comment])));
        },
    },
    getters: {
        comments(state) { return state.comments; },
    },
};
