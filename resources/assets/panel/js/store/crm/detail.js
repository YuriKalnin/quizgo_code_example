export default {
    namespaced: true,
    state() {
        return {
            lead: false,
        };
    },
    mutations: {
        setLead(state, lead) { state.lead = lead; },
    },
    actions: {
        getLead({ commit }, params) {
            commit('setLead', { data: { status: 'load', lead: false } });
            document.body.classList.add('no-scroll');
            params.vm.$httpGet(params.vm.$getRoute('crm.lead.show', params), { notifyError: 'vue' })
                .then((response) => commit('setLead', response))
                .catch((error) => commit('setLead', error.response));
        },
        close({ commit }) {
            commit('setLead', false);
            document.body.classList.remove('no-scroll');
        },
    },
    getters: {
        lead(state) { return state.lead; },
        leadDetail(state) { return state.lead?.data?.lead; },
        authUser(state) { return state.lead?.data?.authUser; },
    },
};
