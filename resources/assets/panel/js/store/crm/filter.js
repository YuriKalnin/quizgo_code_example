/* eslint-disable camelcase */
import i18n from '@modules/i18n';
import { router } from '@src/js/routes/index';

export default {
    namespaced: true,
    state() {
        return {
            sort: '{"order_field":"created_at","order_direction":"desc"}',
        };
    },
    mutations: {
        setSort(state, sort) { state.sort = sort; },
    },
    actions: {
        setSort({ commit }, value) { commit('setSort', value); },
        initSort({ commit }) {
            const route = router.app.$route;
            const { order_field } = route.query;
            const { order_direction } = route.query;

            const params = {};

            if (order_field) params.order_field = order_field;
            if (order_direction) params.order_direction = order_direction;

            if (order_field && order_direction) commit('setSort', JSON.stringify(params));
        },
    },
    getters: {
        sort(state) {
            return state.sort;
        },
        sortBy(state) {
            return JSON.parse(state.sort);
        },
        sortByOptions() {
            return [
                { text: i18n.t('crm.byCreationDown'), value: '{"order_field":"created_at","order_direction":"desc"}' },
                { text: i18n.t('crm.byCreationUp'), value: '{"order_field":"created_at","order_direction":"asc"}' },
                { text: i18n.t('crm.byUpdatedDown'), value: '{"order_field":"updated_at","order_direction":"desc"}' },
                { text: i18n.t('crm.byUpdatedUp'), value: '{"order_field":"updated_at","order_direction":"asc"}' },
            ];
        },
    },
};
