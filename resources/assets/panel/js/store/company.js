export default {
    namespaced: true,
    state: () => ({
        projects: false,
        project: {
            id: '',
            name: '',
        },
    }),
    mutations: {
        removeProject(state, project) {
            state.projects = state.projects.filter((p) => p.id !== project.id);
        },
        addProject(state, project) { state.projects.push(project); },
        setProject(state, project) { state.project = project; },
        setProjects(state, projects) { state.projects = projects; },
        setWidgets(state, { data, projectId }) {
            state.projects.forEach((project) => {
                if (project.id === Number(projectId)) {
                    project.widgets = data;
                    project.quiz_count_change = data.quizes.length + data.clips.length;
                    project.open = true;
                }
            });
        },
    },
    actions: {
        setProject({ commit }, project) { commit('setProject', project); },
        addProject({ commit }, project) { commit('addProject', project); },
        removeProject({ commit }, project) { commit('removeProject', project); },
        getProjects({ commit }, { vm }) {
            const url = vm.$getRoute('projects.list', true);
            vm.$httpGet(url, { notifyError: true }).then(({ data }) => {
                commit('setProjects', data.projects);
            });
        },
        getWidgets({ commit }, { vm, projectId }) {
            const url = vm.$getRoute('quiz.list', {
                company: vm.$route.params.company,
                project: projectId,
            });
            vm.$httpGet(url).then(({ data }) => {
                commit('setWidgets', {
                    data,
                    projectId,
                });
            });
        },
    },
    getters: {
        projects(state) {
            return state.projects;
        },
        project(state) {
            return state.project;
        },
    },
};
