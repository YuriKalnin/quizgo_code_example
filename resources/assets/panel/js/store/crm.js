import axios from '@modules/axios';
import { sleep } from '@modules/main';
import filter from './crm/filter';
import detail from './crm/detail';
import comments from './crm/comments';

export default {
    namespaced: true,
    modules: {
        detail,
        filter,
        comments,
    },
    state: () => ({
        vm: false,
        leads: false,
    }),
    mutations: {
        setVm(state, vm) { state.vm = vm; },
        setLeads(state, leads) { state.leads = leads; },
        setStatusLoadingMore(state, params) { state.leads[params.status].statusLoadingMore = true; },

        setLeadsByStatus(state, params) {
            state.leads[params.status].statusLoading = false;
            state.leads[params.status].statusLoadingMore = false;
            state.leads[params.status].total = params.data.total;
            state.leads[params.status].page = params.data.nextPage;
            params.data.leads.forEach((lead) => {
                state.leads[params.status].data.push(lead);
            });
        },
        deleteLead(state, params) {
            state.leads[params.lead.status].total -= 1;
            state.leads[params.lead.status].data = state.leads[params.lead.status].data.filter((lead) => lead.id !== params.lead.id);
        },
        setStatus(state, { lead, status }) {
            const leadColl = state.leads[lead.status].data.find((l) => l.id === lead.id);

            state.leads[status].data.unshift(leadColl);
            state.leads[lead.status].data = state.leads[lead.status].data.filter((l) => l.id !== leadColl.id);

            state.leads[status].total += 1;
            state.leads[lead.status].total -= 1;

            state.leads[status].data = state.leads[status].data.map((l) => {
                if (l.id === lead.id) { l.status = status; }
                return l;
            });
        },
    },
    actions: {
        setStatus({ commit, state }, params) {
            commit('setStatus', params);
            const url = state.vm.$getRoute('crm.lead.show', { lead: params.lead.id });
            state.vm.$httpPut(url, { status: params.status });
        },
        async loadAll({ dispatch, commit }, vm) {
            await commit('setVm', vm);
            await commit('setLeads', {
                new: {
                    page: 1, total: 0, data: [], statusLoading: true, statusLoadingMore: false,
                },
                unlocked: {
                    page: 1, total: 0, data: [], statusLoading: true, statusLoadingMore: false,
                },
                progress: {
                    page: 1, total: 0, data: [], statusLoading: true, statusLoadingMore: false,
                },
                success: {
                    page: 1, total: 0, data: [], statusLoading: true, statusLoadingMore: false,
                },
                canceled: {
                    page: 1, total: 0, data: [], statusLoading: true, statusLoadingMore: false,
                },
            });

            await dispatch('loadLeads', { status: 'new' });
            await dispatch('loadLeads', { status: 'unlocked' });
            await dispatch('loadLeads', { status: 'progress' });
            await dispatch('loadLeads', { status: 'success' });
            await dispatch('loadLeads', { status: 'canceled' });
        },
        async loadLeads({
            commit, state, dispatch, getters,
        }, params) {
            const options = { loadMore: false, ...params };

            if (options.loadMore) commit('setStatusLoadingMore', options);

            await sleep(500);
            await dispatch('filter/initSort');

            const url = state.vm.$getRoute('crm.lead.load', {
                status: options.status,
                page: state.leads[options.status].page,
                ...getters.leadLoadBy,
                ...getters['filter/sortBy'],
            });

            axios.get(url).then(({ data }) => commit('setLeadsByStatus', {
                status: options.status,
                data,
            }));
        },
        deleteLead({ commit, state, dispatch }, params) {
            const { lead } = params;
            const { vm } = state;
            if (!confirm(vm.$t('crmStatus.confirmDeleteLead', { id: lead.id }))) return false;
            const url = vm.$getRoute('crm.lead.destroy', { lead: lead.id });
            vm.$httpDelete(url).then(() => {
                commit('deleteLead', { lead });
                dispatch('detail/close', lead);
            });
            return true;
        },
    },
    getters: {
        leads(state) {
            return state.leads;
        },
        leadLoadBy(state) {
            if (state.vm.$route.params.quiz) {
                return {
                    value: state.vm.$route.params.quiz,
                    field: 'quiz_id',
                };
            }
            if (state.vm.$route.params.project) {
                return {
                    value: state.vm.$route.params.project,
                    field: 'project_id',
                };
            }
            return {
                value: state.vm.$route.params.company,
                field: 'company_id',
            };
        },
    },
};
