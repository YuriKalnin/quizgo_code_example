export default {
    namespaced: true,
    state: () => ({
        index: false,
        question: false,
        answerValue: false,
    }),
    mutations: {
        setIndex(state, index) { state.index = index; },
        setQuestion(state, question) { state.question = question; },
        setType(state, type) { state.question.type = type; },
        setAnswerValue(state, answerValue) { state.answerValue = answerValue; },
    },
    actions: {
        setIndex({ commit }, index) {
            commit('setIndex', index);
        },
        setAnswerValue({ commit }, answerValue) {
            commit('setAnswerValue', answerValue);
        },
        setQuestion({ commit }, question) {
            commit('setQuestion', question);
        },
        setType({ commit }, type) {
            commit('setType', type);
        },
    },
    getters: {
        question(state) {
            return state.question;
        },
        indexVuex(state) {
            return state.index;
        },
        questionVuex(state) {
            return state.question;
        },
        answerValue(state) {
            return state.answerValue;
        },
        answerValueComment(state) {
            if (state?.question?.values && state.question.values[state.answerValue]) return state.question.values[state.answerValue].comment;
            return '';
        },
    },
};
