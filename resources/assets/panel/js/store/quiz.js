export default {
    namespaced: true,
    state: () => ({
        quizDetail: {
            id: '',
            name: '',
            project_id: '',
        },
        templates: false,
        categories: false,
    }),
    mutations: {
        setQuizDetail(state, quiz) { state.quizDetail = quiz; },
        setTemplates(state, templates) { state.templates = templates; },
        setCategories(state, categories) { state.categories = categories; },
    },
    actions: {
        setQuizDetail({ commit }, quiz) { commit('setQuizDetail', quiz); },
        getTemplates({ commit, state }, vm) {
            if (!state.templates) {
                vm.$httpGet(vm.$getRoute('quiz.templates')).then(({ data }) => {
                    commit('setTemplates', data.templates);
                    commit('setCategories', data.categories);
                });
            }
        },
    },
    getters: {
        quizDetail(state) {
            return state.quizDetail;
        },
        templates(state) {
            return state.templates;
        },
        categories(state) {
            return state.categories;
        },
    },
};
