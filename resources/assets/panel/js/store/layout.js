export default {
    namespaced: true,
    state: () => ({
        sidebar: false,
        sidebarOpen: false,
        selectCompanyOpen: false,
    }),
    mutations: {
        setSidebar(state, sidebar) { state.sidebar = sidebar; },
        sidebarOpen(state, open) { state.sidebarOpen = open; },
        selectCompanyOpen(state, open) { state.selectCompanyOpen = open; },
    },
    actions: {
        getSidebar({ commit }, vm) {
            const url = vm.$getRoute('company.sidebar.get', vm.$route.params);
            vm.$httpGet(url).then(({ data }) => {
                commit('setSidebar', data);
            });
        },
        showSidebar({ commit }) { commit('sidebarOpen', true); },
        hideSidebar({ commit }) { commit('sidebarOpen', false); },
        toggleSidebar({ commit, getters }) { commit('sidebarOpen', !getters.sidebarOpen); },

        showSelectCompany({ commit }) { commit('selectCompanyOpen', true); },
        hideSelectCompany({ commit }) { commit('selectCompanyOpen', false); },
        toggleSelectCompany({ commit, getters }) { commit('selectCompanyOpen', !getters.selectCompanyOpen); },
    },
    getters: {
        sidebar(state) {
            return state.sidebar;
        },
        sidebarOpen(state) {
            return state.sidebarOpen;
        },
        selectCompanyOpen(state) {
            return state.selectCompanyOpen;
        },
        navbarMenuIcon(state) {
            return state.sidebarOpen ? 'uil uil-multiply' : 'uil uil-bars';
        },
    },
};
