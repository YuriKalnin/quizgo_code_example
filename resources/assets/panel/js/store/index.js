import Vue from 'vue';
import Vuex from 'vuex';
import crm from './crm';
import auth from './auth';
import quiz from './quiz';
import clips from './clips';
import layout from './layout';
import company from './company';
import quizBuilder from './quizBuilder';
import createWidget from './createWidget';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        crm,
        auth,
        quiz,
        clips,
        layout,
        company,
        quizBuilder,
        createWidget,
    },
});
