export default {
    namespaced: true,
    state: () => ({
        widget: 'clips',
    }),
    mutations: {
        setWidget(state, widget) { state.widget = widget; },
    },
    actions: {
        setWidget({ commit }, widget) { commit('setWidget', widget); },
    },
    getters: {
        widget(state) {
            return state.widget;
        },
    },
};
