import questions from '@src/js/store/quizBuilder/questions';

export default {
    namespaced: true,
    modules: {
        questions,
    },
};
