export default {
    namespaced: true,
    state: () => ({
        clipsDetail: {
            id: '',
            name: '',
            project_id: '',
        },
    }),
    mutations: {
        setClipsDetail(state, clips) { state.clipsDetail = clips; },
    },
    actions: {
        setClipsDetail({ commit }, clips) { commit('setClipsDetail', clips); },
    },
    getters: {
        clipsDetail(state) {
            return state.clipsDetail;
        },
    },
};
