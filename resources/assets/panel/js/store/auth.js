import axios, { setToken, setLocale } from '@src/js/modules/axios';
import getRouteByName from '@src/js/mixins/getRouteByName';

export default {
    namespaced: true,
    state: () => ({
        auth: false,
        locale: false,
        access: false,
        status: false,
        statusText: false,
        companies: false,
        activeCompany: false,
        exceptionMessage: false,
    }),
    mutations: {
        addCompany(state, company) {
            state.companies.push(company);
        },
        setAuth(state, response) {
            state.auth = response.data?.auth;
            state.access = response.data?.access;

            if (response.data?.activeCompany) state.activeCompany = response.data.activeCompany;
            if (response.data?.auth?.companies) state.companies = response.data.auth.companies;

            state.status = response.status;
            state.statusText = response.statusText;
            state.exceptionMessage = response.data?.message;
            if (state?.auth?.token) {
                setToken(state?.auth?.token);
                localStorage.setItem('quizgo-rtg', state.auth.token);
            }
        },
        setLocale(state, response) {
            state.locale = response.data?.locale;
            if (state?.locale) {
                setLocale(state?.locale);
                localStorage.setItem('locale', state?.locale);
            }
        },
    },
    actions: {
        authUnderSecond({ getters }, vm) {
            localStorage.setItem('quizgo-rtg', getters.secondAuth.token);
            localStorage.setItem('second_auth_token', '');
            localStorage.setItem('second_auth_email', '');
            localStorage.setItem('second_auth_company', '');

            window.location.href = vm.$getRoute('projects', {
                company: getters.secondAuth.company,
            });
        },
        addCompany({ commit }, addCompany) {
            commit('addCompany', addCompany);
        },
        setLocale({ commit }, response) {
            commit('setLocale', response);
        },
        setAuth({ commit }, response) {
            commit('setAuth', response);
        },
        async access({ commit }, params) {
            const accessFor = params.to.name.replace(/\./g, '-');
            const url = getRouteByName('auth.access', {
                access: accessFor,
            });
            const post = { ...params.to.params, cache: 0 };
            return axios.post(url, post).then((response) => {
                commit('setAuth', response);
                commit('setLocale', response);
            }).catch((error) => {
                commit('setAuth', error.response);
                commit('setLocale', error.response);
            });
        },
    },
    getters: {
        auth(state) {
            return state.auth;
        },
        isOk(state) {
            return state.status === 200;
        },
        isNotAuth(state) {
            return state.status === 401;
        },
        status(state) {
            return {
                code: state.status,
                text: state.statusText,
                exceptionMessage: state.exceptionMessage,
            };
        },
        user(state) {
            return state.auth?.user ? state.auth.user : false;
        },
        blocked(state) {
            return state.auth?.user?.blocked;
        },
        isAdmin(state) {
            return state.auth?.isAdmin;
        },
        isSudo(state) {
            return state.auth?.isSudo;
        },
        billing(state) {
            return state.auth && state.auth.billing;
        },
        companies(state) {
            return state.companies && state.companies;
        },
        partnerId(state) {
            return state.auth?.partner?.id;
        },
        activeCompany(state) {
            return state.activeCompany;
        },
        locale(state) {
            if (state?.locale) {
                return state?.locale;
            }
            if (localStorage.getItem('locale')) {
                return localStorage.getItem('locale');
            }
            return state?.locale ? state?.locale : 'ru';
        },
        secondAuth() {
            return {
                company: localStorage.getItem('second_auth_company'),
                token: localStorage.getItem('second_auth_token'),
                email: localStorage.getItem('second_auth_email'),
            };
        },
    },
};
