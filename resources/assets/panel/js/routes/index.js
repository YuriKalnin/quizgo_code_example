import Vue from 'vue';
import VueRouter from 'vue-router';
import NProgress from 'vue-nprogress';
import AuthRoutes from '@src/js/routes/auth';
import CompanyRoutes from '@src/js/routes/company';
import FrontendRoutes from '@src/js/routes/frontend';
import ServicesRoutes from '@src/js/routes/services';
import store from '@src/js/store';
import quizGetJson from '../../http/auto/company/16/project/33/quiz/111/get-json/response-200.json';
import devRouteQuizFileUpload from '../../http/quiz/file-upload.json';
import integrationsGetJson from '../../http/quiz/integrations.json';
import routeNavbarJson from '../../http/layouts/navbar.json';
import routeSidebarJson from '../../http/company/16/vue/template/sidebar/json/response-200.json';

Vue.use(VueRouter);
Vue.use(NProgress);

let routes = [
    {
        path: NODE_SERVER === 'run' ? '/panel/http/users/user.json' : '/service/user/get',
        name: 'get-user',
    },
    {
        path: '/system/get-quiz-integrations-info/:quiz',
        name: 'get-quiz-integrations-info',
        meta: {
            devUrl: integrationsGetJson,
        },
    },
    {
        path: '/company/:company/vue/template/navbar/json',
        name: 'vue.template.navbar',
        meta: {
            devUrl: routeNavbarJson,
        },
    },
    {
        path: '/company/:company/vue/template/sidebar/json',
        name: 'vue.template.sidebar',
        meta: {
            devUrl: routeSidebarJson,
        },
    },
    {
        path: '/system/quiz/:quiz/ssl/check-ssl-domain',
        name: 'quiz.check-ssl.domain',
    },
    {
        path: '/company/:company/market/project-list/json',
        name: 'market.project.list',
    },
    {
        path: '/company/:company/billing/:billing',
        name: 'company.billing',
    },
    {
        path: '/company/:company/subscription',
        name: 'company.subscription',
    },
    {
        path: '/company/:company/project/:project/clips/make',
        name: 'clips.make',
    },
    {
        path: '/company/:company/project/:project/clips/:clips/edit',
        name: 'clips.edit',
    },
    {
        path: '/company/:company/market/bind/product-quiz/json',
        name: 'market.bind.product-quiz',
    },
    {
        path: '/company/:company/project/:project/quiz/:quiz',
        children: [
            {
                path: 'copy',
                name: 'quiz.copy',
            },
            {
                path: 'template/create',
                name: 'quiz-template-create',
            },
            {
                path: 'ssl/create-ssl-domain',
                name: 'quiz.create-ssl',
            },
            {
                path: 'get-json',
                name: 'quiz.get.json',
                meta: {
                    devUrl: quizGetJson,
                },
            },
            {
                path: 'upload-image',
                name: 'quiz-edit-upload-image',
                meta: {
                    devUrl: devRouteQuizFileUpload,
                },
            },
            {
                path: 'json-import',
                name: 'quiz-json-import',
            },
            {
                path: 'update',
                name: 'quiz-edit-update',
            },
            {
                path: 'update-domain',
                name: 'quiz.update.domain',
            },
            {
                path: 'update-slug',
                name: 'quiz.update.slug',
            },
            {
                path: 'remove-domain',
                name: 'quiz.remove.domain',
            },
        ],
    },
    {
        path: '/company/:company/market/bind/billing-company/json',
        name: 'market.bind.billing-company',
    },
    {
        path: '/backend/stat/metrics/conv-reg-to-pay',
        name: 'backend.stat.conv-reg-to-pay',
        component: () => import('../components/pages/backend/stat/metrics/ConvRegToPay.vue'),
    },
    {
        path: '/backend/stat/metrics/revenue',
        name: 'backend.stat.revenue',
        component: () => import('../components/pages/backend/stat/metrics/Revenue.vue'),
    },
    {
        path: '/backend/stat/metrics/live-time',
        name: 'backend.stat.live-time',
        component: () => import('../components/pages/backend/stat/metrics/LiveTime.vue'),
    },
    {
        path: '/backend/stat/metrics/decision-time',
        name: 'backend.stat.decision-time',
        component: () => import('../components/pages/backend/stat/metrics/DecisionTime.vue'),
    },
];

routes = routes.concat(
    FrontendRoutes,
    ServicesRoutes,
    CompanyRoutes,
    AuthRoutes,
);

export const nprogress = new NProgress();

export const router = new VueRouter({
    mode: 'history',
    routes,
});

router.afterEach(() => {
    window.scrollTo(0, 0);
});

router.beforeEach(async (to, from, next) => {
    if (!to.name) return next();
    if (['company'].indexOf(to.name) + 1) return next();
    // if (router.app.$nprogress) router.app.$nprogress.start();
    await store.dispatch('auth/access', { to });
    // if (router.app.$nprogress) router.app.$nprogress.done();
    if (store.getters['auth/isOk'] && (['login', 'register', 'password.reset', 'password.update'].indexOf(to.name) + 1)) {
        return next({
            name: 'projects',
            params: {
                company: store.getters['auth/activeCompany'].id,
            },
        });
    }
    store.dispatch('layout/hideSelectCompany');
    store.dispatch('layout/hideSidebar');
    return next();
});
