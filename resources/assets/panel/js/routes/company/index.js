import urlCssEditor from '@src/http/quiz/css-editor.json';

export default [
    {
        path: '/company/:company',
        component: () => import('@src/js/pages/company/Company'),
        children: [
            {
                path: '/',
                name: 'projects',
                component: () => import('@src/js/pages/company/CompanyProjects'),
            },
            {
                path: 'crm',
                name: 'crm',
                component: () => import('@src/js/pages/crm/CompanyCrm'),
                children: [
                    {
                        name: 'crm.project',
                        path: 'project/:project',
                        component: () => import('@src/js/pages/crm/CompanyCrm'),
                        children: [
                            {
                                name: 'crm.quiz',
                                path: 'quiz/:quiz',
                                component: () => import('@src/js/pages/crm/CompanyCrm'),
                            },
                        ],
                    },
                ],
            },
            {
                path: 'swirl',
                name: 'swirl',
                children: [
                    {
                        name: 'swirl.quiz',
                        path: 'project/:project/quiz/:quiz',
                    },
                ],
            },
            {
                path: 'subscription',
                name: 'subscription',
            },
            {
                path: 'telegram-bot',
                name: 'telegram',
            },
            {
                path: 'market',
                name: 'market',
            },
            {
                path: 'edit',
                name: 'company.edit',
                component: () => import('@src/js/pages/company/CompanyEdit'),
            },
            {
                path: 'access',
                name: 'access',
                component: () => import('@src/js/pages/company/CompanyAccess'),
            },
            {
                path: 'rate',
                name: 'rate.index',
                component: () => import('@src/js/pages/company/Rate'),
            },
        ],
    },
    {
        path: '/partner/:partner/edit',
        name: 'partner',
    },
    {
        path: '/partner/:partner/clients',
        name: 'partner.clients',
    },
    {
        path: '/company/:company/project/:project/quiz/:quiz/edit',
        component: () => import('@pages/quiz-edit/Index.vue'),
        name: 'quiz-edit',
        children: [
            {
                path: 'css/update',
                name: 'quiz-edit-css-update',
            },
            {
                path: 'css/json',
                name: 'quiz-edit-css-json',
                meta: {
                    devUrl: urlCssEditor,
                },
            },
            {
                path: 'form',
                component: () => import('@src/js/components/quiz-builder/Formp.vue'),
                name: 'quiz-edit-form',
            },
            {
                path: 'start',
                component: () => import('@src/js/components/quiz-builder/StartPage.vue'),
                name: 'quiz-edit-start',
            },
            {
                path: 'questions',
                component: () => import('@src/js/components/quiz-builder/Questions.vue'),
                name: 'quiz-edit-questions',
            },
            {
                path: 'bonus',
                component: () => import('@src/js/components/quiz-builder/Bonus.vue'),
                name: 'quiz-edit-bonus',
            },
            {
                path: 'results',
                component: () => import('@src/js/components/quiz-builder/Results.vue'),
                name: 'quiz-edit-results',
            },
            {
                path: 'settings',
                component: () => import('@src/js/components/quiz-builder/Settings.vue'),
                name: 'quiz-edit-settings',
            },
            {
                path: 'setup',
                component: () => import('@src/js/components/quiz-builder/Setup.vue'),
                name: 'quiz-edit-setup',
            },
            {
                path: 'design',
                component: () => import('@src/js/components/quiz-builder/Design.vue'),
                name: 'quiz-edit-design',
            },
            {
                path: 'integrations',
                component: () => import('@src/js/components/quiz-builder/Integrations.vue'),
                name: 'quiz-edit-integrations',
            },
            {
                path: 'css',
                component: () => import('@src/js/components/quiz-builder/CssEditor.vue'),
                name: 'quiz-edit-css',
            },
            {
                path: 'yandex-business',
                name: 'quiz-edit-yandex-business',
                component: () => import('@src/js/components/quiz-builder/YandexBusiness.vue'),
            },
        ],
    },
];
