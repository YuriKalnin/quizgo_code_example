<?php

namespace App\Services\Rate\Services;

class CouponService
{
    private CouponService\Make $make;

    public function __construct(CouponService\Make $make)
    {
        $this->make = $make;
    }

    public function make($user, $company, $coupon) : CouponService\Make {
        return $this->make->handle($user, $company, $coupon);
    }
}
