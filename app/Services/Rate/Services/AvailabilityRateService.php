<?php

namespace App\Services\Rate\Services;

use App\Services\Rate\Services\AvailabilityRateService\DemoAvailableService;
use App\Services\Rate\Services\AvailabilityRateService\LeadMagnetService;
use App\Services\Rate\Services\AvailabilityRateService\PayedAvailableService;

use RateService;

/**
 * Производит расчеты
 * доступности тарифа
 */
class AvailabilityRateService
{
    public DemoAvailableService $demo;
    public PayedAvailableService $payed;
    public LeadMagnetService $leadMagnet;

    public bool $enable;
    public string $enableString;
    public $costDay;

    private $accountCollect;

    public function __construct(
        DemoAvailableService $demoAvailable,
        PayedAvailableService  $payedAvailable,
        LeadMagnetService $leadMagnet
    ) {
        $this->demo = $demoAvailable;
        $this->payed = $payedAvailable;
        $this->leadMagnet = $leadMagnet;
    }

    public function handle($accountCollect, $costDay) : self {

        $this->accountCollect = $accountCollect;
        $this->costDay = $costDay;

        $this->demo->handle($accountCollect);
        $this->payed->handle($accountCollect, $costDay);
        $this->leadMagnet->handle($accountCollect);

        $this->enable = $accountCollect->is_enable;

        $this->getEnableString();

        return $this;
    }

    private function getEnableString() : self {

        $this->enableString = '';

        if ($this->accountCollect->is_quiz_plus) {
            if ($this->accountCollect->demo_enable) {
                $this->enableString = __('rate.enable-string__quiz-plus-demo-active', [
                    'date' => $this->accountCollect->demo_date_format,
                    'days' => $this->accountCollect->demo_date_format_ago,
                ]);
            } elseif ($this->enable) {
                $this->enableString = __('rate.enable-string__quiz-plus-active', [
                    'date' => $this->payed->activeTo,
                    'days' => $this->payed->words,
                ]);
            } else {
                $this->enableString = __('rate.enable-string__quiz-plus-not-active');
            }
        } else {
            if ($this->costDay > 0 && !$this->accountCollect->quiz_plus_enable) {
                $this->enableString = __('rate.additional-packets-need-payed');
            }
            else if ($this->accountCollect->demo_enable) {
                $this->enableString = __('rate.enable-string__lead-magnet-demo-active', [
                    'date' => $this->accountCollect->demo_date_format,
                    'days' => $this->accountCollect->demo_date_format_ago,
                ]);
            } else if ($this->enable) {
                $this->enableString = __('rate.enable-string__lead-magnet-active', [
                    'leads' => $this->accountCollect->lead_magnet_limit_format,
                ]);
            } else {
                $this->enableString = __('rate.enable-string__lead-magnet-not-active', [
                    'leads' => $this->accountCollect->lead_magnet_limit_format
                ]);
            }
        }

        return $this;
    }
}
