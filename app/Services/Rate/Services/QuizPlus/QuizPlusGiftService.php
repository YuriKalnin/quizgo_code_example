<?php

namespace App\Services\Rate\Services\QuizPlus;

use App\Services\Rate\Traits\StatusTrait;
use App\Models\Company;
use App\Models\Transaction;
use App\Services\Rate\Services\GetRateService;
use NotyService;
use Exception;

/**
 * Начисляет подарочные месяцы на тарифе квиз+
 */
class QuizPlusGiftService
{
    use StatusTrait;

    private Transaction $transaction;
    private Company $company;

    private GetRateService $getRateService;
    private int $giftMonths;

    public function __construct(GetRateService $getRateService)
    {
        $this->getRateService = $getRateService;
    }

    public function handle($company, $transaction) : self {

        try {
            return $this->action($company, $transaction);
        } catch (Exception $exception) {
            return $this->sendException($exception, 'Ошибка при начислении подарка пользователю', [
                'company' => $this->company->id,
                'transaction' => $this->transaction->id,
            ]);
        }
    }

    private function action($company, $transaction) : self {

        $this->company = $company;
        $this->transaction = $transaction;

        if ($this->check()) {
            if ($this->getGiftMonths()) {
                if ($this->giveGift()) {
                    $this->noty();
                }
            }
        }

        return $this;
    }

    private function noty() {
        $txt = "Начисление подарка пользователю";
        $txt .= "\nПользователь {$this->transaction->user->email}";
        $txt .= "\nОплатил {$this->transaction->cost}₽";
        $txt .= "\nПодарок {$this->giftMonths} месяца";
        NotyService::sendTelegramMessage($txt);
    }

    private function giveGift() : bool {
        if ($this->giftMonths <= 0) {
            $this->setStatus(false, 'Нет подарка');
            return false;
        }
        $this->company->account->active_date = $this->company->account->current_active_date->addMonths($this->giftMonths);
        $saved = $this->company->account->save();
        $this->setStatus($saved, $saved ? 'Подарок успешно начислен' : 'Не удалось начислить подарок');
        return true;
    }

    private function check() : bool {

        if ($this->transaction->status != 'CONFIRMED') {
            $this->setStatus(false, 'Транзакция не подтверждена банком');
            return false;
        }

        if ($this->transaction->product != 'quiz') {
            $this->setStatus(false, 'Продукт не равен quiz');
            return false;
        }

        return true;
    }

    /***
     * Вернет массив падарков в рамках акции
     * Плати за 3 месяца и получи месяц подписки
     * В подарок
     */
    public function getGiftMonths() : int {

        $this->getRateService->handle($this->company);

        $gifts = [
            1 => $this->getRateService->totalCost->month * 3,
            2 => $this->getRateService->totalCost->month * 6,
            3 => $this->getRateService->totalCost->month * 12,
        ];

        $this->giftMonths = 0;

        /**
         * Проходим по каждому подарку и проверяем начислить его или нет
         */
        foreach ($gifts as $giftMonths => $gift) {

            /**
             * Вводим понятие погрешности, если для начисления подарка не хватает
             * +-10 рублей то все равно начислим подарок
             */
            $inaccuracy = $this->transaction->cost - $gift;

            if ($inaccuracy <= 10 && $inaccuracy >= -10) {

                $this->giftMonths = $giftMonths;
            }
        }

        return $this->giftMonths;
    }
}
