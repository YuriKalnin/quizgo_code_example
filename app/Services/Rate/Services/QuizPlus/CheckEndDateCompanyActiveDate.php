<?php

namespace App\Services\Rate\Services\QuizPlus;

use App\Services\Rate\Traits\StatusTrait;
use App\Models\Account;
use App\Events\Rate\ActivityEndDate;
use Illuminate\Support\Collection;

/**
 * Генерируем события когда дата
 * активности компании заканчивается
 */
class CheckEndDateCompanyActiveDate
{
    use StatusTrait;

    public Collection $accounts;

    /**
     * Ищем аккаунты компании без баланса,
     * и дата активности компании должна
     * быть три дня
     */
    public function handle($fire) : self {

        $this->accounts = Account::whereDate('demo_date', '<=', today())
                            ->where('balance', '<', 0)
                            ->whereDate('active_date', '<=', today()->addDays(3))
                            ->whereDate('active_date', '>=', today())
                            ->get();

        $this->accounts->each(function ($account) use ($fire) {
            if ($fire) {
                event(new ActivityEndDate($account));
            }
            dump("account id = " . $account->id . " left days enable = " . ($account->quiz_plus_days_enable));
        });

        return $this;
    }
}
