<?php

namespace App\Services\Rate\Services\GetRateService;

class Clerking
{
    public int $countActs = 0;
    public int $countBills = 0;

    public function handle($company) : self {
        $this->countActs = $company->bills->count();
        $this->countBills = $company->acts->count();
        return $this;
    }
}
