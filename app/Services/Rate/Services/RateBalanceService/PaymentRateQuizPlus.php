<?php

namespace App\Services\Rate\Services\RateBalanceService;

use App\Events\Rate\AfterPayment;
use App\Services\Rate\Services\GetRateService;
use App\Services\Rate\Traits\StatusTrait;
use RateService;

class PaymentRateQuizPlus
{
    use StatusTrait;

    private $companyCollect;
    private $accountCollect;

    public GetRateService $getRateService;

    public function handler($companyCollect) : self {

        return $this->tryCatch(function () use ($companyCollect) {
            $this->companyCollect = $companyCollect;
            $this->accountCollect = $companyCollect->account;
            $this->getRateService = RateService::get($companyCollect);

            if (!$this->validate()) {
                return $this;
            }

            return $this->payment($companyCollect);
        });
    }

    public function getAccount() {
        return $this->accountCollect;
    }

    private function programName() : string
    {
        return 'PaymentRateQuizPlus';
    }

    private function payment($companyCollect) : self {

        $dayTotalCost = $this->getRateService->getDayTotalCost();

        $spendMoneyCompanyBalance = RateService::balance()->spendMoney($companyCollect, 'Автоматическое списание', $dayTotalCost);

        if ($spendMoneyCompanyBalance->isFail()) {

            return $this->setFailStatus($spendMoneyCompanyBalance->message);
        }

        if ($this->updateActiveDate()) {

            event(new AfterPayment($companyCollect, $spendMoneyCompanyBalance->transactionCollect));

            return $this->setStatus(true, 'OK');
        }

        return $this->setStatus(false, 'Не удалось обновить дату активности компании');
    }

    private function updateActiveDate() {
        $this->accountCollect->active_date = $this->accountCollect->next_active_date;
        return $this->accountCollect->save();
    }

    private function validate() : bool {

        if ($this->accountCollect->demo_enable) {
            $this->setStatus(true, 'Подписка в демо режиме');
            return false;
        }

        if (!$this->accountCollect->quiz_plus_need_payment) {
            $this->setStatus(false, 'Подписку продлять не нужно, дата активности больше двух дней');
            return false;
        }

        return true;

    }

    public function getInfo() : object {
        return collect([
            'company' => $this->companyCollect->id,
            'costInDay' => $this->getRateService->totalCost->dayFormat,
            'status' => $this->getStatus(),
        ]);
    }
}
