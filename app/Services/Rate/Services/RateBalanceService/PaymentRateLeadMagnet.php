<?php

namespace App\Services\Rate\Services\RateBalanceService;

use App\Events\Rate\AfterPayment;
use App\Services\Rate\Services\GetRateService;
use App\Services\Rate\Traits\StatusTrait;
use App\Models\Transaction;
use App\Models\Company;
use RateService;

class PaymentRateLeadMagnet
{
    use StatusTrait;

    private Company $company;
    private Transaction $transaction;

    private GetRateService $getRateService;
    private SpendMoneyCompanyBalance $spendMoneyCompanyBalance;

    public function __construct(
        GetRateService $getRateService,
        SpendMoneyCompanyBalance $spendMoneyCompanyBalance
    )
    {
        $this->getRateService = $getRateService;
        $this->spendMoneyCompanyBalance = $spendMoneyCompanyBalance;
    }

    public function handle($company, $transaction) : self {

        $this->company = $company;
        $this->transaction = $transaction;
        $this->getRateService->handle($company);

        if ($transaction->product !== 'lead') {
            $this->setStatus(false, 'Транзакция должна быть с product=lead');
            return $this;
        }

        if (!$transaction->lead) {
            $this->setStatus(false, 'Не найдено кол-во лидов в транзакции');
            return $this;
        }

        /**
         * Определяем сколько нужно списать за лиды
         */
        $costLeads = $this->getRateService->availability->leadMagnet->getCostByCountLeads($transaction->lead);

        /**
         * Списываем с баланса компании сумму за лиды
         */
        $this->spendMoneyCompanyBalance->handle($this->company, "Покупка лидов {$transaction->lead}", $costLeads);

        /**
         * Не удалось списать ДС с баланса компании
         */
        if ($this->spendMoneyCompanyBalance->isFail()) {
            $this->setStatus(false, "Ошибка при списании");
            return $this;
        }

        /**
         * Если дата активности лидов отсутствует
         * то ничего с ней не делаем
         */
        if ($company->account->leads_active_date) {
            /**
             * Если дата активности лидов еще не
             * прошла то к ней прибавим еще месяц
             */
            if ($this->getRateService->availability->leadMagnet->isActiveDate) {
                $company->account->leads_active_date = $company->account->leads_active_date->addMonth();
            } else {
                $company->account->leads_active_date = now()->addMonth();
            }
        }

        $company->account->lead_limit += $transaction->lead;
        $saved = $company->account->save();

        if (!$saved) {
            $this->setStatus(false, 'Не удалось обновить аккаунт компании');
            return $this;
        }

        /**
         * Если все ок, сделаем запись в журнале
         */
        $create = RateService::leadMagnet()->journal()->create($company->account, $transaction, [
            'type' => 'in',
            'name' => 'Покупка лидов',
            'count' => $transaction->lead,
        ]);

        if ($create->isFail()) {
            $this->setStatus($create->status, $create->message);
            return $this;
        }

        $this->setStatus(true, 'Успешно сохранено');

        event(new AfterPayment($company, $this->spendMoneyCompanyBalance->transactionCollect));

        return $this;
    }
}
