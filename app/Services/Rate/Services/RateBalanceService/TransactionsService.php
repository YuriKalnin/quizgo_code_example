<?php

namespace App\Services\Rate\Services\RateBalanceService;

use App\Services\Rate\Traits\StatusTrait;
use Illuminate\Database\Eloquent\Collection;

class TransactionsService
{
    use StatusTrait;

    public Collection $list;

    public function handle($companyCollect) : self {
        $this->list = $companyCollect->account->quizPlusTransactions()->with('parentTransaction')->get();
        $this->setStatus(1, 'OK');
        return $this;
    }
}
