<?php

namespace App\Services\Rate\Services\RateBalanceService;

use App\Models\Transaction;
use App\Models\Company;
use App\Models\Account;
use App\Models\Rate\QuizPlusTransactions;
use App\Services\Rate\Traits\StatusTrait;

/**
 * Начисляет средства на внутренний
 * баланс компании и добавляет транзакцию
 */
class SpendMoneyCompanyBalance
{
    use StatusTrait;

    private float $cost;
    private string $product;

    private Account $accountCollect;
    private Company $companyCollect;

    public QuizPlusTransactions $transactionCollect;

    public function handle($companyCollect, $product, $cost) : self {

        $this->cost = $cost;
        $this->product = $product;
        $this->companyCollect = $companyCollect;
        $this->accountCollect = $companyCollect->account;

        if ($this->validate()) {

            $this->accountCollect->balance -= $this->cost;
            $saved = $this->accountCollect->save();

            if (!$saved) {
                return $this->setStatus(false, 'Ошибка при обновлении баланса');
            }

            $this->transactionCollect = QuizPlusTransactions::create([
                'account_id' => $this->accountCollect->id,
                'name' => $this->product,
                'cost' => $this->cost,
                'type' => 'out',
                'balance' => $this->accountCollect->balance,
            ]);

            if (!$this->transactionCollect->id) {
                return $this->setStatus(false, 'Ошибка при добавлении транзакции');
            }

            return $this->setStatus(true, 'OK');
        }

        return $this;
    }

    private function validate() : bool {

        if ($this->accountCollect->billing_id) {
            $this->setStatus(false, 'К данной компании привязан счет');
            return false;
        }

        if ($this->accountCollect->balance < 0) {
            $this->setStatus(false, 'Недостаточно средств на балансе компании');
            return false;
        }

        if ($this->cost <= 0) {
            $this->setStatus(false, 'Сумма списания не может быть отрицательной или равной нулю');
            return false;
        }

        return true;
    }
}
