<?php

namespace App\Services\Rate\Services\RateBalanceService;

use App\Models\Transaction;
use App\Models\Company;
use App\Models\Account;
use App\Models\Rate\QuizPlusTransactions;
use App\Services\Rate\Traits\StatusTrait;

/**
 * Начисляет средства на внутренний
 * баланс компании и добавляет транзакцию
 */
class AddMoneyCompanyBalance
{
    use StatusTrait;

    private Account $accountCollect;
    private Company $companyCollect;
    private Transaction $parentTransactionCollect;

    public function handle($companyCollect, $parentTransactionCollect) : self {

        $this->companyCollect = $companyCollect;
        $this->accountCollect = $companyCollect->account;
        $this->parentTransactionCollect = $parentTransactionCollect;

        if ($this->validate()) {

            $this->accountCollect->balance += $this->parentTransactionCollect->cost;
            $saved = $this->accountCollect->save();

            if (!$saved) {
                $this->setStatus(false, 'Ошибка при обновлении баланса');
                return $this;
            }

            if ($this->parentTransactionCollect->status === 'MANUAL_RENEWAL_SUCCESS') {
                $name = $this->parentTransactionCollect->message;
            } else {
                $name = $this->parentTransactionCollect->card_billing === 'card_billing' ? 'Оплата с карты' : 'Оплата по счету';
            }

            $createTransaction = QuizPlusTransactions::create([
                'account_id' => $this->accountCollect->id,
                'parent_transaction_id' => $this->parentTransactionCollect->id,
                'name' => $name,
                'cost' => $this->parentTransactionCollect->cost,
                'type' => 'in',
                'balance' => $this->accountCollect->balance,
            ]);

            if (!$createTransaction->id) {
                $this->setStatus(false, 'Ошибка при добавлении транзакции');
                return $this;
            }

            $this->setStatus(true, 'OK');

            return $this;
        }

        return $this;
    }

    private function validate() {

        if ($this->companyCollect->id !== $this->parentTransactionCollect->company_id) {
            $this->setStatus(false, 'Ид компании в родительской транзакции не соответствует переданной компании');
            return false;
        }

        if (
            $this->parentTransactionCollect->status !== 'CONFIRMED'
            && $this->parentTransactionCollect->status !== 'MANUAL_RENEWAL_SUCCESS'
        ) {
            $this->setStatus(false, 'Статус родительской транзакции должен быть CONFIRMED или MANUAL_RENEWAL_SUCCESS');
            return false;
        }

        if ($this->parentTransactionCollect->cost <= 0) {
            $this->setStatus(false, 'Стоимость родительской транзакции должна быть больше нуля');
            return false;
        }

        $duplicate = $this->accountCollect
                ->quizPlusTransactions()
                ->where('parent_transaction_id', $this->parentTransactionCollect->id)
                ->get()
                ->count();

        if ($duplicate > 0) {
//            $this->setStatus(false, 'Родительская транзакция уже есть в списке транзакций');
//            return false;
        }

        return true;
    }
}
