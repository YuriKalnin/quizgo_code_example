<?php

namespace App\Services\Rate\Services;

use App\Models\Account;
use App\Services\Auth\AuthService;

/**
 * Позволяет получить подробную
 * информацию о тарифе компании
 */
class GetRateService
{
    public Account $accountCollect;

    public TotalCostRateService $totalCost;
    public AvailabilityRateService $availability;
    public AuthService $auth;
    public GetRateService\Clerking $clerking;

    public function __construct(
        AvailabilityRateService $availabilityRateService,
        TotalCostRateService $totalCostRateService,
        AuthService $auth,
        GetRateService\Clerking $clerking
    )
    {
        $this->availability = $availabilityRateService;
        $this->totalCost = $totalCostRateService;
        $this->clerking = $clerking;
        $this->auth = $auth;
    }

    public function handle($companyCollect) : self {

        $this->accountCollect = $companyCollect->account;

        $this->auth->handle();

        /**
         * Считаем сумму подписки в месяц по компании
         */
        $this->totalCost->handle($companyCollect);

        /**
         * Определяем доступность тарифа компании
         */
        $this->availability->handle($this->accountCollect, $this->totalCost->day);

        /**
         * Получаем информацию о документах бух. учёта
         */
        $this->clerking->handle($companyCollect);

        return $this;
    }

    public function getDayTotalCost() {
        return $this->totalCost->day;
    }
}
