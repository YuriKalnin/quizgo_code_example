<?php

namespace App\Services\Rate\Services;

use App\Services\Rate\Services\RateBalanceService\SpendMoneyCompanyBalance;
use App\Services\Rate\Traits\StatusTrait;
use App\Events\Rate\AfterBuying;

/**
 * Сервис содержит методы для
 * работы с балансом внутри компании
 */
class RateBalanceService
{
    use StatusTrait;

    public RateBalanceService\AddMoneyCompanyBalance $addMoneyCompanyBalance;
    public RateBalanceService\PaymentRateQuizPlus $paymentRateQuizPlus;
    public RateBalanceService\TransactionsService $transactionsService;
    public RateBalanceService\PaymentRateLeadMagnet $paymentRateLeadMagnet;
    public SpendMoneyCompanyBalance $spendMoneyCompanyBalance;
    public BaseRateService $baseRateService;

    public function __construct(
        RateBalanceService\AddMoneyCompanyBalance $addMoneyCompanyBalance,
        RateBalanceService\PaymentRateQuizPlus $paymentRateQuizPlus,
        RateBalanceService\TransactionsService $transactionsService,
        RateBalanceService\PaymentRateLeadMagnet $paymentRateLeadMagnet,
        BaseRateService $baseRateService,
        SpendMoneyCompanyBalance $spendMoneyCompanyBalance
    )
    {
        $this->addMoneyCompanyBalance = $addMoneyCompanyBalance;
        $this->paymentRateQuizPlus = $paymentRateQuizPlus;
        $this->transactionsService = $transactionsService;
        $this->baseRateService = $baseRateService;
        $this->paymentRateLeadMagnet = $paymentRateLeadMagnet;
        $this->spendMoneyCompanyBalance = $spendMoneyCompanyBalance;
    }

    /**
     * Начислить деньги на счет компании
     * @param $companyCollect
     * @param $parentTransactionCollect
     * @return RateBalanceService\AddMoneyCompanyBalance
     */
    public function addMoney($companyCollect, $parentTransactionCollect) : RateBalanceService\AddMoneyCompanyBalance {
        return $this->addMoneyCompanyBalance->handle($companyCollect, $parentTransactionCollect);
    }

    /**
     * Списать деньги со счета компании
     * @param $companyCollect
     * @param $product
     * @param $cost
     * @return SpendMoneyCompanyBalance
     */
    public function spendMoney($companyCollect, $product, $cost) : SpendMoneyCompanyBalance {

        return $this->spendMoneyCompanyBalance->handle($companyCollect, $product, $cost);
    }

    /**
     * Вызывается после успешной оплаты сервиса картой или по счету
     * @param $company
     * @param $transaction
     * @return
     */
    public function afterSuccessPayment($company, $transaction) {

        /**
         * Проводим транзакцию, начисляем средства на баланс компании
         */
        $this->addMoneyCompanyBalance->handle($company, $transaction);

        /**
         * Если транзакцию провести не удалось, вернем ошибку
         */
        if ($this->addMoneyCompanyBalance->isFail()) {
            return $this->addMoneyCompanyBalance;
        }

        /**
         * Если тариф лидмагнит запустим сервис
         * по начислению лидов
         */
        if ($this->baseRateService->isLeadMagnet($company)) {
            $this->paymentRateLeadMagnet->handle($company, $transaction);
        }

        /**
         * Пробуем завести автоматическое спасания
         * оплаты за пакеты и услуги в компании
         */
        $this->paymentRateQuizPlus->handler($company);

        event(new AfterBuying($company->account, $transaction));

        return $this;
    }

    /**
     * Списать деньги с внутреннего счета компании за
     * пакеты услуг и продлить дату активности компании
     * @return RateBalanceService\PaymentRateQuizPlus
     */
    public function payment($companyCollect) : RateBalanceService\PaymentRateQuizPlus {
        return $this->paymentRateQuizPlus->handler($companyCollect);
    }

    public function transactions($companyCollect) {
        return $this->transactionsService->handle($companyCollect);
    }
}
