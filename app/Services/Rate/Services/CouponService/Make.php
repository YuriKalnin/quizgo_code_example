<?php

namespace App\Services\Rate\Services\CouponService;

use App\Services\Rate\Traits\StatusTrait;
use App\Models\MakeCoupon;
use App\Models\Company;
use App\Models\Coupon;
use App\Models\User;
use RateService;

class Make
{
    use StatusTrait;

    private string $coupon;
    private User $userCollect;
    private Coupon $couponCollect;
    private Company $companyCollect;
    private MakeCoupon $makeCouponCollect;

    public function handle($user, $company, $coupon) : self {

        $this->coupon = $coupon;
        $this->userCollect = $user;
        $this->companyCollect = $company;

        if (!$this->check()) {
            return $this;
        }

        if (!$this->make()) {
            return $this->setFailStatus('Не удалось применить купон');
        }

        $rateService = RateService::activeDate()->update($company, $this->couponCollect->days);

        if ($rateService->isFail()) {
            return $this->setFailStatus($rateService->message);
        }

        return $this->setOkStatus($this->couponCollect->success_text);
    }

    private function check() : bool {

        if ($coupon = $this->findCoupon()) {
            $this->couponCollect = $coupon;
        } else {
            $this->setFailStatus("Купон {$this->coupon} не найден");
            return false;
        }

        if ($this->duplicateCouponByUser()) {
            $this->setFailStatus("Купон {$this->coupon} уже применен для пользователя {$this->userCollect->email}");
            return false;
        }

        if ($this->duplicateCouponByCompany()) {
            $this->setFailStatus("Купон {$this->coupon} уже применен для компании {$this->companyCollect->id}");
            return false;
        }

        return true;
    }

    private function duplicateCouponByUser() : int {
        return MakeCoupon::where('coupon_id', $this->couponCollect->id)
            ->where('user_id', $this->userCollect->id)
            ->get()
            ->count();
    }

    private function duplicateCouponByCompany() : int {
        return MakeCoupon::where('coupon_id', $this->couponCollect->id)
            ->where('company_id', $this->companyCollect->id)
            ->get()
            ->count();
    }

    private function findCoupon() {
        return Coupon::where('coupon', $this->coupon)
                    ->where('enable', 1)
                    ->whereDate('date_end', '>=', now())
                    ->get()
                    ->first();
    }

    private function make() {
        $this->makeCouponCollect = MakeCoupon::create([
            'user_id' => $this->userCollect->id,
            'coupon_id' => $this->couponCollect->id,
            'company_id' => $this->companyCollect->id,
        ]);
        return isset($this->makeCouponCollect->id);
    }
}
