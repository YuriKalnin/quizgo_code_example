<?php

namespace App\Services\Rate\Services;

use App\Models\Company;
use App\Models\Account;

class BaseRateService
{
    /**
     * Если тариф квиз плюс вернет true
     * @param Company | Account $collection
     * @return bool
     */
    public function isQuizPlus($collection) : bool {
        if ($collection instanceof Company) {
            return $collection->account->rate === 'quiz';
        }
        if ($collection instanceof Account) {
            return $collection->rate === 'quiz';
        }
        return false;
    }

    /**
     * Если тариф лид магнит вернет true
     * @param $company
     * @return bool
     */
    public function isLeadMagnet($company) : bool {
        return $company->account->rate === 'lead';
    }

    /**
     * Вернет true если дата активности
     * тарифа компании еще не прошла
     * @param $accountCollect
     * @return bool
     */
    public function isActive($accountCollect) : bool {
        return today()->diffInDays($accountCollect->active_date, false) > 0;
    }

    /**
     * Вернет true если компания в демо режиме
     * @param $accountCollect
     * @return bool
     */
    public function isDemo($accountCollect) : bool {
        return today()->diffInDays($accountCollect->demo_date, false) > 0;
    }

    /**
     * Вернет true если на аккаунте компании есть доступные лиды
     * @param $accountCollect
     * @return bool
     */
    public function hasEnableLeads($accountCollect) : bool {
        return (int) $accountCollect->lead_limit > 0;
    }

    /**
     * Определяет разрешено ли получение заявок из квиза
     * Если демо период тогда всегда возвращается true
     * На тарифе лид магнит возвращается true если у компании есть активные заявки
     * На тарифе квиз плюс возвращается true если дата активности еще не прошла
     * @param $accountCollect
     * @return bool
     */
    public function isEnable($accountCollect) : bool {
        if ($this->isDemo($accountCollect)) {
            return true;
        }
        if ($this->isQuizPlus($accountCollect)) {
            return $this->isActive($accountCollect);
        } else {
            return $this->hasEnableLeads($accountCollect);
        }
    }

    /**
     * Обнуляет демо период задавая дате демо периода вчерашний день
     * @param $accountCollect
     * @return bool|mixed
     */
    public function resetDemo($accountCollect) {
        if ($this->isDemo($accountCollect)) {
            $accountCollect->demo_date = now()->subDay();
            return $accountCollect->save();
        }
        return true;
    }

}
