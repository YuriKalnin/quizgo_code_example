<?php

namespace App\Services\Rate\Services;

use App\Services\Rate\Services\QuizPrices\QuizPricesService;
use App\Services\Rate\Services\ClipsPrices\ClipsPricesService;
use App\Services\Rate\Services\ProductsPrices\ProductPricesService;
use App\Services\Rate\Traits\MathRateTrait;

/**
 * Считает общую сумму подписки
 * по компании в месяц и в день
 */
class TotalCostRateService
{
    use MathRateTrait;

    private $companyCollect;

    public QuizPricesService $quizPrices;
    public ClipsPricesService $clipsPrices;
    public ProductPricesService $productsPrices;

    public $month = 0;
    public $day = 0;
    public $monthFormat = '0';
    public $dayFormat = '0';

    public function __construct(
        QuizPricesService $quizPrices,
        ClipsPricesService $clipsPrices,
        ProductPricesService $productsPrices
    )
    {
        $this->quizPrices = $quizPrices;
        $this->clipsPrices = $clipsPrices;
        $this->productsPrices = $productsPrices;
    }

    /**
     * Основной метод сервиса,
     * получает стоимость тарифа в месяц / день
     * за квизы и улучшениям в месяц в отдельности
     * суммирует в общую стоимость тарифа в месяц / день
     * @return $this
     */
    public function handle($companyCollect) : self {

        $this->setData($companyCollect);

        /**
         * Получим стоимость тарифа за квизы в месяц / день
         */
        $this->quizPrices->handle($this->companyCollect);

        /**
         * Получим стоимость тарифа за клипсы в месяц / день
         */
        $this->clipsPrices->handle($this->companyCollect);

        /**
         * Получим стоимость за улучшения в компании в месяц / день
         */
        $this->productsPrices->handle($this->companyCollect);

        $this->sum();

        return $this;
    }

    private function setData($companyCollect) : self {

        $this->day = 0;
        $this->month = 0;
        $this->dayFormat = '0';
        $this->monthFormat = '0';
        $this->companyCollect = $companyCollect;

        return $this;
    }

    private function sum() : self {
        $this->month += $this->quizPrices->month;
        $this->month += $this->clipsPrices->month;
        $this->month += $this->productsPrices->month;

        $this->day += $this->quizPrices->day;
        $this->day += $this->clipsPrices->day;
        $this->day += $this->productsPrices->day;

        $this->day = round($this->day, 2);
        $this->month = round($this->month, 2);

        $this->dayFormat = $this->moneyFormat($this->day);
        $this->monthFormat = $this->moneyFormat($this->month);

        return $this;
    }
}
