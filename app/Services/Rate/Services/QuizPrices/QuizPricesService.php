<?php

namespace App\Services\Rate\Services\QuizPrices;

use App\Services\Rate\Traits\MathRateTrait;
/**
 * Умеет считать актуальную стоимость
 * оплаты за кол-во квиз-опросов
 * с течением времени цены менялись
 */
class QuizPricesService
{
    use MathRateTrait;

    public $priceTable;

    public int $count = 0;
    public float $month = 0;
    public float $day = 0;
    public string $monthFormat = '0';
    public string $dayFormat = '0';

    /**
     * Массив таблиц цен за
     * разные временные периоды
     * @var string[]
     */

    private $prices = [
        QuizPrices06_2022::class,
        QuizPrices04_2022::class,
        QuizPrices07_2021::class,
        QuizPrices10_2020::class,
        QuizPrices01_2019::class,
    ];

    /**
     * Ищем актуальные цены для
     * данной компании и пользователя
     * @return mixed|void
     */
    public function handle($company)
    {
        if ($company->account->rate === 'lead') {
            $this->count = $company->count_all_quiz;
            return $this;
        }

        foreach ($this->prices as $quizPricesTable) {

            $quizPricesTable = (new $quizPricesTable($company));

            if ($quizPricesTable->isActualPrices()) {
                $this->priceTable = $quizPricesTable->getPrices();

                $this->month = $this->priceTable->month;
                $this->count = $this->priceTable->count;
                $this->day = $this->priceTable->day;

                $this->dayFormat = $this->moneyFormat($this->day);
                $this->monthFormat = $this->moneyFormat($this->month);

                return $this;
            }
        }
    }
}
