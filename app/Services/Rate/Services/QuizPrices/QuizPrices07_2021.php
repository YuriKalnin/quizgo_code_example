<?php

namespace App\Services\Rate\Services\QuizPrices;
use App\Services\Rate\Services\QuizPrices\Contracts\QuizPricesContract;
use Carbon\Carbon;

/**
 * Содержит информации по ценам за кол-во
 * квизов для пользователей создавших аккаунты
 * после 2021-07-08
 */
class QuizPrices07_2021 extends QuizPricesBase implements QuizPricesContract
{
    public function activeType(): string
    {
        return '07_2021';
    }

    public function prices(): array
    {
        return [
            'limit' => [
                'from' => 10,
                'price' => 3990,
                'more' => 299,
            ],
            'list' => [
                0 => [
                    'all' => 0,
                    'one' => 0,
                ],
                // ---------------------------------------------
                1 => [
                    'all' => 499,
                    'one' => 499,
                ],
                2 => [
                    'all' => 998,
                    'one' => 499,
                ],
                3 => [
                    'all' => 1497,
                    'one' => 499,
                ],
                4 => [
                    'all' => 1996,
                    'one' => 499,
                ],
                5 => [
                    'all' => 2495,
                    'one' => 499,
                ],
                // ---------------------------------------------
                6 => [
                    'all' => 2394,
                    'one' => 399,
                ],
                7 => [
                    'all' => 2793,
                    'one' => 399,
                ],
                8 => [
                    'all' => 3192,
                    'one' => 399,
                ],
                9 => [
                    'all' => 3591,
                    'one' => 399,
                ],
                10 => [
                    'all' => 3990,
                    'one' => 399,
                ],
            ]
        ];
    }
}
