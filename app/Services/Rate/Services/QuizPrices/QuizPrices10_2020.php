<?php

namespace App\Services\Rate\Services\QuizPrices;

use Carbon\Carbon;
use App\Services\Rate\Services\QuizPrices\Contracts\QuizPricesContract;

/**
 * Содержит информации по ценам за кол-во
 * квизов для пользователей создавших аккаунты
 * после 2020-10-15
 */
class QuizPrices10_2020 extends QuizPricesBase implements QuizPricesContract
{
    public function activeType() : string {
        return '10_2020';
    }

    public function prices() : array {
        return [
            'limit' => [
                'from' => 8,
                'price' => 1268,
                'more' => 149,
            ],
            'list' => [
                0 => [
                    'all' => 0,
                    'one' => 0,
                ],
                // ---------------------------------------------
                1 => [
                    'all' => 499,
                    'one' => 499,
                ],
                // ---------------------------------------------
                2 => [
                    'all' => 799,
                    'one' => 199,
                ],
                3 => [
                    'all' => 799,
                    'one' => 199,
                ],
                4 => [
                    'all' => 799,
                    'one' => 199,
                ],
                // ---------------------------------------------
                5 => [
                    'all' => 1199,
                    'one' => 171,
                ],
                6 => [
                    'all' => 1199,
                    'one' => 171,
                ],
                7 => [
                    'all' => 1199,
                    'one' => 171,
                ],
            ]
        ];
    }
}
