<?php

namespace App\Services\Rate\Services\QuizPrices\Contracts;

use Carbon\Carbon;

interface QuizPricesContract
{
    /**
     * Должен вернуть массив цен по кол-ву квизов
     * @return array
     */
    public function prices(): array;

    /**
     * Вернет дату актуальности цен
     * @return String
     */
    public function activeType(): string;

    /**
     * В зависимости от даты регистрации пользователя
     * определяет актуальная цена или нет
     * @return bool
     */
    public function isActualPrices(): bool;
}
