<?php

namespace App\Services\Rate\Services\QuizPrices;

class QuizPricesBase
{
    private $companyCollect;

    public $count = 0;
    public $month = 0;
    public $day = 0;

    public function __construct($companyCollect)
    {
        $this->companyCollect = $companyCollect;
    }

    /**
     * Определяет актуальная ли таблица цен для пользователя
     * в зависимости от даты регистрации пользователя
     * Метод переопределяется в дочернем классе в самой
     * первой таблице цен
     */
    public function isActualPrices() : bool {

        return $this->activeType() === $this->companyCollect->account->quiz_prices;
    }

    /**
     * Вернет стоимость подписки в месяц
     * исходя из таблицы цен определенной в дочернем классе
     * и кол-ва квиз-опросов по тарифу
     * @return $this
     */
    public function getPriceSubscriptionMonth() {

        $countQuizAccount = $this->companyCollect->count_all_quiz;

        $this->month = 0;

        $prices = $this->prices();

        $maxQuizAccount = $prices['limit']['from'];
        $priceMaxQuizAccount = $prices['limit']['price'];
        $morePriceMaxQuizAccount = $prices['limit']['more'];

        if ($countQuizAccount >= $maxQuizAccount) {
            $price = $countQuizAccount-$maxQuizAccount;
            $price = $price * $morePriceMaxQuizAccount;
            $price = $price + $priceMaxQuizAccount;
        } else {
            $price = $prices['list'][$countQuizAccount];
            $price = $price['all'];
        }

        $this->count = $countQuizAccount;
        $this->month = $price;

        return $this;
    }

    /**
     * Вернет стоимость подписки в день
     * исходя из таблицы цен определенной в дочернем классе
     * и кол-ва квиз-опросов по тарифу
     * @return $this
     */
    public function getPriceSubscriptionDay() {

        if (!$this->month) {
            $this->getPriceSubscriptionMonth();
        }

        $this->day = $this->month / (int) date('t');
        $this->day = round($this->day, 2);

        return $this;
    }

    public function getPrices() {
        return $this->getPriceSubscriptionMonth()
                    ->getPriceSubscriptionDay();
    }
}
