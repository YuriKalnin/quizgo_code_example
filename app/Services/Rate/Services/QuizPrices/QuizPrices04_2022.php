<?php

namespace App\Services\Rate\Services\QuizPrices;
use App\Services\Rate\Services\QuizPrices\Contracts\QuizPricesContract;
use Carbon\Carbon;

/**
 * Содержит информации по ценам за кол-во
 * квизов для пользователей создавших аккаунты
 * после 2022-04-01
 */
class QuizPrices04_2022 extends QuizPricesBase implements QuizPricesContract
{
    public function activeType(): string
    {
        return '04_2022';
    }

    public function prices(): array
    {
        return [
            'limit' => [
                'from' => 10,
                'price' => 4990,
                'more' => 399,
            ],
            'list' => [
                0 => [
                    'all' => 0,
                    'one' => 0,
                ],
                // ---------------------------------------------
                1 => [
                    'all' => 599,
                    'one' => 599,
                ],
                2 => [
                    'all' => 1198,
                    'one' => 599,
                ],
                3 => [
                    'all' => 1797,
                    'one' => 599,
                ],
                4 => [
                    'all' => 2396,
                    'one' => 599,
                ],
                5 => [
                    'all' => 2995,
                    'one' => 599,
                ],
                // ---------------------------------------------
                6 => [
                    'all' => 2994,
                    'one' => 499,
                ],
                7 => [
                    'all' => 3493,
                    'one' => 499,
                ],
                8 => [
                    'all' => 3992,
                    'one' => 499,
                ],
                9 => [
                    'all' => 4491,
                    'one' => 499,
                ],
                10 => [
                    'all' => 4990,
                    'one' => 499,
                ],
            ]
        ];
    }
}
