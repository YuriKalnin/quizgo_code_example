<?php

namespace App\Services\Rate\Services\ClipsPrices;

use App\Services\Rate\Traits\MathRateTrait;

class ClipsPricesService
{
    use MathRateTrait;

    private $companyCollect;
    private $prices = [
        0 => 0,
        1 => 499,
        2 => 499,
        3 => 499,
        4 => 499,
        5 => 499,
        6 => 399,
        7 => 499,
        8 => 499,
        9 => 499,
        10 => 499,
        11 => 299,
    ];

    public int $count;
    public float $month;
    public float $day;

    public string $monthFormat;
    public string $dayFormat;

    public function handle($companyCollect) : self
    {
        $this->companyCollect = $companyCollect;

        $countClipsInCompany = $this->companyCollect->count_all_clips;

        if (empty($this->prices[$countClipsInCompany])) {

            $costMonth = $countClipsInCompany * $this->prices[ count($this->prices) - 1 ];
        }
        else {

            $costMonth = $countClipsInCompany * $this->prices[$countClipsInCompany];
        }

        $this->count = $countClipsInCompany;
        $this->month = $costMonth;
        $this->day = round($costMonth / (int) date('t'), 2);

        $this->monthFormat = $this->moneyFormat($this->month);
        $this->dayFormat = $this->moneyFormat($this->day);

        return $this;
    }
}
