<?php

namespace App\Services\Rate\Services\ProductsPrices;

use App\Models\Market\Product;
use App\Services\Rate\Traits\MathRateTrait;

class ProductPricesService
{
    use MathRateTrait;

    private $companyCollect;

    public $products;
    public $month = 0;
    public $day = 0;

    public $monthFormat;
    public $dayFormat;

    public function handle($companyCollect) : self
    {
        $this->setData($companyCollect);

        # получаю все доп. продукты
        $products = Product::select(['id', 'name'])->get();

        # получу все улучшения которые прикреплены к компании
        $productsCompanyCollect = $this->companyCollect->products()->get()->groupBy('product_id');

        # массив вида Product -> Улучшения квизов
        foreach ($products as $product) {

            if (
                isset($productsCompanyCollect[$product->id])
                && count($productsCompanyCollect[$product->id])
            ) {
                $quizProducts = $productsCompanyCollect[$product->id];
            } else {
                continue;
            }

            $month = $quizProducts->sum('price');
            $day = $this->calcPriceDay($quizProducts);

            $product->quizProducts = (object) [
                'show' => false,
                'count' => $quizProducts->count(),
                'month' => $month,
                'day' => $day,
                'monthFormat' => $this->moneyFormat($month),
                'dayFormat' => $this->moneyFormat($day),
            ];

            $product->setHidden([
                'show',
                'price_sale',
                'count_installs_word',
            ]);

            $this->products->push($product);

            $this->month += $product->quizProducts->month;
            $this->day += $product->quizProducts->day;
        }

        $this->day = round($this->day, 2);

        $this->monthFormat = $this->moneyFormat($this->month);
        $this->dayFormat = $this->moneyFormat($this->day);

        return $this;
    }

    private function setData($companyCollect) : self {
        $this->day = 0;
        $this->month = 0;
        $this->products = collect([]);
        $this->companyCollect = $companyCollect;
        return $this;
    }

    private function calcPriceDay($quizProducts) {
        return round($quizProducts->sum('price') / (int) date('t'), 2);
    }
}
