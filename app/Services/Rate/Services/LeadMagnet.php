<?php

namespace App\Services\Rate\Services;

use App\Services\Rate\Services\LeadMagnet\AutoPayment;
use App\Services\Rate\Services\LeadMagnet\AutoRemoveLeadsService;
use App\Services\Rate\Services\LeadMagnet\JournalService;
use App\Services\Rate\Services\LeadMagnet\LeadLimitService;
use App\Services\Rate\Traits\StatusTrait;
use RateService;

class LeadMagnet
{
    use StatusTrait;

    public AutoPayment $autoPayment;
    public JournalService $journalService;
    public LeadLimitService $leadLimitService;
    public AutoRemoveLeadsService $autoRemoveLeadsService;

    public function __construct(
        JournalService $journalService,
        LeadLimitService $leadLimitService,
        AutoRemoveLeadsService $autoRemoveLeadsService,
        AutoPayment $autoPayment
    ) {
        $this->autoPayment = $autoPayment;
        $this->journalService = $journalService;
        $this->leadLimitService = $leadLimitService;
        $this->autoRemoveLeadsService = $autoRemoveLeadsService;
    }

    public function journal() : JournalService {
        return $this->journalService;
    }

    public function limit() : LeadLimitService {
        return $this->leadLimitService;
    }

    public function autoPayment($options) : AutoPayment {
        return $this->autoPayment->handle($options);
    }

    public function autoRemove($options) : AutoRemoveLeadsService {
        return $this->autoRemoveLeadsService->handle($options);
    }

    public function minusLead($account) : self {
        if ($account->is_lead_magnet && !$account->demo_enable) {
            $account->lead_limit += -1;
            $saved = $account->save();
            return $this->setStatus($saved, $saved ? 'OK' : 'Ошибка при обновлении');
        }
        return $this->setFailStatus('У компании тариф Квиз+');
    }

    public function unblockingLeads($account) : self {

        $disableLeads = $account->company->lead()->where('enable', 0)->get();
        $countDisableLeads = $disableLeads->count();

        if ($countDisableLeads > 0) {
            $rateService = RateService::leadMagnet()->limit()->update($account->company, ($countDisableLeads * -1));
            if ($rateService->isOk()) {
                $disableLeads->each(function ($lead) {
                    $lead->enable = 1;
                    $lead->save();
                });
                return $this->setOkStatus();
            }
            return $this->setFailStatus();
        }

        return $this->setOkStatus('У компании нет заблокированных заявок');
    }
}
