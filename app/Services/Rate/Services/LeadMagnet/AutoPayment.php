<?php

namespace App\Services\Rate\Services\LeadMagnet;

use App\Services\Traits\StatusTrait;

use App\Models\Account;
use PayService;
use RateService;
use Log;

/**
 * Автоматическая покупка лидов с привязанных
 * ранее банковских карт пользователей
 */
class AutoPayment
{
    use StatusTrait;

    private array $options;

    private $userTinkoffCards;
    private $leadPackage;

    private string $journal = '';

    public function handle($options) : self {

        $this->options = $options;

        $this->output("Получаем список аккаунтов");

        $accounts = $this->getAccount();

        $this->output("Полечено {$accounts->count()} аккаунтов");

        foreach ($accounts as $account) {

            $this->output("Проверка аккаунта {$account->id}");

            if ($this->isFuture($account)) {
                $this->output("Аккаунт проверялся менее дня назад");
                continue;
            }

            if (!$this->check($account)) {
                continue;
            }

            $this->output("Начинаем попытки оплаты");
            $this->pay($account);
            $this->output(PHP_EOL);
            $this->remember($account);
        }

        $this->log();

        return $this->setOkStatus('Успешно');
    }

    private function remember($account) : self {
        $account->check_count_leads = now()->addDay();
        $account->save();
        return $this;
    }

    private function pay($account) : self {
        foreach ($this->userTinkoffCards->list as $card) {

            $this->output("Попытка оплаты с карты {$card->RebillId}")
                ->sleep();

            $userTinkoffCards = PayService::methods()->cardTinkoff()->pay($account->userAutopayment, [
                'mont' => 0,
                'product' => 'lead',
                'rebill_id' => $card->RebillId,
                'cost' => $this->leadPackage['cost'],
                'lead' => $this->leadPackage['count'],
                'company_id' => $account->company_id,
                'user_id' => $account->userAutopayment->id,
            ]);

            $this->sleep();

            if ($userTinkoffCards->isOk()) {
                $this->output("Оплата прошла успешно сумма {$this->leadPackage['cost']} карта {$card->RebillId}");
                break;
            } else {
                $this->output("Не удалось оплатить сумма {$this->leadPackage['cost']} карта {$card->RebillId}");
            }
        }
        return $this;
    }

    private function sleep() : self {
        sleep(1);
        return $this;
    }

    private function log() : self {
        if (isset($this->options['log']) && $this->options['log']) {
            Log::channel('lead-magnet-auto-payment')->info($this->journal);
        }
        return $this;
    }

    private function output($message) : self {
        $this->journal .= PHP_EOL . $message;
        if (isset($this->options['output']) && $this->options['output']) {
            if (is_string($message)) {
                echo $message.PHP_EOL;
            } else {
                dump($message);
            }
        }
        return $this;
    }

    private function isFuture($account) : bool {
        $checkCountLeads = $account->check_count_leads ?? now()->subDay();
        return $checkCountLeads->isFuture();
    }

    private function check($account) : bool {

        if (!$account->userAutopayment) {
            $this->output("Не найден пользователь под которым производить оплату");
            return false;
        }

        $this->userTinkoffCards = PayService::methods()->cardTinkoff()->getUserCards($account->userAutopayment);

        if (!$this->userTinkoffCards->hasCards()) {
            $this->output("У пользователя нет активных карт");
            return false;
        }

        $rateService = RateService::get($account->company);

        if (!isset($rateService->availability->leadMagnet->leadPackages[0])) {
            $this->output("Не найдены цены для первого пакета лидов");
            return false;
        }

        $this->leadPackage = $rateService->availability->leadMagnet->leadPackages[0];

        return true;
    }

    private function getAccount() {
        return Account::isEndDemo()
            ->where('lead_limit', '<=', 10)
            ->whereNull('billing_id')
            ->where('rate', 'lead')
            ->where('auto_payment', '>', 0)
            ->get();
    }
}
