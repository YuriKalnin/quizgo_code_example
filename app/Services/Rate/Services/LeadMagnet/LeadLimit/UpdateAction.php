<?php

namespace App\Services\Rate\Services\LeadMagnet\LeadLimit;

use App\Services\Rate\Traits\StatusTrait;
use RateService;

class UpdateAction
{
    use StatusTrait;

    private $company;
    private int $leadCount;

    public function handle($company, $leadCount) : self {

        $this->company = $company;
        $this->leadCount = $leadCount;

        $company->account->lead_limit += $leadCount;
        $saved = $company->account->save();

        if (!$saved) {
            return $this->setFailStatus('Не удалось сохранить');
        }

        RateService::leadMagnet()->journal()->create($company->account, null, [
            'type' => 'in',
            'name' => $this->getJournalName($leadCount),
            'count' => $leadCount,
        ]);

        return $this->setOkStatus('Успешно сохранено');
    }

    private function getJournalName($leadCount) {
        return $leadCount > 0 ? 'Ручное начисление лидов' : 'Ручное списание лидов';
    }

    public function getResult() : object {
        return (object) [
            'result' => $this->getStatus(),
            'companyID' => $this->company->id,
            'leadCount' => $this->leadCount,
        ];
    }

    public function getResultJson() : string {
        return json_encode($this->getResult());
    }
}
