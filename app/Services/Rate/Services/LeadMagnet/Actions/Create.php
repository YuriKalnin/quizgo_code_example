<?php

namespace App\Services\Rate\Services\LeadMagnet\Actions;

use App\Models\Account;
use App\Models\Transaction;

use App\Models\Rate\LeadMagnetJournal;
use App\Services\Rate\Traits\StatusTrait;

class Create
{
    use StatusTrait;

    private object $data;
    private Account $account;
    private $transaction;

    public LeadMagnetJournal $leadMagnetJournal;

    public function handle($account, $transaction, $data) : self {
        $this->data = (object) $data;
        $this->account = $account;
        $this->transaction = $transaction;

        $insertData = [
            'account_id' => $account->id,
            'account_lead_limit' => $account->lead_limit,
            'parent_transaction_id' => is_object($transaction) ? $transaction->id : null,
            'type' => $this->data->type,
            'name' => $this->data->name,
            'count' => $this->data->count,
        ];

        $this->leadMagnetJournal = LeadMagnetJournal::create($insertData);

        $status = isset($this->leadMagnetJournal->id) && $this->leadMagnetJournal->id;

        $this->setStatus($status, $status ? 'Успешно сохранено' : 'Ошибка при сохранении');

        return $this;
    }
}
