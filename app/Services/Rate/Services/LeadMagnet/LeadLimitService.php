<?php

namespace App\Services\Rate\Services\LeadMagnet;

class LeadLimitService
{
    public LeadLimit\UpdateAction $updateAction;

    public function __construct(LeadLimit\UpdateAction $updateAction) {
        $this->updateAction = $updateAction;
    }

    public function update($company, $leadCount) {
        return $this->updateAction->handle($company, $leadCount);
    }
}
