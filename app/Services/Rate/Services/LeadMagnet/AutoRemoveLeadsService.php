<?php

namespace App\Services\Rate\Services\LeadMagnet;

use App\Models\Account;
use RateService;

class AutoRemoveLeadsService
{
    public $options;

    public function handle($options) : self {

        $this->options = $options;

        $accounts = Account::where('lead_limit', '>', 0)->whereDate('leads_active_date', '<=', now())->get();

        foreach ($accounts as $account) {
            if ($account->count_days_active_lead <= -1) {
                $rateService = RateService::leadMagnet()->limit()->update($account->company, $account->lead_limit * -1);
                $this->output($rateService->getResultJson());
            }
        }

        return $this;
    }

    private function output($message) : string {
        if (isset($this->options['output']) && $this->options['output']) {
            dump($message);
        }
        return $message;
    }
}
