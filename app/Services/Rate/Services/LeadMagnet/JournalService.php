<?php

namespace App\Services\Rate\Services\LeadMagnet;

use App\Services\Rate\Traits\StatusTrait;
use Illuminate\Database\Eloquent\Collection;
use Exception;

class JournalService
{
    use StatusTrait;

    private Actions\Create $create;

    public Collection $leadMagnetJournal;

    public function __construct(Actions\Create $create)
    {
        $this->create = $create;
    }

    public function create($account, $transaction, $data) : Actions\Create {
        return $this->create->handle($account, $transaction, $data);
    }

    public function get($company) : self {
        try{
            $this->leadMagnetJournal = $company->account->leadMagnetJournal()->with('parentTransaction')->get();
            $this->setStatus(true, 'Журнал получен');
        } catch (Exception $exception) {
            $this->setStatus(false, $exception->getMessage());
        }
        return $this;
    }
}
