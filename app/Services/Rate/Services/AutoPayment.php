<?php

namespace App\Services\Rate\Services;

use RateService;
use App\Services\Rate\Traits\StatusTrait;
use App\Models\Account;
use App\Events\Rate\SmallCompanyBalance;
use App\Services\Rate\Services\RateBalanceService\PaymentRateQuizPlus;

class AutoPayment
{
    use StatusTrait;

    private array $options;

    public function handle($options) : self {

        $this->options = $options;

        $accounts = Account::hasBalance()->isEndDemo()->get();

        foreach ($accounts as $account) {
            $rateService = RateService::balance()->payment($account->company);
            $this->pushEvents($rateService);
            $this->output($rateService->getInfo());
        }

        return $this;
    }

    private function pushEvents(PaymentRateQuizPlus $rateService) {

        if (!$rateService->status) {
            return false;
        }
        if (!isset($rateService->getRateService->availability->payed->forecast->days)) {
            return false;
        }
        if (!isset($rateService->getRateService->totalCost->month)) {
            return false;
        }
        $leftDays = $rateService->getRateService->availability->payed->forecast->days;
        $cost = $rateService->getRateService->totalCost->month;

        /**
         * Денег хватит на внутреннем счете хватит на 7 дней
         */
        if ((int) $leftDays === 7) {
            event(new SmallCompanyBalance($rateService->getAccount(), 7, $cost));
        }

        if ((int) $leftDays === 5) {
            event(new SmallCompanyBalance($rateService->getAccount(), 5, $cost));
        }

        if ((int) $leftDays === 3) {
            event(new SmallCompanyBalance($rateService->getAccount(), 3, $cost));
        }

        if ((int) $leftDays === 1) {
            event(new SmallCompanyBalance($rateService->getAccount(), 1, $cost));
        }
    }

    private function output($message) {
        if (isset($this->options['output']) && $this->options['output']) {
            dump($message);
        }
        return $message;
    }
}
