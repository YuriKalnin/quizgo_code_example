<?php

namespace App\Services\Rate\Services;

use App\Services\Rate\Traits\StatusTrait;
use Exception;
use App\Models\Account;

class ChangeRateService
{
    use StatusTrait;

    public Account $accountCollect;

    public function handle($companyCollect, $rate) : self {

        try {
            $this->accountCollect = $companyCollect->account;
            $this->accountCollect->rate = $rate;
            // если переходим на тариф лидМагнит
            if ($rate === 'lead') {
                // если нет даты активности лидов, сделаем ее вчерашним днем
                if (!$this->accountCollect->leads_active_date) {
                    $this->accountCollect->leads_active_date = now()->subDay();
                }
                // если дата активности лидов уже прошла, удалим лиды
                if (!$this->accountCollect->leads_active_date->isFuture()) {
                    $this->accountCollect->lead_limit = 0;
                }
            }
            $this->status = $this->accountCollect->save();
            $this->message = $this->status ? 'Тариф успешно изменен' : 'Ошибка при изменении тарифа';
            $this->accountCollect->setHidden([
                'company'
            ]);
        } catch (Exception $exception) {
            $this->setFailStatus($exception->getMessage());
        }

        return $this;
    }
}
