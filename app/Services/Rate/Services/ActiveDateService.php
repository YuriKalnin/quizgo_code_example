<?php

namespace App\Services\Rate\Services;

use App\Services\Rate\Traits\StatusTrait;
use Carbon\Carbon;
use CrmService;
use CacheService;

class ActiveDateService
{
    use StatusTrait;

    public function update($company, $days) {

        $account = $company->account;
        $account->demo_date = Carbon::today()->subDay();

        $activeDate = $account->activeDateExpired() ? today() : $account->active_date;

        if ($days > 0) {
            $activeDate->addDays(abs($days));
        } else {
            $activeDate->subDays(abs($days));
        }

        $account->active_date = $activeDate->format('Y-m-d');
        $saved = $account->save();

        $this->setStatus($saved, $saved ? 'Успешно обновлено' : 'Ошибка при обновлении');

        CacheService::clearCacheAccountCompany($company->id);
        CrmService::switchEnabledLeadCompany($company);

        return $this;
    }
}
