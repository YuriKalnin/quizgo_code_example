<?php

namespace App\Services\Rate\Services;

class QuizPlusService
{
    private QuizPlus\QuizPlusGiftService $quizPlusGiftService;
    private QuizPlus\CheckEndDateCompanyActiveDate $checkEndDateCompanyActiveDate;

    public function __construct(
        QuizPlus\QuizPlusGiftService $quizPlusGiftService,
        QuizPlus\CheckEndDateCompanyActiveDate $checkEndDateCompanyActiveDate
    )
    {
        $this->quizPlusGiftService = $quizPlusGiftService;
        $this->checkEndDateCompanyActiveDate = $checkEndDateCompanyActiveDate;
    }

    public function giveGift($company, $transaction) : QuizPlus\QuizPlusGiftService{
        return $this->quizPlusGiftService->handle($company, $transaction);
    }

    public function checkEndDateCompanyActiveDate($fire) : QuizPlus\CheckEndDateCompanyActiveDate{
        return $this->checkEndDateCompanyActiveDate->handle($fire);
    }
}
