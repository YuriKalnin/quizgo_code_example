<?php

namespace App\Services\Rate\Services\AvailabilityRateService;

use App\Services\Rate\Services\BaseRateService;
use Lang;
use Date;

/**
 * Производит расчеты
 * доступности тарифа
 */
class DemoAvailableService
{
    private $accountCollect;
    private BaseRateService $baseRateService;

    public $days;
    public $words;
    public $enable;
    public $activeTo;

    public function __construct(
        BaseRateService $baseRateService
    )
    {
        $this->baseRateService = $baseRateService;
    }

    public function handle($accountCollect) : self
    {
        $this->accountCollect = $accountCollect;
        $this->activeTo = $this->accountCollect->demo_date_format;
        $this->days = $this->accountCollect->demo_days_enable;
        $this->words = $this->accountCollect->demo_date_format_ago;
        $this->enable = $this->accountCollect->demo_enable;

        return $this;
    }
}
