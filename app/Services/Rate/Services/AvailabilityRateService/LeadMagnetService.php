<?php
namespace App\Services\Rate\Services\AvailabilityRateService;

use App\Models\Account;
use Date;
use App\Services\Rate\Services\AvailabilityRateService\LeadMagnetService\Prices\Prices12Dec2022;

class LeadMagnetService
{
    private Account $account;

    public $activeDateFormat = false;
    public $activeDateEnable = false;
    public $isActiveDate = false;

    public $leadPackages = [
        [
            'sale' => 0,
            'count' => 30,
            'cost' => 30 * 14,
            'oneCost' => 14,
            'hit' => false,
        ],
        [
            'sale' => 42,
            'count' => 100,
            'cost' => 100 * 8,
            'oneCost' => 8,
            'hit' => false,
        ],
        [
            'sale' => 57,
            'count' => 200,
            'cost' => 200 * 6,
            'oneCost' => 6,
            'hit' => true,
        ],
        [
            'sale' => 60,
            'count' => 300,
            'cost' => 300 * 5.5,
            'oneCost' => 5.5,
            'hit' => false,
        ],
        [
            'sale' => 64,
            'count' => 500,
            'cost' => 500 * 5.0,
            'oneCost' => 5.0,
            'hit' => false,
        ],
        [
            'sale' => 71,
            'count' => 1000,
            'cost' => 1000 * 4,
            'oneCost' => 4,
            'hit' => false,
        ],
        [
            'sale' => 79,
            'count' => 1500,
            'cost' => 1500 * 3,
            'oneCost' => 3,
            'hit' => false,
        ],
        [
            'sale' => 86,
            'count' => 5000,
            'cost' => 5000 * 2,
            'oneCost' => 2,
            'hit' => false,
        ],
    ];

    public function handle($account) {
        $this->account = $account;
        $this->setActualPrices();
        $this->setIsActiveDate();
        $this->setActiveDateEnable();
        $this->setActiveDateFormat();
        return $this;
    }

    public function setActualPrices() {
        if ($this->account->rate_leads === '12_2022') {
            $this->leadPackages = (new Prices12Dec2022)->handle();
        }
    }

    public function getCostByCountLeads($count) {
        foreach ($this->leadPackages as $package) {
            if ($count === $package['count']) {
                return $package['cost'];
            }
        }
        return $this->leadPackages[0]['cost'];
    }

    private function setActiveDateFormat() {

        if (!$this->account->leads_active_date) {
            $this->activeDateFormat = 'unlimited';
            return $this;
        }

        $this->activeDateFormat = Date::parse($this->account->leads_active_date)->format('j F');

        return $this;
    }

    private function setActiveDateEnable() {

        if (!$this->account->leads_active_date) {
            $this->activeDateEnable = true;
            return $this;
        }

        $this->activeDateEnable = $this->isActiveDate;

        return $this;
    }

    private function setIsActiveDate() {

        if (!$this->account->leads_active_date) {
            $this->isActiveDate = false;
            return $this;
        }

        $this->isActiveDate = today()->diffInDays($this->account->leads_active_date, false) > 0;

        return $this;
    }
}
