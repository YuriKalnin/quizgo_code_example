<?php

namespace App\Services\Rate\Services\AvailabilityRateService\LeadMagnetService\Prices;

class Prices12Dec2022
{
    public function handle() {
        return [
            [
                'sale' => 0,
                'count' => 30,
                'cost' => 499,
                'oneCost' => 16.63,
                'hit' => false,
            ],
            [
                'sale' => 9.8,
                'count' => 60,
                'cost' => 60 * 15,
                'oneCost' => 15,
                'hit' => false,
            ],
            [
                'sale' => 15.8,
                'count' => 90,
                'cost' => 90 * 14,
                'oneCost' => 14,
                'hit' => false,
            ],
            [
                'sale' => 21.8,
                'count' => 120,
                'cost' => 120 * 13,
                'oneCost' => 13,
                'hit' => false,
            ],
            [
                'sale' => 45.9,
                'count' => 400,
                'cost' => 400 * 9,
                'oneCost' => 9,
                'hit' => true,
            ],
            [
                'sale' => 51.9,
                'count' => 600,
                'cost' => 600 * 8,
                'oneCost' => 8,
                'hit' => false,
            ],
            [
                'sale' => 57.9,
                'count' => 800,
                'cost' => 800 * 7,
                'oneCost' => 7,
                'hit' => false,
            ],
            [
                'sale' => 63.9,
                'count' => 1000,
                'cost' => 1000 * 6,
                'oneCost' => 6,
                'hit' => false,
            ],
        ];
    }
}
