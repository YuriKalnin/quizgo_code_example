<?php

namespace App\Services\Rate\Services\AvailabilityRateService\PayedAvailable;

use Date;
use Lang;

/**
 * Считает фактическую дату активности по тарифу квиз+
 * Фактическая дата это поле active_date в таблице account
 */
class FactualPayedRateService
{
    private $accountCollect;

    public $days;
    public $words;
    public $enable;
    public $activeTo;

    public function handle($accountCollect) {

        $this->accountCollect = $accountCollect;

        $this
            ->checkDays()
            ->checkEnable()
            ->checkWord()
            ->checkActiveTo();

        return $this;
    }

    public function checkActiveTo() {
        $this->activeTo = Date::parse($this->accountCollect->active_date)->format('j F');
        return $this;
    }

    public function checkWord() {
        $daysAbs = abs($this->days);
        $words = Lang::choice('день|дня|дней', $daysAbs, [], 'ru');
        if ($this->days === 0) {
            $this->words = 'сегодня';
        } elseif ($this->days < 0) {
            $this->words = "$daysAbs $words назад";
        } else {
            $this->words = "$daysAbs $words";
        }

        return $this;
    }

    public function checkEnable() {
        $this->enable = $this->days > 0;
        return $this;
    }

    public function checkDays() {
        $this->days = today()->diffInDays($this->accountCollect->active_date, false);
        return $this;
    }
}
