<?php

namespace App\Services\Rate\Services\AvailabilityRateService\PayedAvailable;

use Lang;

/**
 * Считает прогнозируемую дату активности по тарифу квиз+
 * Прогнозируемая дата это поле balance поделенное на
 * ежедневную стоимость подписки
 */
class ForecastPayedRateService
{
    private $accountCollect;
    public $days;

    public $words;
    public $enable;
    public $balance;
    public $costDay;

    public function handle($accountCollect, $costDay) {

        $this->accountCollect = $accountCollect;
        $this->costDay = $costDay;
        $this->balance = $accountCollect->balance;

        $this
            ->checkDays()
            ->checkEnable()
            ->checkWord();

        return $this;
    }

    public function checkWord() {
        $words = Lang::choice('день|дня|дней', $this->days, [], 'ru');
        $this->words = "{$this->days} $words";
        return $this;
    }

    public function checkEnable() {
        $this->enable = $this->days > 0;
        return $this;
    }

    public function checkDays() {

        if ($this->accountCollect->balance <= 0) {
            $this->days = 0;
            return $this;
        }
        if ($this->costDay <= 0) {
            $this->days = 0;
            return $this;
        }

        $this->days = ceil($this->accountCollect->balance / $this->costDay);
        return $this;
    }
}
