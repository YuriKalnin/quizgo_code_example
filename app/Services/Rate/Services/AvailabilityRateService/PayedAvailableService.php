<?php

namespace App\Services\Rate\Services\AvailabilityRateService;

use App\Services\Rate\Services\QuizPrices\QuizPricesService;
use App\Services\Rate\Services\ClipsPrices\ClipsPricesService;
use App\Services\Rate\Services\ProductsPrices\ProductPricesService;
use Carbon\Carbon;
use Lang;
use Date;

/**
 * Производит расчеты
 * доступности тарифа
 */
class PayedAvailableService
{
    private $accountCollect;
    private $activeDate;

    public $factual;
    public $forecast;

    public $days;
    public $words;
    public $enable;
    public $activeTo;

    public function __construct(
        PayedAvailable\FactualPayedRateService $factualPayedRate,
        PayedAvailable\ForecastPayedRateService $forecastPayedRate
    )
    {
        $this->factual = $factualPayedRate;
        $this->forecast = $forecastPayedRate;
    }

    public function handle($accountCollect, $costDay)
    {
        $this->accountCollect = $accountCollect;

        $this->factual->handle($this->accountCollect);
        $this->forecast->handle($this->accountCollect, $costDay);

        $this
            ->checkDays()
            ->checkEnable()
            ->checkWord()
            ->checkActiveTo();

        return $this;
    }

    public function checkActiveTo() {
        $this->activeTo = Date::parse($this->activeDate)->format('j F');
        return $this;
    }

    public function checkWord() {
        $daysAbs = abs($this->days);
        $words = Lang::choice(__('rate.days-word'), $daysAbs);
        if ($this->days === 0) {
            $this->words = __('rate.today');
        } elseif ($this->days < 0) {
            $this->words = "$daysAbs $words " . __('rate.ago');
        } else {
            $this->words = "$daysAbs $words";
        }
        return $this;
    }

    public function checkEnable() {
        $this->enable = $this->days > 0;
        return $this;
    }

    public function checkDays() {

        $this->activeDate = $this->accountCollect->active_date;
        $this->activeDate = $this->activeDate->addDays($this->forecast->days);

        $this->days = today()->diffInDays($this->activeDate, false);
        return $this;
    }
}
