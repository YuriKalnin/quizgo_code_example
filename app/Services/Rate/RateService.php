<?php

namespace App\Services\Rate;

/**
 * Сервис для работы с тарифами
 */
class RateService
{
    /**
     * Позволяет получить подробную информацию о тарифе компании
     * @var Services\GetRateService
     */
    public Services\GetRateService $getRateService;

    /**
     * Позволяет поменять тариф компании
     * @var Services\ChangeRateService
     */
    public Services\ChangeRateService $changeRateService;

    /**
     * Позволяет начислить или списать деньги со счета
     * @var Services\RateBalanceService
     */
    public Services\RateBalanceService $balanceService;

    /**
     * Умеет продлять дату активности компании на кол-во дней
     * @var Services\ActiveDateService
     */
    public Services\ActiveDateService $activeDateService;

    public Services\AutoPayment $autoPayment;

    public Services\LeadMagnet $leadMagnet;

    public Services\BaseRateService $baseRateService;

    public Services\QuizPlusService $quizPlusService;

    /**
     * Работа с купонами
     * @var Services\CouponService
     */
    public Services\CouponService $couponService;

    public function __construct(
        Services\GetRateService $getRateService,
        Services\ChangeRateService $changeRateService,
        Services\RateBalanceService $balanceService,
        Services\ActiveDateService $activeDateService,
        Services\AutoPayment $autoPayment,
        Services\LeadMagnet $leadMagnet,
        Services\BaseRateService $baseRateService,
        Services\QuizPlusService $quizPlusService,
        Services\CouponService $couponService
    )
    {
        $this->getRateService = $getRateService;
        $this->changeRateService = $changeRateService;
        $this->balanceService = $balanceService;
        $this->activeDateService = $activeDateService;
        $this->autoPayment = $autoPayment;
        $this->leadMagnet = $leadMagnet;
        $this->baseRateService = $baseRateService;
        $this->quizPlusService = $quizPlusService;
        $this->couponService = $couponService;
    }

    /**
     * Вернет подробную информацию по тарифу компании
     * @param $company
     * @return Services\GetRateService
     */
    public function get($company) : Services\GetRateService {
        return $this->getRateService->handle($company);
    }

    /**
     * Меняет тариф
     * @param $company - коллекция компании
     * @param $rate - название тарифа на который нужно поменять, quiz или lead
     * @return Services\ChangeRateService
     */
    public function change($company, $rate) : Services\ChangeRateService {
        return $this->changeRateService->handle($company, $rate);
    }

    /**
     * @return Services\RateBalanceService
     */
    public function balance() : Services\RateBalanceService {
        return $this->balanceService;
    }

    public function activeDate() : Services\ActiveDateService {
        return $this->activeDateService;
    }

    public function autoPayment($options) : Services\AutoPayment {
        return $this->autoPayment->handle($options);
    }

    public function leadMagnet() : Services\LeadMagnet {
        return $this->leadMagnet;
    }

    public function quizPlus() : Services\QuizPlusService {
        return $this->quizPlusService;
    }

    public function base() : Services\BaseRateService {
        return $this->baseRateService;
    }

    public function coupon() : Services\CouponService {
        return $this->couponService;
    }
}
