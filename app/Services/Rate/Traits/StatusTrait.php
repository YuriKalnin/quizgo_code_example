<?php

namespace App\Services\Rate\Traits;

use NotyService;
use Exception;

trait StatusTrait
{
    public bool $status = false;
    public string $message = '';

    private function setStatus($status, $message = '') : self {
        $this->status = $status;
        $this->message = $message;
        return $this;
    }

    private function setFailStatus($message = '') : self {
        $this->setStatus(false, $message);
        return $this;
    }

    private function setOkStatus($message = '') : self {
        $this->setStatus(true, $message);
        return $this;
    }

    public function isFail() : bool {
        return $this->status === false;
    }

    public function isOk() : bool {
        return $this->status === true;
    }

    public function sendException($exception, $message, $data = []) : self {
        try{
            $txt = $message;
            $txt .= "\nСообщение {$exception->getMessage()}";
            $txt .= "\nДанные: " . json_encode($data);
            NotyService::sendTelegramMessage($txt);
            return $this->setStatus(false, $exception->getMessage());
        } catch (Exception $exception) {
            return $this->setStatus(false, $exception->getMessage());
        }
    }

    public function getStatus() : object {
        return (object) [
            'status' => $this->status,
            'message' => $this->message,
        ];
    }

    public function tryCatch($callback) : self {
        try {
            return call_user_func($callback);
        } catch (Exception $exception) {
            if (method_exists($this,'programName')) {
                $message = $this->programName();
            } else {
                $message = 'Произошла ошибка';
            }
            if (method_exists($this,'getInfo')) {
                $data = $this->getInfo();
            } else {
                $data = [];
            }
            return $this->sendException($exception, $message, $data);
        }
    }
}
