<?php

namespace App\Services\Rate\Traits;

trait MathRateTrait
{
    public function moneyFormat($number, $currency = false) : string {
        $value = number_format($number, 2, ',', ' ');
        if ($currency) {
            return "$value $currency";
        }
        return $value;
    }
}
