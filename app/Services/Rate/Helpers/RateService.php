<?php

namespace App\Services\Rate\Helpers;

use App\Services\Rate\Services\CouponService;
use App\Services\Rate\Services\AutoPayment;
use App\Services\Rate\Services\QuizPlusService;
use App\Services\Rate\Services\LeadMagnet;
use App\Services\Rate\Services\BaseRateService;
use App\Services\Rate\Services\ActiveDateService;
use App\Services\Rate\Services\RateBalanceService;
use App\Services\Rate\Services\ChangeRateService;
use App\Services\Rate\Services\GetRateService;

class RateService
{
    public function get($company) : GetRateService {
        return $this->resolve()->get($company);
    }

    public function change($company, $rate) : ChangeRateService {
        return $this->resolve()->change($company, $rate);
    }

    public function balance() : RateBalanceService {
        return $this->resolve()->balance();
    }

    public function activeDate() : ActiveDateService {
        return $this->resolve()->activeDate();
    }

    public function base() : BaseRateService {
        return $this->resolve()->base();
    }

    public function leadMagnet() : LeadMagnet {
        return $this->resolve()->leadMagnet();
    }

    public function quizPlus() : QuizPlusService {
        return $this->resolve()->quizPlus();
    }

    public function autoPayment($options) : AutoPayment {
        return $this->resolve()->autoPayment($options);
    }

    public function coupon() : CouponService {
        return $this->resolve()->coupon();
    }

    public function resolve() {
        return resolve('App\Services\Rate\RateService');
    }
}
