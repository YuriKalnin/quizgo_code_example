<?php

namespace App\Models;

use AdminTablesService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lead extends Model
{
    use SoftDeletes;

    protected $dates = ['created_at', 'deleted_at'];

    protected $appends = ['created_at_format'];

    protected $fillable = [
        'title',
        'name',
        'phone',
        'email',
        'company_id',
        'project_id',
        'quiz_id',
        'status',
        'enable',
        'json_questions',
        'json_answers',
        'json_answers_2',
        'json_result',
        'source'  ,
        'medium'  ,
        'campaign',
        'content' ,
        'term'    ,
        'sale_value',
        'sale_type',
        'messenger',
        'contact_messenger',
        'contact_type',
        'recaptcha_rating',

        'rsi_webhook_status',
        'rsi_webhook_response',
        'rsi_calltouch_status',
        'rsi_calltouch_response',

        'roistat_visit',
        'y_client_id',
        'g_client_id',
        'yclid',
        'gclid',
        'fbclid',
        'fbc',
        'fbp',
        'comagic_session_id',

        'calltouch_session_id',
        'swirl_id',
        'page_url',
        'ip',
        'user_agent',
        'custom_fields',
    ];

    public function quiz()
    {
        return $this->belongsTo('App\Models\Quiz');
    }

    public function swirl()
    {
        return $this->belongsTo('App\Models\Swirl');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\CommentCrm', 'lead_id');
    }

    public function getDefaultContactIsNullAttribute()
    {
        return (boolean) ($this->phone == null) && ($this->email == null);
    }

    public function filterEmoji($str){

        return preg_replace('/[^а-яёa-z0-9\,\/\!\:\;\?\.\(\)\=\+\*\-\s\<\>\_\#]/ui', "", $str);
    }

    public function getCreatedAtFormatAttribute()
    {
        $in = __('crm.in');
        return \Date::parse($this->created_at)->format("j F Y {$in} H:i:s");
    }

    public function getPhoneCleanAttribute(){

        return str_replace([' ', '(', ')', '-'], ['', '', '', ''], $this->phone_format);
    }

    public function getPageUrlDecodedAttribute() {

        if ($this->page_url) {
            return str_replace(['[*]', '[^]'], ['?', '#'], $this->page_url);
        }

        return "";
    }

    public function getCustomFieldsObjectAttribute() {
        try {
            if ($this->custom_fields && $this->custom_fields !== '{}') {
                return json_decode($this->custom_fields);
            }
        } catch (\Exception $exception) {}
        return false;
    }
}
