<?php

namespace App\Models;

use App\Models\Integrations\FacebookConversionsApiEvent;
use App\Models\Integrations\RoistatIntegration;
use CacheService;
use Illuminate\Database\Eloquent\Model;
use AdminTablesService;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;

class Quiz extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $appends = [
        'count_new_leads',
    ];

    protected $table = 'quiz';

    protected $fillable = [
        'name',
        'author_id',
        'project_id',
        'quiz_json',
        'white_label',
        'css_theme',
        'count_leads',
    ];

    public function style(){

        return $this->hasOne('App\Models\Quiz\Styles');
    }

    public function crmBx24()
    {
        return $this->hasOne('App\Models\CrmBitrix24');
    }

    public function yandexMetrica()
    {
        return $this->hasOne('App\Models\YandexMetricaIntegration');
    }

    public function googleAnalitics()
    {
        return $this->hasOne('App\Models\GoogleAnaliticsIntegration');
    }

    public function vkPixel()
    {
        return $this->hasOne('App\Models\VkPixelIntegration');
    }


    public function facebookPixel()
    {
        return $this->hasOne('App\Models\FacebookPixelIntegration');
    }

    public function sendpulse()
    {
        return $this->hasOne('App\Models\SendPulseIntegration');
    }

    public function vkapp()
    {
        return $this->hasOne('App\Models\Quiz\VkApp');
    }

    public function vkappold()
    {
        return $this->hasOne('App\Models\VKApp');
    }

    public function webHooks()
    {
        return $this->hasOne('App\Models\WebHooksIntegration');
    }

    public function domain()
    {
        return $this->hasOne('App\Models\Domain');
    }

    public function amoCrm()
    {
        return $this->hasOne('App\Models\AmoCrm');
    }

    public function getAmoCrmSlimAttribute()
    {
        return $this->amoCrm()->first(['id', 'enabled']);
    }

    public function amoCrm3()
    {
        return $this->hasOne('App\Models\Amo\AmoIntegration');
    }

    public function getAmoCrm3SlimAttribute()
    {
        $amoSlimCollect = $this->amoCrm3()->first(['id', 'enable']);
        if (!$amoSlimCollect) {
            $amoSlimCollect = collect([
                'id' => 0,
                'enable' => 0,
            ]);
        }
        return $amoSlimCollect;
    }

    public function tiktok()
    {
        return $this->hasOne('App\Models\Integrations\Tiktok');
    }

    public function calltouch() {
        return $this->hasOne('App\Models\Integrations\Calltouch');
    }

    public function zapier() {
        return $this->hasOne('App\Models\Integrations\Zapier');
    }

    public function alytics()
    {
        return $this->hasOne('App\Models\Integrations\Alytics');
    }

    public function recaptcha()
    {
        return $this->hasOne('App\Models\Integrations\RecaptchaIntegration');
    }

    public function pixelVkAds()
    {
        return $this->hasOne('App\Models\Integrations\PixelVkAdsIntegration');
    }

    public function tagmanager()
    {
        return $this->hasOne('App\Models\Integrations\TagManagerIntegration');
    }

    public function getRoistatAttribute()
    {
        return RoistatIntegration::where("quiz_id", $this->id)
            ->firstOrCreate(["quiz_id" => $this->id]);
    }

    public function emailNotify2()
    {
        return $this->hasOne('App\Models\EmailNotify\EmailNotifyIntegrate');
    }

    public function emailNotify()
    {
        return $this->hasOne('App\Models\EmailNotifyIntegrate');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }

    public function lead(){
        return $this->hasMany('App\Models\Lead');
    }

    protected function preventLeadsDuplicationCookie()
    {
        return $this->hasOne('App\Models\PreventLeadsDuplicationCookie');
    }

    public function facebookConversionsApiEvents()
    {
        return $this->hasMany(FacebookConversionsApiEvent::class);
    }


    public function getPreventLeadsDuplicationCookieAttribute()
    {
        return PreventLeadsDuplicationCookie::where("quiz_id", $this->id)
            ->firstOrCreate(["quiz_id" => $this->id]);
    }

    public function getCountNewLeadsAttribute() {

        $key = CacheService::keyQuizCountNewLeads($this);
        $ttl = CacheService::day();

        return Cache::remember($key, $ttl, function () {
            return $this->lead()->where('status', 'new')->get()->count();
        });

    }

    public function clearCountNewLeadsAttribute($quiz) {

        Cache::forget('quiz_' . $quiz->id . '_count_new_leads');

    }

    public function swirls(){
        return $this->hasMany('App\Models\Swirl');
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'author_id');
    }

    public function getJsonQuizAttribute() {
        return json_decode($this->quiz_json);
    }
}
