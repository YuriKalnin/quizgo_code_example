<?php

namespace App\Providers;

class QuizgoServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind("App\Services\Rate\RateService", function($app) {
            return new \App\Services\Rate\RateService(
                $app->make('\App\Services\Rate\Services\GetRateService'),
                $app->make('\App\Services\Rate\Services\ChangeRateService'),
                $app->make('\App\Services\Rate\Services\RateBalanceService'),
                $app->make('\App\Services\Rate\Services\ActiveDateService'),
                $app->make('\App\Services\Rate\Services\AutoPayment'),
                $app->make('\App\Services\Rate\Services\LeadMagnet'),
                $app->make('\App\Services\Rate\Services\BaseRateService'),
                $app->make('\App\Services\Rate\Services\QuizPlusService'),
                $app->make('\App\Services\Rate\Services\CouponService'),
            );
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
