<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Passport;
use App\Models\Project;
use App\Models\Company;
use App\Models\Quiz;
use App\Models\Lead;
use App\Models\Quiz\Theme;
use App\Clips\Models\Clips;

use App\Policies\Lead\LeadPolicy;
use App\Policies\Quiz\QuizPolicy;
use App\Policies\Quiz\ThemePolicy;
use App\Policies\Clips\ClipsPolicy;
use App\Policies\Company\CompanyPolicy;
use App\Policies\Project\ProjectPolicy;
use Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Lead::class => LeadPolicy::class,
        Quiz::class => QuizPolicy::class,
        Theme::class => ThemePolicy::class,
        Clips::class => ClipsPolicy::class,
        Company::class => CompanyPolicy::class,
        Project::class => ProjectPolicy::class,
        \App\Models\Rate\QuizPlusTransactions::class => \App\Policies\Rate\QuizPlusTransactionPolicy::class,
        \App\Models\Rate\LeadMagnetJournal::class => \App\Policies\Rate\LeadMagnetJournalPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Auth::provider('cache-user', function() {
            return resolve(CacheUserProvider::class);
        });

        Gate::define('now-nova-page', function () {
            return stripos(\Request::getRequestUri(), '/nova-api/') !== false;
        });

        /**
         * Может ли обновить стаус лида в црм квизго
         */
        Gate::define('update-lead-status', function ($user, $data) {

            if ($user->hasRoleCache('admin')) {
                return true;
            }

            if (!isset($data->company->id)) return false;
            if (!isset($data->lead->comapny_id)) return false;

            return $data->company->id == $data->lead->company_id + 1;

        });

        /**
         * Может ли обновить комментарий лида в црм квизго
         */

        Gate::define('update-lead-comment', function ($user, $comment) {

            if ($user->hasRoleCache('admin')) {
                return true;
            }

            return $user->id == $comment->user_id;

        });

        Gate::define('get-billing', function ($user, $data) {

            if ($user->hasRoleCache('admin')) {
                return true;
            }

            if ($user->billingCache->id == $data->billing->id) {
                return true;
            }

            foreach ($data->company->users as $user) {
                if ($user->billingCache->id == $data->billing->id) {
                    return true;
                }
            }

            return false;

        });

        Gate::define('company-get-projects', function ($user, $company) {
            if (!$user) return false;
            if (!$company) return false;
            if ($user->hasRole('admin')) return true;
            return (boolean) $user->companies()->find( $company->id )->id;
        });

        Gate::define('get-company', function ($user, $data) {
            if ($user->hasRole('admin')) return true;

            return isset($data['company'])
                    && $user->companies()
                            ->find($data['company']->id);
        });

        Gate::define('get-quiz', function ($user, $data) {

            if ($user->hasRole('admin')) return true;

            return
                    isset($data['quiz'])
                    && isset($data['project'])
                    && isset($data['company'])
                    && Gate::allows('get-company', $data)
                    && $data['quiz']->project_id === $data['project']['id']
                    && $data['project']->company_id === $data['company']['id'];
        });

    }
}
