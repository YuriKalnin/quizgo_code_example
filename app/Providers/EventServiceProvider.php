<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        'App\Listeners\Lead\LeadRSIListenerSubscriber',
        'App\Listeners\Clerking\ClerkingSubscriber',
        'App\Listeners\AmoCrm3\AmoCrm3Subscriber',

        /**
         * Подписчик на событие App\Events\Rate\AfterBuying
         * Событие срабатывает после покупки лидов с карты или по счету
         */
        'App\Listeners\Rate\AfterBuyingSubscriber',

        /**
         * Подписчик на событие App\Events\Rate\AfterPaymentQuizPlus
         * Событие срабатывает после автоматической оплаты подписки квиз+
         */
        'App\Listeners\Rate\AfterPaymentSubscriber',

        /**
         * Подписчик на событие App\Events\Rate\ActivityEndDate
         * Событие срабатывает если у компании нет денег на балансе
         * и дата активности заканчивается
         */
        'App\Listeners\Rate\ActivityEndDateSubscriber',
    ];

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        \App\Events\Rate\After5DaysCompanyCreatedEvent::class => [
            \App\Listeners\User\Tunnel\UpdateDealDemoEnd::class
        ],

        \App\Events\Rate\ActivityEndDate::class => [
            \App\Listeners\User\Tunnel\CreateRepeatDealBitrix24::class
        ],

        \App\Events\Rate\AfterBuying::class => [
            \App\Listeners\User\Tunnel\UpdateDealRatePayment::class
        ],

        \App\Events\User\Tunnel\NeedQuizTemplate::class => [
            \App\Listeners\User\Tunnel\UpdateDealNeedQuizTemplate::class,
        ],

        \App\Events\User\ConfirmUserTunnel::class => [
            \App\Listeners\User\Tunnel\CreateDealBitrix24::class,
        ],

        \App\Events\Company\AfterCreatedCompany::class => [
            \App\Listeners\User\Tunnel\CreateDealBitrix24AfterCreatedCompany::class,
        ],

        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        /**
         * Срабатывает при добавлении или
         * удалении пользователя к сегменту
         */
        \App\Events\UserSegmentService\UserSegmentEventService::class => [

            \App\Listeners\UserSegments\UserDidNotLogin2DaysListener::class,
        ],

        /**
         * Срабатывает если на балансе компании
         * осталось денег на 7, 5, 3 или 1 дней
         */
        \App\Events\Rate\SmallCompanyBalance::class => [

            /**
             * Слушатель пытается запустить автооплату с карты
             */
            \App\Listeners\Rate\AutoPaymentCompanyListener::class,
        ],

        /**
         * событие срабатывает после
         * регистрации пользователя
         */
        \App\Events\AfterUserRegister::class => [

            \App\Listeners\AfterUserRegisterListener::class,
            \App\Listeners\User\CreateUserTunnel::class,
        ],

        /**
         * событие срабатывает после создания лида
         * и поставновки всех очередей
         */
        \App\Events\Lead\AfterSendLead::class => [

            /**
             * Общий слушатель
             * - Отслеживает создание первого лида для OnboardingProgress
             */
            \App\Listeners\Lead\AfterSendLeadListener::class,

            /**
             * Отправляет письмо администраторам компании
             * Если осталось 4 лида
             */
            \App\Listeners\Lead\SendEmailHas4Lead::class,

            /**
             * Слушатель очищает кеш для модели Quiz
             * Очищает кол-во лидов для квиза в статусе новый
             * Очищает кеш общего кол-ва лидов в компании
             */
            \App\Listeners\Lead\ClearCacheCountQuizLeadsListener::class,

            \App\Listeners\Lead\AddUserSegmentGetLeadListener::class,
        ],

        /**
         * Событие срабатывает после создания квиза
         */
        \App\Events\Quiz\AfterCreatedQuiz::class => [

            /**
             * Общий слушатель
             * Отслеживает создание квиза для OnboardingProgress
             */
            \App\Listeners\Quiz\AfterQuizCreateListener::class,

            /**
             * Запустит процесс оплаты подписки квиз плюс
             */
            \App\Listeners\Quiz\AutoPaymentListener::class,

        ],

        /**
         * Событие срабатывает после
         * обновления квиза и всех его
         * связанных сущностей
         */
        \App\Events\Quiz\AfterUpdatedQuiz::class => [

            /**
             * Основной слушатель события
             */
            \App\Listeners\Quiz\AfterQuizUpdateListener::class,

        ],

        \App\Events\Lead\AfterDeleteLead::class => [

            /**
             * Слушатель очищает кеш для модели Quiz
             * Очищает кол-во лидов для квиза в статусе новый
             */
            \App\Listeners\Lead\AfterDeleteLeadListener::class,

        ],
        \App\Events\Lead\AfterUpdateLead::class => [

            /**
             * Слушатель очищает кеш для модели Quiz
             * Очищает кол-во лидов для квиза в статусе новый
             */
            \App\Listeners\Lead\AfterUpdateLeadListener::class,
        ],

        /**
         * Событие срабатывает после покупки чего-нибудь на сервисе
         * после создания биллинг транзакции на списание ДС
         */
        \App\Events\Billing\AfterCreatedSuccessPayedTransaction::class => [

            /**
             * Слушатель передвигает сделку пользователя
             * В статус успешно реализовано для воронки
             * "Воронка QuizGo" и "Воронка покупатели"
             **/
            \App\Listeners\Billing\AmoFirstPayedListener::class,

            /**
             * Слушатель отправляет данные о пользователе в CarrotQuest
             */
            \App\Listeners\Billing\CarrotQuestSetUserRevenueListener::class,

            /**
             * Общшй слушатель
             * Фиксирует событие подписки для обучающего OnboardingProgress
             */
            \App\Listeners\Billing\AfterCreatedSuccessPayedTransactionListener::class

        ],

        /**
         * Событие срабатывает после зачисления
         * средсв на счет пользователя
         */
        \App\Events\Billing\AfterCreatedSuccessChargeTransaction::class => [

            /**
             * Запускаем метод зачисления ДС на счет
             */
            \App\Listeners\Billing\AfterCreatedSuccessChargeTransactionListener::class

        ],

        /**
         * Событие запускается когда администратор
         * Вручную проводит счет на оплату
         */
        \App\Events\Billing\AfterPayedBill::class => [

            /**
             * Основной случащатель события
             * - Отправляет увдомления в телеграм о том что счет оплачен
             * - Фиксирует данные в "вороноке обучения прогрессбар"
             */
            \App\Listeners\Billing\AfterPayedBillListener::class
        ],

        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\\VKontakte\\VKontakteExtendSocialite@handle',
            'SocialiteProviders\\Yandex\\YandexExtendSocialite@handle',
            'SocialiteProviders\\Facebook\\FacebookExtendSocialite@handle',
        ],
    ];



    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
