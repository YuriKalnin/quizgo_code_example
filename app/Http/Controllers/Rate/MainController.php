<?php
namespace App\Http\Controllers\Rate;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Rate\Requests\GetRequest;
use App\Http\Controllers\Rate\Requests\ChangeRequest;
use App\Services\Rate\RateService;

class MainController extends Controller
{
    public function get(GetRequest $request, RateService $rateService) {

        return response()->json(
            $rateService->get(
                $request->getCompany()
            )
        );
    }

    public function change(ChangeRequest $request, RateService $rateService) {

        return response()->json(
            $rateService->change(
                $request->getCompany(),
                $request->get('rate'),
            )
        );
    }
}

