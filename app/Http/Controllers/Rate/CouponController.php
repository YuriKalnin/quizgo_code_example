<?php
namespace App\Http\Controllers\Rate;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Rate\Requests\CouponController\MakeRequest;
use App\Http\Controllers\Rate\Requests\ChangeRequest;
use App\Services\Rate\RateService;

class CouponController extends Controller
{
    public function make(MakeRequest $request, RateService $rateService) {

        return response()->json(
            $rateService->coupon()->make(
                $request->user(),
                $request->company(),
                $request->coupon(),
            )
        );
    }
}

