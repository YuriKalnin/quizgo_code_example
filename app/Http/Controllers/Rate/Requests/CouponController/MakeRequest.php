<?php
namespace App\Http\Controllers\Rate\Requests\CouponController;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use App\Models\Company;
use Gate;

class MakeRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public $companyCollect;

    public function authorize()
    {
        $this->companyCollect = Company::findOrFail(
            $this->request->get('company')
        );
        return Gate::allows('getRateInfo', $this->companyCollect);
    }

    public function company() {
        return $this->companyCollect;
    }

    public function coupon() {
        return request('coupon');
    }

    public function rules()
    {
        return [
            'company' => 'required|integer|exists:companies,id',
            'coupon' => 'required|string|exists:coupons,coupon',
        ];
    }

    public function messages()
    {
        return [
            'coupon.exists' => "Купон {$this->coupon()} не найден",
        ];
    }
}
