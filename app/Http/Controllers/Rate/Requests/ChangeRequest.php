<?php
namespace App\Http\Controllers\Rate\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use App\Models\Company;
use Gate;

class ChangeRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public $companyCollect;

    public function authorize()
    {
        $this->companyCollect = Company::findOrFail(
            $this->request->get('company')
        );
        return Gate::allows('getRateInfo', $this->companyCollect);
    }

    public function getCompany() {
        return $this->companyCollect;
    }

    public function rules()
    {
        return [
            'company' => 'required|integer|exists:companies,id',
            'rate' => 'required|in:quiz,lead',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if ($this->companyCollect->account->rate == $this->request->get('rate')) {
                $validator->errors()->add('rate', 'У компании тот же тариф');
            }
        });
    }

    public function messages()
    {
        return [
        ];
    }
}
