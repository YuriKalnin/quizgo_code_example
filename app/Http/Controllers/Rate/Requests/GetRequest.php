<?php
namespace App\Http\Controllers\Rate\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use App\Models\Company;
use Gate;

class GetRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public $companyCollect;

    public function authorize()
    {
        $this->companyCollect = Company::findOrFail(
            $this->request->get('company')
        );
        return Gate::allows('getRateInfo', $this->companyCollect);
    }

    public function getCompany() {
        return $this->companyCollect;
    }

    public function rules()
    {
        return [
            'company' => 'required|integer|exists:companies,id',
        ];
    }

    public function messages()
    {
        return [
        ];
    }
}
