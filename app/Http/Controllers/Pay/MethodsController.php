<?php

namespace App\Http\Controllers\Pay;

use App\Http\Controllers\Controller;
use App\Services\PayService\PayService;
use App\Http\Controllers\Pay\Requests\MethodsController\GetRequest;
use App\Http\Controllers\Pay\Requests\MethodsController\CreateBillRequest;

class MethodsController extends Controller
{
    public function get(GetRequest $request, PayService $payService) {

        return response()->json(
            $payService->methods()->get(
                $request->user(),
                $request->company(),
            )
        );
    }

    public function createBill(CreateBillRequest $request, PayService $payService) {

        return response()->json(
            $payService->methods()->bill()->create(
                $request->user(),
                $request->company(),
                $request->validated(),
            )
        );
    }
}

