<?php

namespace App\Http\Controllers\Pay\AutoPayment;

use App\Http\Controllers\Controller;
use App\Services\PayService\PayService;
use App\Http\Controllers\Pay\Requests\AutoPaymentController\TurnRequest;

class AutoPaymentController extends Controller
{
    public function turn(TurnRequest $request, PayService $payService) {
        return response()->json(
            $payService->autoPayment()->turn(
                $request->user(),
                $request->company(),
                request('turn'),
            )
        );
    }
}

