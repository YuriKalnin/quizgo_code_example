<?php

namespace App\Http\Controllers\Pay;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PayService\PayService;
use App\Http\Controllers\Pay\Requests\MethodsController\CreateBillRequest;

class CounterAgentController extends Controller
{
    public function get(Request $request, PayService $payService) {

        return response()->json(
            $payService->counterAgent()->get(
                $request->get('counter_agent_id')
            )
        );
    }
}

