<?php
namespace App\Http\Controllers\Pay\Requests\MethodsController;

use App\Models\Company;
use App\Models\EmailNotify\Email;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use Gate;

class PayTinkoffRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public $companyCollect;

    public function authorize()
    {
        $this->companyCollect = Company::findOrFail(
            request('company_id')
        );
        return Gate::allows('view', $this->companyCollect);
    }

    public function company() {
        return $this->companyCollect;
    }

    public function rules()
    {
        return [
            'rebill_id' => 'required|integer',
            'user_id' => 'required|integer',
            'company_id' => 'required|integer|exists:companies,id',
            'cost' => 'required|numeric',
            'product' => 'required',
            'mont' => 'required|numeric',
            'lead' => 'required|numeric',
            'count' => 'required|numeric',
        ];
    }

    public function prepareForValidation()
    {
        $userID = auth()->user()->id;
        $count = request('count', 0);

        /**
         * На тарифе Квиз+ пользователю покупает месяцы
         */
        $month = request('product') === 'quiz' ? $count : 0;

        /**
         * На тарифе ЛидМагнит пользователю покупает лиды
         */
        $leads = request('product') === 'lead' ? $count : 0;

        $this->merge([
            'user_id' => $userID,
            'mont' => $month,
            'lead' => $leads,
        ]);
    }
}
