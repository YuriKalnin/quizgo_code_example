<?php
namespace App\Http\Controllers\Pay\Requests\MethodsController;

use App\Models\Company;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use Gate;

class GetRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public $companyCollect;

    public function authorize()
    {
        $this->companyCollect = Company::findOrFail(request('company'));
        return Gate::allows('view', $this->companyCollect);
    }

    public function company() {
        return $this->companyCollect;
    }

    public function rules()
    {
        return [
            'company' => 'required|integer|exists:companies,id',
        ];
    }
}
