<?php
namespace App\Http\Controllers\Pay\Requests\MethodsController;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;

class WebhookEnotRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public function authorize()
    {
        return true;
    }

    public function company() {
        return $this->companyCollect;
    }

    public function rules()
    {
        return [
            'merchant' => 'required|integer',
            'amount' => 'required',
            'credited' => 'required',
            'intid' => 'required',
            'merchant_id' => 'required|integer|exists:transactions,id',
            'method' => 'required|string',
            'sign' => 'required|string',
            'sign_2' => 'required|string',
            'currency' => 'required|string',
        ];
    }
}
