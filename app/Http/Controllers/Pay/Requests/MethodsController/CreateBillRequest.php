<?php
namespace App\Http\Controllers\Pay\Requests\MethodsController;

use App\Models\Company;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use Gate;

class CreateBillRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public $companyCollect;

    public function authorize()
    {
        $this->companyCollect = Company::findOrFail(
            $this->request->get('company')
        );
        return Gate::allows('view', $this->companyCollect);
    }

    public function company() {
        return $this->companyCollect;
    }

    public function rules()
    {
        return [
            'company' => 'required|integer|exists:companies,id',
            'full_name' => 'required',
            'inn' => 'required',
            'kpp' => 'nullable',
            'address' => 'required',
            'cost' => 'required|integer',
            'rate' => 'required',

            'date_from' => 'required',
            'date_to' => 'required',
            'mont' => 'required|integer',
            'count_leads' => 'required|integer',
            'billing_id' => 'required|integer',
            'user_id' => 'required|integer',
            'post_address' => 'nullable',
            'cost_currency' => 'required',
        ];
    }

    public function prepareForValidation()
    {
        $dateFrom = now()->format('Y-m-d');
        $dateTo = now()->add(15, 'day')->format('Y-m-d');
        $count = request('count', 0);
        $kpp = request('kpp', '');
        $cost = request('cost', '');
        $userID = auth()->user()->id;

        /**
         * На тарифе Квиз+ пользователю покупает месяцы
         */
        $month = $this->request->get('rate') === 'quiz' ? $count : 0;

        /**
         * На тарифе ЛидМагнит пользователю покупает лиды
         */
        $leads = $this->request->get('rate') === 'lead' ? $count : 0;

        $this->merge([
            'date_from' => $dateFrom,
            'date_to' => $dateTo,
            'mont' => $month,
            'count_leads' => $leads,
            'billing_id' => 0,
            'post_address' => '',
            'user_id' => $userID,
            'kpp' => $kpp,
            'cost_currency' => $cost,
        ]);
    }
}
