<?php
namespace App\Http\Controllers\Pay\Requests\AutoPaymentController;

use App\Models\Company;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use Gate;

class TurnRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public $companyCollect;

    public function authorize()
    {
        $this->companyCollect = Company::findOrFail(
            request('company_id')
        );
        return Gate::allows('view', $this->companyCollect);
    }

    public function company() {
        return $this->companyCollect;
    }

    public function rules()
    {
        return [
            'turn' => 'required|boolean',
            'company_id' => 'required|integer|exists:companies,id',
        ];
    }
}
