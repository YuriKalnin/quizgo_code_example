<?php

namespace App\Http\Controllers\Pay\Methods\Enot;

use App\Http\Controllers\Controller;
use App\Services\PayService\PayService;
use App\Http\Controllers\Pay\Requests\MethodsController\InitEnotTransactionRequest;
use App\Http\Controllers\Pay\Requests\MethodsController\WebhookEnotRequest;

class EnotController extends Controller
{
    public function init(InitEnotTransactionRequest $request, PayService $payService) {

        return response()->json(
            $payService->methods()->enot()->initPay(
                $request->validated(),
            )
        );
    }

    public function webhook(WebhookEnotRequest $request, PayService $payService) {

        return response()->json(
            $payService->methods()->enot()->webhook(
                $request->validated(),
            )
        );
    }
}

