<?php

namespace App\Http\Controllers\Pay\Methods\Prodamus;

use App\Http\Controllers\Controller;
use App\Actions\Payment\Prodamus\InitPaymentUrlAction;
use App\Actions\Payment\Prodamus\ListenWebhookAction;
use App\Http\Controllers\Pay\Methods\Prodamus\Requests\InitRequest;
use App\Http\Controllers\Pay\Methods\Prodamus\Requests\WebhookRequest;

class ProdamusController extends Controller
{
    public function init(InitRequest $request, InitPaymentUrlAction $action) {
        return $action->handle($request->validated())->jsonResponse();
    }

    public function webhook(WebhookRequest $request, ListenWebhookAction $action) {
        return $action->handle($request->all(), $request->header())->jsonResponse();
    }
}

