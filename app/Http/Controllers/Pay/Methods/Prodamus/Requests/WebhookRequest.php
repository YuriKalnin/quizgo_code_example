<?php
namespace App\Http\Controllers\Pay\Methods\Prodamus\Requests;

use App\Actions\Payment\Prodamus\Api\Prodamus;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;

class WebhookRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public function authorize() {
        return app()->make(Prodamus::class)->isProdamus($this->request->all(), $this->header());
    }

    public function rules() {
        return [

        ];
    }
}
