<?php

namespace App\Http\Controllers\Pay\Methods\Tinkoff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PayService\PayService;
use App\Http\Controllers\Pay\Requests\MethodsController\InitTinkoffTransactionRequest;
use App\Http\Controllers\Pay\Requests\MethodsController\PayTinkoffRequest;

class TinkoffCardController extends Controller
{
    public function success(Request $request) {

        return view('service.pay.methods.tinkoff-card.success');
    }

    public function fail(Request $request) {

        return view('service.pay.methods.tinkoff-card.fail');
    }

    public function init(InitTinkoffTransactionRequest $request, PayService $payService) {

        return response()->json(
            $payService->methods()->cardTinkoff()->initPay(
                $request->user(),
                $request->validated(),
            )
        );
    }

    public function pay(PayTinkoffRequest $request, PayService $payService) {
        return response()->json(
            $payService->methods()->cardTinkoff()->pay(
                $request->user(),
                $request->validated(),
            )
        );
    }
}

