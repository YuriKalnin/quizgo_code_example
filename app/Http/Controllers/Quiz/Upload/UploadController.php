<?php

namespace App\Http\Controllers\Quiz\Upload;

use App\Http\Controllers\Controller;
use App\Models\Quiz;
use App\Actions\Quiz\Upload\RemoveFilesAction;
use App\Actions\Quiz\Upload\CopyFilesAction;

class UploadController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function remove(
        Quiz $quiz,
        RemoveFilesAction $action,
        Request\RemoveRequest $request
    ) {
        return $action->handle($quiz, $request->get('files_ids'))->jsonResponse();
    }

    public function copy(
        Quiz $quiz,
        CopyFilesAction $action,
        Request\RemoveRequest $request
    ) {
        return $action->handle($quiz, $request->get('files_ids'))->jsonResponse();
    }
}

