<?php
namespace App\Http\Controllers\Quiz\Upload\Request;

use Gate;
use Illuminate\Foundation\Http\FormRequest;

class RemoveRequest extends FormRequest
{
    public function authorize() {
        return Gate::allows('view', $this->route('quiz'));
    }

    public function rules()
    {
        return [
            'files_ids' => 'required|array',
        ];
    }
}
