<?php

namespace App\Http\Controllers\Quiz;

use App\Http\Controllers\Controller;
use QuizService;
use App\Http\Controllers\Quiz\Requests\QuizThemeStoreRequest;
use App\Models\Quiz\Theme;

class QuizThemeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {

        $quizThemes = QuizService::theme()->list();

        return response()->json($quizThemes);
    }

    public function store(QuizThemeStoreRequest $request) {

        $this->authorize('create', Theme::class);

        $theme = QuizService::theme()->store($request->all());

        return response()->json($theme);
    }

    public function destroy(Theme $theme) {

        $this->authorize('delete', $theme);

        $theme = QuizService::theme()->destroy($theme);

        return response()->json($theme);
    }
}

