<?php
namespace App\Http\Controllers\Quiz\Requests;

use Gate;
use Illuminate\Foundation\Http\FormRequest;

class MoveToProjectRequest extends FormRequest
{

    public function authorize()
    {
        return Gate::allows('view', $this->route('quiz'));
    }

    public function projectTo() {
        return request('project_id');
    }

    public function rules()
    {
        return [
            'project_id' => 'required|integer|exists:projects,id',
            'agree' => 'required|accepted'
        ];
    }
}
