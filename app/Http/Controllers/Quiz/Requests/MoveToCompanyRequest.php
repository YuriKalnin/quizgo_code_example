<?php
namespace App\Http\Controllers\Quiz\Requests;

use Gate;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\Company;

class MoveToCompanyRequest extends FormRequest
{
    public $company;

    public function authorize()
    {
        return Gate::allows('view', $this->route('quiz'));
    }

    public function company() {
        return $this->company;
    }

    public function withValidator($validator)
    {
        $this->company = Company::findOrFail(request('company_id', 0));
        $validator->after(function ($validator) {
            if (!$this->company->project->count()) {
                $validator->errors()->add('company_id', 'В целевой компании нет ни одного проекта');
            }
        });
    }

    public function rules()
    {
        return [
            'company_id' => 'required|integer|exists:companies,id',
            'agree' => 'required|accepted'
        ];
    }
}
