<?php
namespace App\Http\Controllers\Quiz\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class GetQuizTemplatesRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [];
    }
}
