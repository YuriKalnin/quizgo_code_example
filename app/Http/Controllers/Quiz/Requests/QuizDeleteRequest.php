<?php
namespace App\Http\Controllers\Quiz\Requests;

use Gate;
use Illuminate\Foundation\Http\FormRequest;

class QuizDeleteRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('view', $this->route('quiz'));
    }

    public function rules()
    {
        return [];
    }
}
