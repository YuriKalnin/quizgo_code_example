<?php
namespace App\Http\Controllers\Quiz\Requests;

use Gate;
use Illuminate\Foundation\Http\FormRequest;

class QuizGetRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('view', $this->route('project'));
    }

    public function rules()
    {
        return [

        ];
    }
}
