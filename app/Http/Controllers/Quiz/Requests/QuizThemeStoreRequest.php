<?php
namespace App\Http\Controllers\Quiz\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

use App\Models\Quiz\Theme;

class QuizThemeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'font' => 'required',
            'bg' => 'required',
            'color' => 'required',
            'buttons_background' => 'required',
            'buttons_color' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Поле обязательно для заполнения.'
        ];
    }
}
