<?php

namespace App\Http\Controllers\Quiz;

use App\Http\Controllers\Controller;
use QuizService;

class SettingsController extends Controller
{
    public function __invoke($quiz) {
        $quizService = QuizService::getByQuizID($quiz, true, \CacheService::getKeyQuiz($quiz));
        return response()->json(
            json_decode($quizService->defaultQuizTpl)
        );
    }
}
