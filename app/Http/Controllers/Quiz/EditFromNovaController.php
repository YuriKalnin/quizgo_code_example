<?php

namespace App\Http\Controllers\Quiz;

use App\Http\Controllers\Controller;
use App\Models\Quiz;
use App\Models\Project;

class EditFromNovaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Quiz $quiz) {

        if (!isset($quiz->project->company->id)) {
            abort(404);
        }

        return redirect()->route('quiz.edit', [$quiz->project->company, $quiz->project, $quiz]);
    }
}

