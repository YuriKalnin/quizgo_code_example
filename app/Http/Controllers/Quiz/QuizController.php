<?php

namespace App\Http\Controllers\Quiz;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Quiz\Requests\RenameQuizRequest;
use App\Http\Controllers\Quiz\Requests\CreateQuizRequest;
use App\Http\Controllers\Quiz\Requests\QuizDeleteRequest;
use App\Http\Controllers\Quiz\Requests\MoveToCompanyRequest;
use App\Http\Controllers\Quiz\Requests\MoveToProjectRequest;
use App\Http\Controllers\Quiz\Requests\GetQuizTemplatesRequest;

use App\Models\Quiz;
use App\Models\Project;
use App\Actions\Quiz\CreateQuizAction;
use App\Actions\Quiz\DeleteQuizAction;
use App\Actions\Quiz\RenameQuizAction;
use App\Actions\Quiz\MoveToCompanyAction;
use App\Actions\Quiz\MoveToProjectAction;
use App\Actions\Quiz\GetQuizTemplatesAction;


class QuizController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('devtool.save-response');
    }

    public function update(
        $project,
        Quiz $quiz,
        RenameQuizAction $action,
        RenameQuizRequest $request
    ) {
        return response()->json($action->handle($quiz, $request->all()));
    }

    public function moveToCompany(
        Quiz $quiz,
        MoveToCompanyAction $action,
        MoveToCompanyRequest $request
    ) {
        return response()->json($action->handle($quiz, $request->company()));
    }

    public function moveToProject(
        Quiz $quiz,
        MoveToProjectAction $action,
        MoveToProjectRequest $request
    ) {
        return response()->json($action->handle($quiz, $request->projectTo()));
    }

    public function templates(
        GetQuizTemplatesAction $action,
        GetQuizTemplatesRequest $request
    ) {
        return response()->json($action->handle());
    }

    public function destroy(
        $project,
        Quiz $quiz,
        DeleteQuizAction $action,
        QuizDeleteRequest $request
    ) {
        return $action->handle($quiz)->jsonResponse();
    }

    public function store(
        Project $project,
        CreateQuizRequest $request,
        CreateQuizAction $action
    ) {
        return $action->handle($project, $request->user(), $request->all())->jsonResponse();
    }
}

