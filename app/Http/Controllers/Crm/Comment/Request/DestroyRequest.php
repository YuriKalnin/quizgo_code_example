<?php
namespace App\Http\Controllers\Crm\Comment\Request;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use Gate;

class DestroyRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public function authorize()
    {
        return Gate::allows('view', $this->route('lead'));
    }

    public function rules() {
        return [];
    }
}
