<?php
namespace App\Http\Controllers\Crm\Comment;

use App\Http\Controllers\Controller;
use App\Models\Lead;
use App\Models\CommentCrm;
use App\Http\Controllers\Crm\Comment\Request\DestroyRequest;
use App\Http\Controllers\Crm\Comment\Request\CreateRequest;
use App\Http\Controllers\Crm\Comment\Request\UpdateRequest;
use App\Actions\Crm\Comment\DestroyLeadCommentAction;
use App\Actions\Crm\Comment\CreateLeadCommentAction;
use App\Actions\Crm\Comment\UpdateLeadCommentAction;
use App\Actions\Crm\Comment\GetLeadCommentsAction;

class ResourceController extends Controller {

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(
        Lead $lead,
        DestroyRequest $request,
        GetLeadCommentsAction $action
    ){
        return $action->handle($lead)->jsonResponse();
    }

    public function destroy(
        Lead $lead,
        CommentCrm $comment,
        DestroyRequest $request,
        DestroyLeadCommentAction $action
    ){
        return $action->handle($comment)->jsonResponse();
    }

    public function store(
        Lead $lead,
        CreateRequest $request,
        CreateLeadCommentAction $action
    ){
        return $action->handle($lead, $request->validated())->jsonResponse();
    }

    public function update(
        Lead $lead,
        CommentCrm $comment,
        UpdateRequest $request,
        UpdateLeadCommentAction $action
    ){
        return $action->handle($comment, $request->validated())->jsonResponse();
    }
}
