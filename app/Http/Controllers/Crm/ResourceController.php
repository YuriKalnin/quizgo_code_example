<?php
namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm\Request\LoadLeadsRequest;
use App\Http\Controllers\Crm\Request\ShowRequest;
use App\Http\Controllers\Crm\Request\UpdateStatusRequest;
use App\Actions\Crm\LoadLeadAction;
use App\Actions\Crm\GetLeadDetailAction;
use App\Actions\Crm\DestroyLeadAction;
use App\Actions\Crm\SetLeadStatusAction;
use App\Models\Lead;

class ResourceController extends Controller {

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(
        LoadLeadsRequest $request,
        LoadLeadAction $action
    ){
        return $action->handle($request->validated())->jsonResponse();
    }

    public function show(
        Lead $lead,
        ShowRequest $request,
        GetLeadDetailAction $action
    ){
        return $action->handle($lead)->jsonResponse();
    }

    public function destroy(
        Lead $lead,
        ShowRequest $request,
        DestroyLeadAction $action
    ){
        return $action->handle($lead)->jsonResponse();
    }

    public function update(
        Lead $lead,
        UpdateStatusRequest $request,
        SetLeadStatusAction $action
    ){
        return $action->handle($lead, $request->get('status', 'new'))->jsonResponse();
    }
}
