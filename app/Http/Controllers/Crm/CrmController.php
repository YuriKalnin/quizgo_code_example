<?php

namespace App\Http\Controllers\Crm;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm\Request\ExportRequest;
use App\Http\Controllers\Crm\Request\DisableLeadsInfoRequest;
use App\Actions\Crm\GetExportParamsAction;
use App\Actions\Crm\GetDisableLeadsInfoAction;
use App\Actions\Crm\ExportAction;
use App\Models\Company;

class CrmController extends Controller {

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function disableLeadsInfo(
        Company $company,
        DisableLeadsInfoRequest $request,
        GetDisableLeadsInfoAction $action
    ){
        return $action->handle($company)->jsonResponse();
    }

    public function getExportParams(
        Company $company,
        DisableLeadsInfoRequest $request,
        GetExportParamsAction $action
    ){
        return $action->handle($company)->jsonResponse();
    }

    public function export(
        Company $company,
        ExportRequest $request,
        ExportAction $action
    ){
        return $action->handle($request->validated(), false)->jsonResponse();
    }
}
