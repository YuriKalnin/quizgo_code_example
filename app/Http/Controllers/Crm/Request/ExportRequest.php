<?php
namespace App\Http\Controllers\Crm\Request;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use Gate;

class ExportRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public function authorize()
    {
        return Gate::allows('view', $this->route('company'));
    }

    public function rules() {
        return [
            'quiz_id' => 'required|exists:quiz,id',
            'status' => 'required|array|in:new,success,progress,canceled',
            'period' => 'required|in:today,yesterday,week,month,half_year,year,custom',
            'continueDisableQuestions' => 'required',
            'created_at_from' => 'nullable',
            'created_at_to' => 'nullable',
        ];
    }

    public function messages() {
        return [
            'quiz_id.exists' => 'Выберите квиз. Квиз не найден.',
            'status.required' => 'Выберите статусы для выгрузки',
            'period.in' => 'Недопустимое значение поля период',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {

            if (request('period', false) == 'custom') {

                $created_at_from = request('created_at_from', null);
                $created_at_to = request('created_at_to', null);

                if (!$created_at_from || !$created_at_to) {
                    $validator->errors()->add('date', 'Неверно задан период выгрузки');
                    return;
                }

                $created_at_from = Carbon::parse($created_at_from);
                $created_at_to = Carbon::parse($created_at_to);

                if ($created_at_from->diffInDays($created_at_to) > 370) {
                    $validator->errors()->add('date', 'Превышен максимальный период выгрузки в 1 год');
                }

                if ($created_at_from >= $created_at_to) {
                    $validator->errors()->add('date', 'Дата начала выгрузки не может быть позже или равна дате конца выгрузки');
                }
            }
        });
    }
}
