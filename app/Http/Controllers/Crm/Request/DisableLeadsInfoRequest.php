<?php
namespace App\Http\Controllers\Crm\Request;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use Gate;

class DisableLeadsInfoRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public function authorize()
    {
        return Gate::allows('view', $this->route('company'));
    }

    public function rules() {
        return [];
    }
}
