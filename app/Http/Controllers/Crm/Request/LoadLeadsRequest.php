<?php
namespace App\Http\Controllers\Crm\Request;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Traits\JsonFailedResponseTrait;
use App\Models\Company;
use App\Models\Project;
use App\Models\Quiz;
use Gate;

class LoadLeadsRequest extends FormRequest
{
    use JsonFailedResponseTrait;

    public function authorize()
    {
        return Gate::allows('view', $this->collection());
    }

    public function rules() {
        return [
            'field' => 'required|in:company_id,project_id,quiz_id',
            'value' => 'required|integer',
            'status' => 'required|in:new,progress,success,canceled,unlocked',
            'page' => 'required|integer',
            'order_field' => 'required|in:created_at,updated_at',
            'order_direction' => 'required|in:asc,desc',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'page' => request('page', 1),
            'field' => request('field', 'company_id'),
            'status' => request('status', 'new'),
            'order_field' => request('order_field', 'created_at'),
            'order_direction' => request('order_direction', 'asc'),
        ]);
    }

    private function model() {
        $field = request('field', 'company_id');
        if ($field === 'company_id') return new Company;
        if ($field === 'project_id') return new Project;
        if ($field === 'quiz_id') return new Quiz;
    }

    private function collection() {
        return $this->model()
                    ->findOrFail(request('value'));
    }
}
