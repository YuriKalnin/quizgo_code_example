<?php
namespace App\Actions\Lead;

use App\Services\Traits\StatusTrait;
use App\Models\Lead;

class GetLeadDataAction
{
    use StatusTrait;

    private $lead;
    private $quiz;
    private $project;
    private $company;

    public $data;

    public function handle($lead) {
        $this->lead = is_object($lead) ? $lead : Lead::find($lead);
        $this->quiz = $this->lead->quiz;
        $this->project = $this->quiz->project;
        $this->company = $this->project->company;

        $this->data = $this->lead->only([
            'id',
            'title',
            'created_at',
            'name',
            'phone',
            'phone_clean',
            'email',
            'messenger',
            'contact_messenger',
            'roistat_visit',
            'comagic_session_id',
            'y_client_id',
            'g_client_id',
            'yclid',
            'gclid',
            'fbclid',
            'fbc',
            'fbp',
            'ip',
            'sale_value',
            'sale_type',
            'recaptcha_rating',
            'calltouch_session_id',
            'page_url',
            'user_agent',
            'user_agent',
            'answers_as_arrays2',
        ]);

        $this->data = array_merge($this->data, [
            'company' => $this->company->name,
            'company_id' => $this->company->id,

            'project' => $this->project->name,
            'project_id' => $this->project->id,

            'quiz' => $this->quiz->name,
            'quiz_id' => $this->quiz->id,

            'utm_source'  => $this->lead->utm_source,
            'utm_medium'  => $this->lead->utm_medium,
            'utm_campaign'=> $this->lead->utm_campaign,
            'utm_content' => $this->lead->utm_content,
            'utm_term'    => $this->lead->utm_term,
            'answers' => $this->lead->answers_string,
        ]);

        return $this->setOkStatus();
    }
}
