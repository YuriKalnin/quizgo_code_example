<?php
namespace App\Actions\Payment\Prodamus;

use App\Models\Transaction;
use App\Services\Traits\StatusTrait;
use RateService;
use NotyService;

class ListenWebhookAction
{
    use StatusTrait;

    private $transaction;
    private $company;
    private $account;

    public function handle($data) {

        $this->transaction = Transaction::find($data['order_num']);

        if ($this->transaction->status == 'CONFIRMED') {
            return $this->setFailStatus('Transaction is CONFIRMED status');
        }

        $this->company = $this->transaction->company;
        $this->account = $this->company->account;

        $this->resetDemo();

        $this->afterSuccessPayment();

        $this->sendNotyTelegram();

        return $this->setOkStatus();
    }

    private function resetDemo() {
        return RateService::base()->resetDemo($this->account);
    }

    private function afterSuccessPayment() {

        $this->transaction->update([
            'status' => 'CONFIRMED',
            'message' => 'Транзакция подтверждена банком',
            'type' => 'card_prodamus',
        ]);

        $this->account->last_pay_method = 'card_eu';
        $this->account->save();

        return RateService::balance()->afterSuccessPayment($this->company, $this->transaction);
    }

    private function sendNotyTelegram() {
        $msg = "Поступила оплата PRODAMUS";
        $msg .= "\nСтоимость " . ($this->transaction->cost ?? 0) . "₽";
        $msg .= "\nПользователь " . ($this->transaction->user_id ?? 0);
        $msg .= "\nКомпания " . ($this->company->id ?? 0);
        NotyService::sendTelegramMessage($msg, false, '-600491881');
    }
}
