<?php

namespace App\Actions\Payment\Prodamus\Api;

use App\Services\Traits\StatusTrait;

class Prodamus
{
    use StatusTrait;

    private $url;
    private $secretKey;

    public function __construct()
    {
        $this->url = config('quizgo.prodamus.url');
        $this->secretKey = config('quizgo.prodamus.secret');
    }

    public function preparation($data) {
        $data['signature'] = $this->hmac($data);
        return sprintf('%s?%s', $this->url, http_build_query($data));
    }

    public function isProdamus($data, $headers) {
        return $this->hmac($data) === $headers['sign'][0];
    }

    public function hmac($data, $algo = 'sha256') {
        if (!in_array($algo, hash_algos()))
            return false;
        $data = (array) $data;
        array_walk_recursive($data, function(&$v){
            $v = strval($v);
        });
        $this->hmacSort($data);
        if (version_compare(PHP_VERSION, '5.4.0', '<')) {
            $data = preg_replace_callback('/((\\\u[01-9a-fA-F]{4})+)/', function ($matches) {
                return json_decode('"'.$matches[1].'"');
            }, json_encode($data));
        }
        else {
            $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        }
        return hash_hmac($algo, $data, $this->secretKey);
    }

    public function hmacVerify($data, $sign, $algo = 'sha256') {
        $_sign = $this->hmac($data, $algo);
        return ($_sign && (strtolower($_sign) == strtolower($sign)));
    }

    public function hmacSort(&$data) {
        ksort($data, SORT_REGULAR);
        foreach ($data as &$arr)
            is_array($arr) && $this->hmacSort($arr);
    }
}
