<?php
namespace App\Actions\Payment\Prodamus;

use App\Models\Transaction;
use App\Services\Traits\StatusTrait;
use App\Actions\Payment\Prodamus\Api\Prodamus;

class InitPaymentUrlAction
{
    use StatusTrait;

    public $paymentUrl;

    public function handle($data) {

        $transaction = Transaction::create($data);

        $params = [
            'order_id' => $transaction->id,
            'customer_email' => $transaction->user->email,
            'products' => [
                [
                    'name' => "Неисключительная лицензия на ПО КвизГо №{$transaction->id}",
                    'price' => $transaction->cost,
                    'quantity' => '1',
                    'paymentMethod' => 1,
                    'paymentObject' => 1,
                ],
            ],
            'do' => 'link',
            'sys' => 'quizgo',
            'discount_value' => 0.00,
            'npd_income_type' => 'FROM_INDIVIDUAL',
        ];

        if ($transaction->user->phone) {
            $params['customer_phone'] = $transaction->user->phone;
        }

        $this->paymentUrl = app()->make(Prodamus::class)->preparation($params);
        $this->paymentUrl = file_get_contents($this->paymentUrl);

        return $this->setOkStatus();

    }
}
