<?php

namespace App\Actions\Crm;

use App\Services\Traits\StatusTrait;

class GetExportParamsAction
{
    use StatusTrait;

    public $projects;

    public function handle($company) {

        $this->projects = $company->project()->with('quiz')->get();

        $this->projects = $this->projects->map(function ($project) {
            $project->quiz = $project->quiz->map(function ($quiz) {
                return $quiz->only([
                    'id',
                    'name',
                ]);
            });
            return $project->only([
                'id',
                'name',
                'quiz',
            ]);
        });

        return $this->setOkStatus();
    }
}
