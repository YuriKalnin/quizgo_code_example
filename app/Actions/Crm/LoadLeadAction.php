<?php
namespace App\Actions\Crm;

use App\Services\Traits\StatusTrait;
use App\Models\Lead;

class LoadLeadAction
{
    use StatusTrait;

    public $limit = 50;
    public $total = 0;
    public $count = 0;
    public $nextPage;
    public $currentPage;
    public $nextPageUrl;
    public $data;
    public $leads;

    private $leadsCollect;

    public function handle($data) {

        $this->data = $data;

        $this->leadsCollect = Lead::where($data['field'], $data['value'])
                            ->where('status', $data['status'])
                            ->where('enable', 1)
                            ->orderBy($data['order_field'], $data['order_direction'])
                            ->paginate($this->limit);

        $this->total = $this->leadsCollect->total();
        $this->count = $this->leadsCollect->count();
        $this->currentPage = $this->leadsCollect->currentPage();
        $this->nextPageUrl = $this->leadsCollect->nextPageUrl();
        $this->nextPage = $this->nextPageUrl ? $this->currentPage + 1 : false;

        $this->leads = $this->leadsCollect->map(function ($lead) {
            return $lead->only([
                'id',
                'company_id',
                'project_id',
                'quiz_id',
                'title',
                'created_at',
                'created_at_format',
                'recaptcha_rating',
                'name',
                'phone',
                'email',
                'messenger',
                'contact_messenger',
                'status',
            ]);
        });

        return $this->setOkStatus();
    }
}
