<?php
namespace App\Actions\Crm;

use App\Events\Lead\AfterUpdateLead;
use App\Services\Traits\StatusTrait;

class SetLeadStatusAction
{
    use StatusTrait;

    public function handle($lead, $status) {

        if ($lead->update([
            'status' => $status
        ])) {
            event(new AfterUpdateLead( $lead ));
            return $this->setOkStatus();
        }

        return $this->setFailStatus();
    }
}
