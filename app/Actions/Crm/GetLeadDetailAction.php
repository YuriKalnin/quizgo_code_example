<?php
namespace App\Actions\Crm;

use App\Services\Traits\StatusTrait;
use Auth;

class GetLeadDetailAction
{
    use StatusTrait;

    public $lead;
    public $authUser;

    public function handle($lead) {

        $this->lead = $lead;

        if (isset($this->lead->json_answers)) {
            $this->lead->data_answers = json_decode($this->lead->json_answers);
        }

        if (isset($this->lead->json_questions)) {
            $this->lead->data_questions = json_decode($this->lead->json_questions);
        }

        if (isset($this->lead->json_result) && is_string($this->lead->json_result)) {
            $this->lead->json_result = json_decode($this->lead->json_result);
        }

        if (isset($this->lead->json_answers_2) && is_string($this->lead->json_answers_2)) {
            $this->lead->json_answers_2 = json_decode($this->lead->json_answers_2);
        }

        if ($this->lead->rsi_webhook_response) {
            if (json_decode($this->lead->rsi_webhook_response)) {
                $this->lead->rsi_webhook_response = json_decode($this->lead->rsi_webhook_response);
            }
            $this->lead->rsi_webhook_response = print_r($this->lead->rsi_webhook_response, 1);
        }

        $this->lead = $lead->only([
            'id',
            'ip',
            'roistat_visit',
            'calltouch_session_id',
            'comagic_session_id',
            'y_client_id',
            'g_client_id',
            'fbp',
            'fbc',
            'fbclid',
            'gclid',
            'yclid',
            'answers2_as_arrays',

            'data_answers',
            'data_questions',
            'json_result',
            'json_answers_2',
            'rsi_webhook_response',

            'sale_value',
            'sale_type',
            'page_url',

            'messenger',
            'contact_messenger',
            'contact_type',

            'created_at_format',
            'created_at',
            'phone',
            'email',
            'name',

            'rsi_webhook_status',
            'status',
            'title',

            'source',
            'medium',
            'campaign',
            'content',
            'term',

            'rsi_webhook_status',
            'status',
            'title',

            'custom_fields',
        ]);

        $this->authUser = Auth::user()->only([
            'id',
            'name',
            'email',
        ]);

        return $this->setOkStatus();
    }
}
