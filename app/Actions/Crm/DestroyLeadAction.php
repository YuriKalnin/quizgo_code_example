<?php
namespace App\Actions\Crm;

use App\Events\Lead\AfterDeleteLead;
use App\Services\Traits\StatusTrait;

class DestroyLeadAction
{
    use StatusTrait;

    public function handle($lead) {

        if ($lead->delete()) {
            event(new AfterDeleteLead( $lead ));
            return $this->setOkStatus();
        }

        return $this->setFailStatus();
    }
}
