<?php

namespace App\Actions\Crm;

use App\Services\Billing\SubscriptionService;
use App\Services\Traits\StatusTrait;
use CrmService;
use QuizGo;

class GetDisableLeadsInfoAction
{
    use StatusTrait;

    public $text = false;

    private $countDisabledLeads;

    public function handle($company) {

        $this->countDisabledLeads = CrmService::getLeadsDisableCollection($company)->count();
        $subscribe = new SubscriptionService();
        $subscribe = $subscribe->info($company);

        if (!$company->account->billing_id) {
            if (!$company->account->is_enable || $this->countDisabledLeads > 0) {
                if ($this->countDisabledLeads > 0) {
                    $this->text = $this->hasBlockedLeadsText();
                }
                elseif (!$company->account->is_enable) {
                    $this->text = $this->enableOnlyTestLedasText();
                }
            }
        } else {
            if(!$subscribe->enable || $this->countDisabledLeads > 0) {
                if ($subscribe->isRateQuiz()) {
                    if ($this->countDisabledLeads > 0) {
                        $this->text = $this->hasBlockedLeadsText();
                    } else {
                        $this->text = $this->enableOnlyTestLedasText();
                    }
                } else {
                    if ($this->countDisabledLeads > 0) {
                        $this->text = $this->hasBlockedLeadsMagnitText();
                    } else {
                        $this->text = $this->enableOnlyTestLedasText();
                    }
                }
            }
        }

        return $this->setOkStatus();
    }

    private function hasBlockedLeadsText() {
        $leads = QuizGo::wordFormMutation($this->countDisabledLeads, "заблокированная заявка", "заблокированные заявки", "заблокированных заявок");
        $their = $this->countDisabledLeads === 1 ? 'её' : 'их';
        return "У Вас есть $this->countDisabledLeads $leads! Чтобы увидеть $their, необходимо активировать тариф.";
    }

    private function hasBlockedLeadsMagnitText() {
        $leads = QuizGo::wordFormMutation($this->countDisabledLeads, "заблокированная заявка", "заблокированные заявки", "заблокированных заявок");
        $their = $this->countDisabledLeads === 1 ? 'её' : 'их';
        return "У Вас есть $this->countDisabledLeads $leads! Чтобы увидеть $their, необходимо купить заявки. <br> После покупки лиды автоматически разблокируются и станут доступны для просмотра.";
    }

    private function enableOnlyTestLedasText() {
        return "В данный момент доступны только тестовые заявки. Чтобы снять это ограничение необходимо активировать тариф.";
    }
}
