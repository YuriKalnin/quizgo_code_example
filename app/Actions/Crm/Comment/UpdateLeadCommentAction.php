<?php
namespace App\Actions\Crm\Comment;

use App\Services\Traits\StatusTrait;

class UpdateLeadCommentAction
{
    use StatusTrait;

    public $comment;

    public function handle($comment, $data) {

        if ($comment->update($data)) {
            return $this->setOkStatus();
        }

        return $this->setFailStatus();
    }
}
