<?php
namespace App\Actions\Crm\Comment;

use App\Services\Traits\StatusTrait;

class GetLeadCommentsAction
{
    use StatusTrait;

    public $comments;

    public function handle($lead) {

        $this->comments = $lead->comments->map(function ($comment) {

            $comment = $comment->load('user');
            $comment->user = $comment->user->only([
                'id',
                'email',
                'name',
            ]);

            return $comment->only([
                'id',
                'created_at_format',
                'message',
                'user_id',
                'user',
            ]);
        });

        return $this->setFailStatus();
    }
}
