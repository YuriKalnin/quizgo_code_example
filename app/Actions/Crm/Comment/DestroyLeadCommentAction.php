<?php
namespace App\Actions\Crm\Comment;

use App\Services\Traits\StatusTrait;

class DestroyLeadCommentAction
{
    use StatusTrait;

    public function handle($comment) {

        if ($comment->delete()) {
            return $this->setOkStatus();
        }

        return $this->setFailStatus();
    }
}
