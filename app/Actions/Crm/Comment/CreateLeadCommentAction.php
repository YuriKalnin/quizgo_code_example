<?php
namespace App\Actions\Crm\Comment;

use App\Services\Traits\StatusTrait;

class CreateLeadCommentAction
{
    use StatusTrait;

    public $comment;

    public function handle($lead, $data) {

        $this->comment = $lead->comments()->create($data);

        if ($this->comment) {
            $this->comment->user = $this->comment->user->only([
                'id',
                'email',
                'name',
            ]);
            $this->comment = $this->comment->only([
                'id',
                'created_at_format',
                'edit_process',
                'message',
                'edit_process',
                'save_process',
                'user_id',
                'user',
            ]);

            return $this->setOkStatus();
        }

        return $this->setFailStatus();
    }
}
