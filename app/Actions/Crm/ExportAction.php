<?php

namespace App\Actions\Crm;

use App\Exports\LeadsExport;
use App\Services\Traits\StatusTrait;
use Carbon\Carbon;
use Excel;
use Maatwebsite\Excel\Excel as ExelType;

class ExportAction
{
    use StatusTrait;

    public $path;
    public $export;

    private $params;

    public function handle($params, $download = true) {

        $this->params = $params;

        $nameFile = $this->fileName();

        $exportFilePath = storage_path("app/public/upload/crm-export/$nameFile");

        if (file_exists( $exportFilePath )) {
            unlink($exportFilePath);
        }

        if ($download) {
            return Excel::download( new LeadsExport($params) , $nameFile, ExelType::CSV, [
                'Content-Type' => 'text/csv'
            ]);
        } else {
            $this->export = Excel::store( new LeadsExport($params) , "upload/crm-export/$nameFile", 'public');
            $this->path = "/upload/crm-export/$nameFile";
        }

        return $this->setOkStatus();
    }

    private function fileName() {
        $nameFile = Carbon::today();
        $nameFile = $nameFile->format('Y-m-d');
        return "{$nameFile}_quiz_ID_{$this->params['quiz_id']}.csv";
    }
}
