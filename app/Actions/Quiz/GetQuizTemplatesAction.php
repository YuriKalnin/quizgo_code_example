<?php

namespace App\Actions\Quiz;

use Cache;
use App\Models\Quiz;
use App\Models\QuizCategory;
use App\Services\Traits\StatusTrait;

class GetQuizTemplatesAction
{
    use StatusTrait;

    public $categories;
    public $templates;

    public function handle() {
        return $this->getCategories()
                    ->getTemplates()
                    ->setOkStatus();
    }

    private function getCategories() {
        $this->categories = Cache::rememberForever("quizCategories", function () {
            return QuizCategory::all()->map(function ($category) {
                return $category->only([
                    'id',
                    'name'
                ]);
            });
        });
        return $this;
    }

    private function getTemplates() {
        $this->templates = Cache::rememberForever("quizTemplate", function () {
            $templates = Quiz::where('template_category_id', '>', 0)->get()->map(function ($template) {
                $template->createdProcess = false;
                $template->imageSrc = json_decode($template->quiz_json)->first_page->image->src;
                return $template->only([
                    'id',
                    'name',
                    'imageSrc',
                    'createdProcess',
                    'template_category_id',
                ]);
            });

            return $templates;

        });
        return $this;
    }
}
