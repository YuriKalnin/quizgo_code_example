<?php

namespace App\Actions\Quiz;

use App\Services\Traits\StatusTrait;
use App\Models\Project;
use CacheService;

class MoveToProjectAction
{
    use StatusTrait;

    public function handle($quiz, $project) {

        CacheService::keyQuizProjects($quiz->project, true);

        $project = Project::find($project);

        $quiz->project_id = $project->id;

        $quiz->lead()->update([
            'project_id' => $project->id,
        ]);

        CacheService::keyQuizProjects($project, true);

        if ($quiz->save()) {
            return $this->setOkStatus(__('quiz-edit.project-move-success'));
        }

        return $this->setFailStatus(__('quiz-edit.error'));
    }
}
