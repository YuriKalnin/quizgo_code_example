<?php
namespace App\Actions\Quiz\Email;

use App\Events\Quiz\AfterUpdatedQuiz;
use App\Services\Traits\StatusTrait;
use App\Models\Quiz;
use Exception;

class UpdateEmailAction
{
    use StatusTrait;

    private $data;
    private $quizCollect;

    public $emailNotifyCollect;

    public function handle($data)
    {
        return $this
            ->setData($data)
            ->update()
            ->setOkStatus();
    }

    private function update() {
        $this->emailNotifyCollect->lang = $this->data['lang'];
        $this->emailNotifyCollect->enabled = $this->data['enabled'];
        if ($this->emailNotifyCollect->save()) {
            event(new AfterUpdatedQuiz($this->quizCollect));
            return $this;
        }
        throw new Exception("Ошибка при обновлении интеграции");
    }

    private function setData($data) {
        $this->data = $data;
        $this->quizCollect = Quiz::findOrFail($data['quiz_id']);
        $this->emailNotifyCollect = $this->quizCollect->emailNotify2;
        return $this;
    }
}
