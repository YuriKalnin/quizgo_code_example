<?php
namespace App\Actions\Quiz\Email;

use App\Services\Traits\StatusTrait;
use App\Mail\QuizLead;
use QuizIntegrationsAdapterService;
use QuizService;
use NotyService;
use Exception;
use Mail;
use Log;

class SendEmailNewLeadAction
{
    use StatusTrait;

    public $lead;
    public $subscribe;
    public $emailNotifyCollect;
    public $quizCollect;
    public $quizService;

    public function handle($lead, $subscribe = true)
    {
        try {
            if (!$this->setData($lead, $subscribe)) {
                return $this->setFailStatus('Лид не отправлен');
            }
            $this->setLang();
            $this->send();
            $this->setOkStatus();
            return $this;
        } catch (Exception $exception) {
            $this->loggingExceptionAfterSend($exception);
            return $this->setFailStatus($exception->getMessage());
        }
    }

    private function loggingInitSend($moreEmailsEnable, $allEmails) {
        $allEmails = $allEmails->pluck('address');
        $allEmails = implode(',', $allEmails->toArray());
        $message = "\n\n";
        $message .= "Инициализация отправки лида на email.\n";
        $message .= "Lead ID: {$this->lead->id}\n";
        $message .= "Quiz ID: {$this->quizCollect->id}\n";
        $message .= "Subscribe: {$this->subscribe}\n";
        $message .= "Все email адреса: {$allEmails}\n";
        $message .= "Улучшение email+: " . ($moreEmailsEnable?'Подключено':'Не подключено');
        Log::channel('email')->debug($message);
    }

    private function loggingBeforeSend($email, $moreEmailsEnable, $allEmails) {
        $allEmails = $allEmails->pluck('address');
        $allEmails = implode(',', $allEmails->toArray());
        $message = "\n";
        $message .= "   Отправка лида на email.\n";
        $message .= "   Lead ID: {$this->lead->id}\n";
        $message .= "   Quiz ID: {$this->quizCollect->id}\n";
        $message .= "   Subscribe: {$this->subscribe}\n";
        $message .= "   Все email адреса: {$allEmails}\n";
        $message .= "   Email получателя: {$email->address}\n";
        $message .= "   Улучшение email+: " . ($moreEmailsEnable?'Подключено':'Не подключено');
        Log::channel('email')->debug($message);
    }

    public function loggingAfterSend($email) {
        $message  = "\n";
        $message .= "       Письмо успешно отправлено\n";
        $message .= "       Email: {$email->address}\n";
        $message .= "       Lead ID: {$this->lead->id}\n";
        $message .= "       Quiz ID: {$this->quizCollect->id}\n";
        $message .= "       Subscribe: {$this->subscribe}";
        Log::channel('email')->debug($message);
    }

    private function loggingExceptionAfterSend($exception, $emailAddress = '') {
        $message = "\n";
        $message .= "       Ошибка при отправке email уведомления.\n";
        $message .= "       Email получателя: $emailAddress\n";
        $message .= "       Lead ID: {$this->lead->id}\n";
        $message .= "       Quiz ID: {$this->quizCollect->id}\n";
        $message .= "       Subscribe: {$this->subscribe}\n";
        $message .= "       Message: {$exception->getMessage()}.\n";
        $message .= "       File: {$exception->getFile()}\n";
        $message .= "       Line: {$exception->getLine()}";
        Log::channel('email')->debug($message);
    }

    private function setLang() {
        if (
            isset($this->emailNotifyCollect->lang)
            && $this->emailNotifyCollect->lang === 'auto'
            && isset($this->quizService->settings->locale)
            && $this->quizService->settings->locale
        ) {
            $lang = $this->quizService->settings->locale;
        }
        elseif (
            isset($this->emailNotifyCollect->lang)
            && $this->emailNotifyCollect->lang
        ) {
            $lang = $this->emailNotifyCollect->lang;
        }
        else {
            $lang = 'ru';
        }

        app()->setLocale($lang);

        return $this;
    }

    private function send() {

        $confirmedEmails = $this->emailNotifyCollect->confirmed_emails;
        $moreEmailsEnable = isset($this->quizService->quizProducts->more_emails) && $this->quizService->quizProducts->more_emails;

        $this->loggingInitSend($confirmedEmails, $confirmedEmails);

        foreach ($confirmedEmails as $emailSendIndex => $email) {

            $this->loggingBeforeSend($email, $moreEmailsEnable, $confirmedEmails);

            if ($emailSendIndex < 1) {
                $this->sendToEmail($email);
            } else {
                if ($moreEmailsEnable) {
                    $this->sendToEmail($email);
                } else {
                    NotyService::sendTelegramMessage("Попытка отправить лид на почту, но улучшение не подключено. Квиз: {$this->quizCollect->id}");
                }
            }

            sleep(1);
        }

        return $this;
    }

    private function sendToEmail($email) {
        try {
            $to = (object) [
                'name' => 'NoName',
                'email' => $email->address,
            ];
            Mail::to($to)->send(new QuizLead($this->lead , $this->subscribe));
            $this->loggingAfterSend($email);
        } catch (Exception $exception) {
            $this->loggingExceptionAfterSend($exception, $email->address);
        }

        return $this;
    }

    private function setData($lead, $subscribe) {

        if (!isset($lead->quiz->emailNotify2->confirmed_emails)) {
            return false;
        }
        if (!$lead->quiz->emailNotify2->enabled) {
            return false;
        }

        $this->lead = $lead;
        $this->subscribe = $subscribe;
        $this->quizCollect = $this->lead->quiz;
        $this->emailNotifyCollect = $this->quizCollect->emailNotify2;
        QuizIntegrationsAdapterService::boot($this->quizCollect);
        $this->quizService = QuizService::get($this->quizCollect);
        $this->quizService = json_decode($this->quizService->defaultQuizTpl);
        return $this;
    }
}
