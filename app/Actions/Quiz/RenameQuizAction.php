<?php

namespace App\Actions\Quiz;

use App\Services\Traits\StatusTrait;

class RenameQuizAction
{
    use StatusTrait;

    public $quiz;

    public function handle($quiz, $data) {
        $this->quiz = $quiz->update($data);
        return $this->setOkStatus('Квиз успешно переименован');
    }
}
