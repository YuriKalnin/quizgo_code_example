<?php

namespace App\Actions\Quiz;

use App\Services\Traits\StatusTrait;
use CacheService;

class MoveToCompanyAction
{
    use StatusTrait;

    public function handle($quiz, $company) {

        $project = $company->project->first();

        $quiz->project_id = $project->id;

        $quiz->lead()->update([
            'project_id' => $project->id,
            'company_id' => $company->id,
        ]);

        $saved = $quiz->save();

        CacheService::keyCompanyProjects($company, true);
        CacheService::keyCountAllQuizProject($company, true);

        if ($saved) {
            return $this->setOkStatus(__('quiz-edit.project-move-success'));
        }

        return $this->setFailStatus(__('quiz-edit.error'));
    }
}
