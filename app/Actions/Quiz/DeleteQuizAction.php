<?php

namespace App\Actions\Quiz;

use SubscriptionService;
use App\Jobs\DeleteQuizRelatedObjects;
use App\Services\Traits\StatusTrait;
use App\Events\Quiz\AfterUpdatedQuiz;
use Illuminate\Support\Facades\Cookie;
use Cache;

class DeleteQuizAction
{
    use StatusTrait;

    public function handle($quiz) {

        SubscriptionService::info($quiz->project->company)->autoUpdateQuizLimit();
        DeleteQuizRelatedObjects::dispatch($quiz)->onQueue('delete.quiz.related.objects');

        if (isset($quiz->domain->subdomain)) {
            Cache::forget("quiz_subdomain_{$quiz->domain->subdomain}");
            $quiz->domain->delete();
        }

        $delete = $quiz->delete();

        Cookie::queue(Cookie::forget("crm_change_quiz"));
        Cookie::queue(Cookie::forget("crm_change_project"));

        event(new AfterUpdatedQuiz( $quiz ));

        if ($delete) {
            return $this->setOkStatus(__('quiz-edit.delete-success'));
        }

        return $this->setFailStatus(__('quiz-edit.delete-error'));
    }
}
