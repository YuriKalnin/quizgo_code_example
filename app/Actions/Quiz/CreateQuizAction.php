<?php

namespace App\Actions\Quiz;

use App\Models\Domain;
use App\Models\Quiz;

use App\Events\Quiz\AfterCreatedQuiz;
use App\Services\Traits\StatusTrait;

use SubscriptionService;
use CacheService;
use QuizService;
use File;
use Arr;

class CreateQuizAction
{
    use StatusTrait;

    public array $quiz;
    private array $data;
    private string $locale;
    private object $quizParams;
    private string $quizParamsJson;

    public function handle($project, $user, $data) {

        $this->data = $data;
        $this->locale = $this->getLocale();
        $this->quizParamsJson = $this->getJsonTemplate();

        $data = Arr::add($data, 'author_id', $user->id);
        $data = Arr::add($data, 'quiz_json', $this->quizParamsJson);

        $quiz = $project->quiz()->create($data);

        if ($quiz)
        {
            $quiz->crmBx24()->create();
            $quiz->emailNotify()->create();
            $quiz->yandexMetrica()->create();
            $quiz->facebookPixel()->create([
                'api_version' => 'v11.0'
            ]);
            $quiz->webHooks()->create();

            $quiz->domain()->create([
                'subdomain' => 'q'.Domain::max('id'),
                'slug' => $quiz->id,
                'mirror' => (new Domain())->genMirror(),
            ]);
            $quiz->vkPixel()->create([
                'event_lead' => 'lead'
            ]);
            $quiz->googleAnalitics()->create([]);
            $quiz->sendpulse()->create([]);
        }

        SubscriptionService::setCompany($project->company)->addQuizLimitToAccount();

        QuizService::autoModerate($quiz);

        event(new AfterCreatedQuiz($quiz, $user));

        CacheService::keyQuizProjects($project, true);
        CacheService::keyClipsProjects($project, true);

        $this->quiz = $quiz->only([
            'id',
            'name',
            'project_id'
        ]);

        return $this->setOkStatus(__('Квиз опрос успешно создан'));
    }

    private function getJsonTemplate() {
        return $this->getBaseJsonTemplate()
                    ->setJsonWidgetOpenSelector()
                    ->getQuizParamsJson();
    }

    private function getQuizParamsJson() {
        return json_encode($this->quizParams);
    }

    private function getBaseJsonTemplate() {
        $quizTemplate = Quiz::find($this->data['template_id']);
        if ($quizTemplate) {
            $this->quizParams = json_decode($quizTemplate->quiz_json);
        } else {
            $base = base_path('resources/assets/panel/json/quiz-default.json');
            $template = base_path("resources/assets/panel/json/quiz-default_{$this->locale}.json");
            $template = file_exists($template) ? $template : $base;
            $this->quizParams = json_decode(File::get($template));
        }
        return $this;
    }

    private function setJsonWidgetOpenSelector() : self {
        $this->quizParams->widget->openSelector = '#q' . time();
        return $this;
    }

    private function getLocale() : string {
        return app()->getLocale();
    }

}
