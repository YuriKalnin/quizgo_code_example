<?php

namespace App\Actions\Quiz\Upload;

use App\Models\System\FileRelation;
use App\Services\Traits\StatusTrait;

class CopyFilesAction
{
    use StatusTrait;

    public $results = [];

    public function handle($quiz, $files) {
        foreach ($files as $file) {
            $this->results = FileRelation::create([
                'quiz_id' => $quiz->id,
                'file_id' => $file,
            ]);
        }
        return $this;
    }
}
