<?php

namespace App\Actions\Quiz\Upload;

use App\Models\File;
use App\Models\System\FileRelation;
use App\Services\Traits\StatusTrait;
use SelectelService;
use Storage;

class RemoveFileAction
{
    use StatusTrait;

    private $disk;

    private $disable = [
        1, 2
    ];

    public function handle($quiz, $fileID) {

        $fileRelationCollect = FileRelation::where('file_id', $fileID)->get();

        if ($fileRelationCollect->count() === 0) {
            return $this->setFailStatus("Файл {$fileID} не найден");
        }

        # в базе данных одна запись по этому файлу
        # и она равна ид переданного квиза
        if ($fileRelationCollect->count() === 1 && $fileRelationCollect->first()->quiz_id == $quiz->id) {
            if ($this->remove($fileID)->isOk()) {
                $fileRelationCollect->first()->delete();
                return $this->setOkStatus('Файл удален');
            }
            return $this->setFailStatus('Ошибка при удалении');
        }

        # в базе данных несколько записей по файлу
        if ($fileRelationCollect->count() > 1) {

            # прохожу по каждой записи
            foreach ($fileRelationCollect as $item) {

                # нахожу связь этого файла по ид квиза и удаляю ее
                if ($item->quiz_id == $quiz->id) { $item->delete(); break; }
            }

            return $this->setOkStatus('Файл был удален');
        }

        return $this->setFailStatus('Ошибка при удалении файла');
    }

    private function remove($fileID) {

        $this->setOkStatus();

        if (!$fileID) {
            return $this->setFailStatus('Не передан ID файла');
        }
        if (in_array($fileID, $this->disable)) {
            return $this->setFailStatus('Файл находится в исключении для удаления');
        }

        $this->disk = Storage::disk('public');
        $rmFile = File::find($fileID);

        if (is_object($rmFile)) {

            if ($rmFile->storage == 'selectel') {
                if (SelectelService::exist($rmFile->path)) {
                    if (SelectelService::delete($rmFile->path)) {
                        $this->deleted = $rmFile->delete();
                    }
                }
            }
            else {
                if ($this->disk->exists($rmFile->path)) {
                    if ($this->disk->delete($rmFile->path)) {
                        $this->deleted = $rmFile->delete();
                    }
                }else{
                    /**
                     * DELETE FILE IN SELECTEL
                     */
                    $rmPathSelectel = str_replace('upload', 'upload/from-local', $rmFile->path);
                    if (SelectelService::exist($rmPathSelectel)) {
                        SelectelService::delete($rmPathSelectel);
                        $this->deleted = $rmFile->delete();
                    }
                }
            }
        }

        return $this;
    }
}
