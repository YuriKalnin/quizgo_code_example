<?php

namespace App\Actions\Quiz\Upload;

use App\Services\Traits\StatusTrait;

class RemoveFilesAction
{
    use StatusTrait;

    public $results = [];

    public function handle($quiz, $files) {

        $results = [];

        foreach ($files as $file) {
            try {
                $results[] = app()->make(RemoveFileAction::class)->handle($quiz, $file);
            } catch (\Exception $exception) {
                $results[] = $this->setFailStatus($exception->getMessage());
            }
        }

        $this->results = $results;

        return $this;
    }
}
