<?php

namespace App\Actions\Quiz;

use App\Services\Traits\StatusTrait;

class GetQuizListAction
{
    use StatusTrait;

    public $quizes;
    public $clips;

    public function handle($project) {
        $this->quizes = $project->quiz_cache->map(function ($quiz) {
            return $quiz->only([
                'id',
                'name',
                'project_id',
                'count_new_leads'
            ]);
        });
        $this->clips = $project->clips_cache->map(function ($clips) {
            return $clips->only([
                'id', 'name', 'project_id',
            ]);
        });
        return $this->setOkStatus();
    }
}
