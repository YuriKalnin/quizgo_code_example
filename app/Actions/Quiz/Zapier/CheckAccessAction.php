<?php
namespace App\Actions\Quiz\Zapier;

use App\Models\Integrations\Zapier;
use App\Services\Traits\StatusTrait;

class CheckAccessAction
{
    use StatusTrait;

    public $zapier;

    public function handle($token) {
        $this->zapier = Zapier::where('token', $token)->get()->first();

        if ($this->zapier) {
            return $this->setOkStatus();
        }

        return $this->setFailStatus('Access denied');
    }
}
