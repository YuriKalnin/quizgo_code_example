<?php
namespace App\Actions\Quiz\Zapier\Triggers\NewLead;

use App\Services\Traits\StatusTrait;
use App\Actions\Lead\GetLeadDataAction as GetLeadDataBasicAction;

class GetLeadDataAction
{
    use StatusTrait;

    private $lead;
    private $quiz;
    private $project;
    private $company;

    public $data;

    public function handle($lead) {

        $action = app()->make(GetLeadDataBasicAction::class)->handle($lead);

        $this->data = $action->data;

        foreach ($this->data['answers_as_arrays2'] as $answer) {
            $this->data["answer_{$answer->slug}_title"] = $answer->question->title;
            $this->data["answer_{$answer->slug}_type"] = $answer->question->type;
            $this->data["answer_{$answer->slug}_value"] = $answer->answer;
        }

        unset($this->data['answers_as_arrays2']);

        return $this->setOkStatus();
    }
}
