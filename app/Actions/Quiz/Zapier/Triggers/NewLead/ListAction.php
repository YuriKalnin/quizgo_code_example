<?php
namespace App\Actions\Quiz\Zapier\Triggers\NewLead;

use App\Services\Traits\StatusTrait;
use App\Actions\Quiz\Zapier\CheckAccessAction;

class ListAction
{
    use StatusTrait;

    private $lead;
    private $quiz;
    private $project;
    private $company;

    public $data;

    public function handle($data) {

        $auth = app()->make(CheckAccessAction::class)->handle($data['api_key']);

        if ($auth->isFail()) {
            return $auth;
        }

        $this->quiz = $auth->zapier->quiz;
        $this->project = $this->quiz->project;
        $this->company = $this->project->company;

        $this->data = [[
            'id' => 0,

            'company' => $this->company->name,
            'company_id' => $this->company->id,

            'project' => $this->project->name,
            'project_id' => $this->project->id,

            'quiz' => $this->quiz->name,
            'quiz_id' => $this->quiz->id,

            'title' => 'Lead title',
            'created_at' => '2022-12-16T19:27:50.000000Z',
            'name' => 'User name',
            'phone' => '+7 (999) 777 33 11',
            'phone_clean' => '+79997773311',
            'email' => 'user-email@example.com',
            'messenger' => 'telegram',
            'contact_messenger' => '+79997773311',
            'utm_source'  => 'utm-source',
            'utm_medium'  => 'utm-medium',
            'utm_campaign'=> 'utm-campaign',
            'utm_content' => 'utm-content',
            'utm_term'    => 'utm-term',
            'answers' => 'All answers to questions',
            'roistat_visit' => '0000000001',
            'comagic_session_id' => '0000000001',
            'y_client_id' => '0000000001',
            'g_client_id' => '0000000001',
            'yclid' => '0000000001',
            'gclid' => '0000000001',
            'fbclid' => '0000000001',
            'fbc' => '0000000001',
            'fbp' => '0000000001',
            'ip' => '0000000001',

            'sale_value' => '',
            'sale_type' => '',
            'recaptcha_rating' => '',
            'calltouch_session_id' => '',
            'page_url' => '',
            'user_agent' => '',
        ]];

        $this->fillAnswers();

        return $this->setOkStatus();
    }

    private function fillAnswers() {
        $questions = array_filter($this->quiz->json_quiz->questions, function ($question) {
            return $question->questionView;
        });
        foreach ($questions as $question) {
            $this->data[0]["answer_{$question->slug}_title"] = $question->title;
            $this->data[0]["answer_{$question->slug}_type"] = $question->type;
            $this->data[0]["answer_{$question->slug}_value"] = 'Answer';
        }
    }
}
