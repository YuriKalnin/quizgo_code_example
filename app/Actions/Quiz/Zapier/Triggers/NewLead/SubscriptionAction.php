<?php
namespace App\Actions\Quiz\Zapier\Triggers\NewLead;

use App\Services\Traits\StatusTrait;
use App\Actions\Quiz\Zapier\CheckAccessAction;

class SubscriptionAction
{
    use StatusTrait;

    public function handle($data) {

        $auth = app()->make(CheckAccessAction::class)->handle($data['api_key']);

        if ($auth->isFail()) {
            return $auth;
        }

        $created = $auth->zapier->webhooks()->updateOrCreate(
            ['url' =>  $data['hookUrl']],
            [
                'status' => 'on',
                'name' =>  $data['integrationName']
            ],
        );

        if ($created) {
            return $this->setOkStatus();
        }

        return $this->setFailStatus();
    }
}
