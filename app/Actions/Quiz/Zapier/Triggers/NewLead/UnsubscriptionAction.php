<?php
namespace App\Actions\Quiz\Zapier\Triggers\NewLead;

use App\Services\Traits\StatusTrait;
use App\Actions\Quiz\Zapier\CheckAccessAction;

class UnsubscriptionAction
{
    use StatusTrait;

    public $zapier;

    public function handle($data) {

        $auth = app()->make(CheckAccessAction::class)->handle($data['api_key']);

        if ($auth->isFail()) {
            return $auth;
        }

        $updated = $auth->zapier->webhooks()->where([
            'url' =>  $data['hookUrl']
        ])->update([
            'status' => 'off',
        ]);

        if ($updated) {
            return $this->setOkStatus();
        }

        return $this->setFailStatus();
    }
}
