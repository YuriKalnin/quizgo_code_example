<?php

namespace Tests\Unit\Services\RateService\Methods\ChangeRateService;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use DevtoolsService;

/**
 * Тестируем метод RateService::change()
 * Смена тарифа
 */
class ChangeRateServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Переход на тариф лидМагнит
     * На счету было 10 заявок и дата активности не активна
     * @return void
     */
    public function testChangeRateQuizToLeadOK()
    {
        $fakeData = DevtoolsService::tests()
                        ->fakeData()
                        ->createCompany();

        $fakeData->account->rate = 'quiz';
        $fakeData->account->lead_limit = 10;
        $fakeData->account->leads_active_date = null;
        $fakeData->account->save();

        $rateService = RateService::change($fakeData->company, 'lead');

        $this->assertTrue($rateService->isOk());
        $this->assertTrue($rateService->accountCollect->rate === 'lead');
        $this->assertTrue(!is_null($rateService->accountCollect->leads_active_date));
        $this->assertTrue($rateService->accountCollect->lead_limit === 0);
    }

    /**
     * Переход на тариф лидМагнит
     * На счету было 10 заявок и дата активности активна
     * @return void
     */
    public function testChangeRateQuizToLead2OK()
    {
        $fakeData = DevtoolsService::tests()
                        ->fakeData()
                        ->createCompany();

        $fakeData->account->rate = 'quiz';
        $fakeData->account->lead_limit = 10;
        $fakeData->account->leads_active_date = now()->addDays(10);
        $fakeData->account->save();

        $rateService = RateService::change($fakeData->company, 'lead');

        $this->assertTrue($rateService->isOk());
        $this->assertTrue($rateService->accountCollect->rate === 'lead');
        $this->assertTrue(!is_null($rateService->accountCollect->leads_active_date));
        $this->assertTrue($rateService->accountCollect->lead_limit === 10);
    }

    /**
     * Переход на тариф лидМагнит
     * На счету было 10 заявок и дата активности не активна
     * @return void
     */
    public function testChangeRateQuizToLead3OK()
    {
        $fakeData = DevtoolsService::tests()
                        ->fakeData()
                        ->createCompany();

        $fakeData->account->rate = 'quiz';
        $fakeData->account->lead_limit = 10;
        $fakeData->account->leads_active_date = now()->subDays(10);
        $fakeData->account->save();

        $rateService = RateService::change($fakeData->company, 'lead');

        $this->assertTrue($rateService->isOk());
        $this->assertTrue($rateService->accountCollect->rate === 'lead');
        $this->assertTrue(!is_null($rateService->accountCollect->leads_active_date));
        $this->assertTrue($rateService->accountCollect->lead_limit === 0);
    }
}
