<?php

namespace Tests\Unit\Services\RateService\Methods\QuizPluse\CheckEndDateCompanyActiveDate;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use DevtoolsService;

/**
 * Тестируем метод RateService::base()->checkEndDateCompanyActiveDate()
 * Генерация события о том что подписка заканчивается
 */
class CheckEndDateCompanyActiveDateTest extends TestCase
{
    use DatabaseTransactions;

    public function testCheckEndDateCompanyActiveDateTestOK()
    {
        $fakeData = DevtoolsService::tests()
                        ->fakeData()
                        ->createCompany();

        $fakeData->account->demo_date = today()->subDays(10);
        $fakeData->account->active_date = today()->addDays(3);
        $fakeData->account->balance = -1;
        $fakeData->account->save();

        $rateService = RateService::quizPlus()->checkEndDateCompanyActiveDate(true);

        $filteredAccounts = $rateService->accounts->filter(function ($account) use ($fakeData) {
            return $fakeData->account->id === $account->id;
        });

        $this->assertTrue(!is_null($filteredAccounts->first()));
    }
}
