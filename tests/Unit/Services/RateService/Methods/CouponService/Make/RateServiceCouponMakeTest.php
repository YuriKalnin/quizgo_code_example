<?php

namespace Tests\Unit\Services\RateService\Methods\CouponService\Make;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use DevtoolsService;

/**
 * Тестируем метод RateService::coupon()->make()
 * Применение купона
 */
class RateServiceCouponMakeTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Успешное применение купона
     * @return void
     */
    public function testRateServiceCouponMakeOK()
    {

        $fakeData = DevtoolsService::tests()
                        ->fakeData()
                        ->createCompany()
                        ->createCoupon();

        $fakeData->account->active_date = today();
        $fakeData->account->save();

        $rateServiceMakeCoupon = RateService::coupon()->make($fakeData->user, $fakeData->company, $fakeData->coupon->coupon);

        $fakeData->reloadCompany();

        $this->assertTrue($rateServiceMakeCoupon->isOk());
        $this->assertTrue($rateServiceMakeCoupon->message === $fakeData->coupon->success_text);
        $this->assertTrue($fakeData->account->quiz_plus_days_enable === 30);
    }

    /**
     * Неудачное применение купона, купон
     * уже применен для пользователя
     * @return void
     */
    public function testRateServiceCouponMakeFailUserRepeat()
    {
        $fakeData = DevtoolsService::tests()
                        ->fakeData()
                        ->createCompany()
                        ->createCoupon();

        $fakeData2 = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createCoupon();

        RateService::coupon()->make($fakeData->user, $fakeData->company, $fakeData->coupon->coupon);
        $rateServiceMakeCoupon = RateService::coupon()->make($fakeData->user, $fakeData2->company, $fakeData->coupon->coupon);

        $this->assertTrue($rateServiceMakeCoupon->isFail());
        $this->assertTrue($rateServiceMakeCoupon->message === "Купон {$fakeData->coupon->coupon} уже применен для пользователя {$fakeData->user->email}");
    }

    /**
     * Неудачное применение купона, купон
     * уже применен для пользователя
     * @return void
     */
    public function testRateServiceCouponMakeFailCompanyRepeat()
    {
        $fakeData = DevtoolsService::tests()
                        ->fakeData()
                        ->createCompany()
                        ->createCoupon();

        $fakeData2 = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createCoupon();

        RateService::coupon()->make($fakeData->user, $fakeData->company, $fakeData->coupon->coupon);
        $rateServiceMakeCoupon = RateService::coupon()->make($fakeData2->user, $fakeData->company, $fakeData->coupon->coupon);

        $this->assertTrue($rateServiceMakeCoupon->isFail());
        $this->assertTrue($rateServiceMakeCoupon->message === "Купон {$fakeData->coupon->coupon} уже применен для компании {$fakeData->company->id}");
    }

    /**
     * Неудачное применение купона, купон не найден
     * @return void
     */
    public function testRateServiceCouponMakeFailNotFound()
    {
        $fakeData = DevtoolsService::tests()
                        ->fakeData()
                        ->createCompany()
                        ->createCoupon();

        $rateServiceMakeCoupon = RateService::coupon()->make($fakeData->user, $fakeData->company, 'BLA-BLA-BLA');

        $this->assertTrue($rateServiceMakeCoupon->isFail());
        $this->assertTrue($rateServiceMakeCoupon->message === 'Купон BLA-BLA-BLA не найден');
    }
}
