<?php

namespace Tests\Unit\Services\RateService\Controllers\CouponController;

use App\Services\DevtoolsService\TestService\FakeRequest\Requests\Rate\Coupon\MakeCouponRequest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use DevtoolsService;

/**
 * Тестируем метод RateService::coupon()->make()
 * Применение купона
 */
class MakeCouponControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Успешное применение купона
     * @return void
     */
    public function testRateServiceCouponMakeOK()
    {
        $request = new MakeCouponRequest($this);

        $request->onBefore(function (MakeCouponRequest $request) {
            $request->fakeData()->account->active_date = today();
            $request->fakeData()->account->save();
        });

        $result = DevtoolsService::tests()->request($request);

        $result->response()->assertJson([
            'status' => true,
            'message' => $result->fakeData()->coupon->success_text,
        ]);

        $result->fakeData()->reloadCompany();

        $this->assertTrue($result->fakeData()->account->quiz_plus_days_enable === 30);
    }

    /**
     * Неудачное применение купона, купон не найден
     * @return void
     */
    public function testRateServiceCouponMakeFailNotFound()
    {
        $request = new MakeCouponRequest($this);

        $request->onBefore(function (MakeCouponRequest $request) {
            $request->coupon = "ВсякаяШолупонь";
        });

        $result = DevtoolsService::tests()->request($request);

        $result->response()->assertJson([
            'status' => false,
            'errors' => [
                'coupon' => [
                    'Купон ВсякаяШолупонь не найден'
                ]
            ],
        ]);
    }
}
