<?php

namespace Tests\Unit\Services\RateService\Actions;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use DevtoolsService;
use BillService;

/**
 * Проверка начисления подарка пользователю при оплате тарифа квиз плюс
 */
class GiveGiftQuizPlusTest extends TestCase
{
    use DatabaseTransactions;

    // тестируем начисление одного месяца в подарок
    public function testAdd1MonthGift() {
        $this->addOneMonthsGift(3, [120, 125]);
    }

    // тестируем начисление двух месяца в подарок
    public function testAdd2MonthGift() {
        $this->addOneMonthsGift(6, [240, 248]);
    }

    // тестируем начисление двух месяца в подарок
    public function testAdd3MonthGift() {
        $this->addOneMonthsGift(12, [450, 465]);
    }

    public function addOneMonthsGift($month, $daysBetween)
    {
        $fakeData = DevtoolsService::tests()
                        ->fakeData()
                        ->createCompany()
                        ->createBill();

        $fakeData->company->project()->first()->quizCache->count();

        // спишем дату активности у компании
        RateService::activeDate()->update($fakeData->company, -10);

        // обнулим демо период у компании
        RateService::base()->resetDemo($fakeData->company->account);

        // получаем инфу о тарифе компании
        $rate = RateService::get($fakeData->company);


        // получим стоимость тарифа в месяц
        // делаем новую стоимость для счета
        $newCostBill = $rate->totalCost->quizPrices->month * $month;

        // обновляем данные в счете
        $fakeData->bill->cost_currency = $newCostBill;
        $fakeData->bill->mont = $month;
        $fakeData->bill->save();

        // оплатим счет
        BillService::payBill($fakeData->bill);

        $fakeData->reloadCompany()->reloadBill();

        $rate = RateService::get($fakeData->company);

        // должно быть от 120 до 124 дней на аккаунте
        $this->assertTrue($rate->availability->payed->days >= $daysBetween[0] && $rate->availability->payed->days <= $daysBetween[1]);

    }
}
