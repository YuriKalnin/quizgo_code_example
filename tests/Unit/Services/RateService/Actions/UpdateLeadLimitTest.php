<?php

namespace Tests\Unit\Services\RateService\Actions;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use DevtoolsService;

class UpdateLeadLimitTest extends TestCase
{
    use DatabaseTransactions;

    // начислим 100 лидов
    public function testAdd100leads()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createLeads();

        $rateService = RateService::leadMagnet()->limit()->update($fakeData->company, 100);
        $this->assertTrue($rateService->isOk());

        // просто так проверяем метод сервиса
        $rateService = RateService::leadMagnet()->journal()->get($fakeData->company);
        $this->assertTrue($rateService->isOk());

        // проверяем есть ли в журнале запись о ста лидах
        $this->assertTrue(
    $fakeData
                ->account
                ->leadMagnetJournal()
                ->where('count', 100)
                ->where('name', 'Ручное начисление лидов')
                ->get()
                ->count() > 0
        );


        // на аккаунте компании должно быть 100 лидов
        $this->assertTrue($rateService->leadMagnetJournal->last()->count === 100);
    }


    // спишем 130 лидов
    public function testMinus100leads()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createLeads();

        $rateService = RateService::leadMagnet()->limit()->update($fakeData->company, 100 * -1);
        $this->assertTrue($rateService->isOk());

        // проверяем есть ли в журнале запись о ста лидах
        $this->assertTrue(
            $fakeData
                ->account
                ->leadMagnetJournal()
                ->where('count', -100)
                ->where('name', 'Ручное списание лидов')
                ->get()
                ->count() > 0
        );

        // просто так проверяем метод сервиса
        $rateService = RateService::leadMagnet()->journal()->get($fakeData->company);
        $this->assertTrue($rateService->isOk());

        // на аккаунте компании должно быть 100 лидов
        $this->assertTrue($rateService->leadMagnetJournal->last()->count === -100);
    }
}
