<?php

namespace Tests\Unit\Services\RateService\Actions;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use DevtoolsService;

/**
 * Тестируем метод RateService::autoPayment
 * Автоматическое продление даты активности компании
 */
class AutoPaymentCompanyAccountTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Тестируем успешное продление дата активности
     * @return void
     */
    public function testAutoPaymentCompanyAccountOK()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        // Делаем компанию не доступной
        $fakeData->account->active_date = now()->subMonth();
        $fakeData->account->demo_date = now()->subMonth();

        // кладем на баланс 500 рублей
        $fakeData->account->balance = 500;

        $fakeData->account->save();

        // проверяем фактическая дата активности не активна
        $this->assertTrue($fakeData->account->quiz_plus_days_enable <= 0);

        // 4 раза подряд запускаем скрипт, дата фактической
        // активности должна увеличиваться на 1 день
        $fakeData = $this->autoPayment($fakeData, 1, true);
        $fakeData = $this->autoPayment($fakeData, 2, true);
        $fakeData = $this->autoPayment($fakeData, 3, true);
        $this->autoPayment($fakeData, 3, false);
    }

    /**
     * Тестируем неудачное продление даты активности
     * На балансе нет денег
     * @return void
     */
    public function testAutoPaymentCompanyAccountFailBalanceNull()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        // Делаем компанию не доступной
        $fakeData->account->active_date = now()->subMonth();
        $fakeData->account->demo_date = now()->subMonth();
        $fakeData->account->balance = -500;

        $fakeData->account->save();

        // проверяем фактическая дата активности не активна
        $this->assertTrue($fakeData->account->quiz_plus_days_enable <= 0);

        // 2 раза подряд запускаем скрипт, дата фактической
        // активности должна оставаться такой же
        $fakeData = $this->autoPaymentNotResult($fakeData);
        $this->autoPaymentNotResult($fakeData);
    }

    public function autoPayment($fakeData, $enableDays, $diffBalance) {

        $preBalance = $fakeData->account->balance;

        // продляем компанию
        RateService::autoPayment([ 'output' => false ]);

        // обновляем данные у компании
        $fakeData->reloadCompany();

        // проверяем что баланс у аккаунта уменьшался
        if ($diffBalance) {
            $this->assertTrue($preBalance > $fakeData->account->balance);
        }
        // проверяем что баланс у аккаунта не уменьшался
        else {
            $this->assertTrue((int) $preBalance === (int) $fakeData->account->balance);
        }

        // проверяем что фактическая дата активности активна
        // фактическая дата активности должна была продлится на $enableDays дней
        $this->assertTrue($fakeData->account->quiz_plus_days_enable === $enableDays);

        return $fakeData;
    }

    public function autoPaymentNotResult($fakeData) {

        $preBalance = $fakeData->account->balance;

        // продляем компанию
        RateService::autoPayment([ 'output' => false ]);

        // обновляем данные у компании
        $fakeData->reloadCompany();

        // проверяем что баланс у аккаунта не уменьшался
        $this->assertTrue((int) $preBalance === (int) $fakeData->account->balance);

        // проверяем что фактическая дата активности не активна
        // фактическая дата активности не должна была продлится, т.к на балансе нет денег
        $this->assertTrue($fakeData->account->quiz_plus_days_enable < 0);

        return $fakeData;
    }
}
