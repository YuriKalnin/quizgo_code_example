<?php

namespace Tests\Unit\Services\RateService\Actions;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use DevtoolsService;

/**
 * Тестируем метод RateService::leadMagnet()->autoremove()
 * Автоматическое удаление лидов
 */
class AutoLeadRemoveTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Успешное удаление лидов
     * @return void
     */
    public function testAutoLeadRemoveOk()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        // Накинем на аккаунт лидов и установим
        // сегодняшнюю дату активности лидов
        $fakeData->account->lead_limit = 200;
        $fakeData->account->leads_active_date = now()->subDay();
        $fakeData->account->save();

        // проверяем кол-во лидов положительное
        $this->assertTrue($fakeData->account->lead_limit === 200);

        // запускаем удаление лидов
        RateService::leadMagnet()->autoremove([
            'output' => false
        ]);

        $fakeData->reloadCompany();

        // проверяем кол-во лидов отрицательное
        $this->assertTrue($fakeData->account->lead_limit === 0);
    }

    /**
     * Лиды еще не время удалять
     * @return void
     */
    public function testAutoLeadRemoveFail()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        // Накинем на аккаунт лидов и установим
        // сегодняшнюю дату активности лидов
        $fakeData->account->lead_limit = 200;
        $fakeData->account->leads_active_date = today();
        $fakeData->account->save();

        // проверяем кол-во лидов положительное
        $this->assertTrue($fakeData->account->lead_limit === 200);

        // запускаем удаление лидов
        RateService::leadMagnet()->autoremove([
            'output' => false
        ]);

        $fakeData->reloadCompany();

        // проверяем кол-во лидов отрицательное
        $this->assertTrue($fakeData->account->lead_limit === 200);
    }

    /**
     * Тестируем неудачное удаление лидов
     * Дата еще не подошла
     * @return void
     */
    public function testAutoLeadRemoveFailDateIsFuture()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        // Накинем на аккаунт лидов и установим
        // сегодняшнюю дату активности лидов
        $fakeData->account->lead_limit = 200;
        $fakeData->account->leads_active_date = today()->addDay();
        $fakeData->account->save();

        // проверяем кол-во лидов положительное
        $this->assertTrue($fakeData->account->lead_limit === 200);

        // запускаем удаление лидов
        RateService::leadMagnet()->autoremove([
            'output' => false
        ]);

        $fakeData->reloadCompany();

        // проверяем кол-во лидов положительное
        $this->assertTrue($fakeData->account->lead_limit === 200);
    }

    /**
     * Тестируем неудачное удаление лидов
     * Дата активности лидов не ограничена
     * @return void
     */
    public function testAutoLeadRemoveFailDateIsUnlimited()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        // Накинем на аккаунт лидов и установим
        // сегодняшнюю дату активности лидов
        $fakeData->account->lead_limit = 200;
        $fakeData->account->leads_active_date = null;
        $fakeData->account->save();

        // проверяем кол-во лидов положительное
        $this->assertTrue($fakeData->account->lead_limit === 200);

        // запускаем удаление лидов
        RateService::leadMagnet()->autoremove([
            'output' => false
        ]);

        $fakeData->reloadCompany();

        // проверяем кол-во лидов положительное
        $this->assertTrue($fakeData->account->lead_limit === 200);
    }
}
