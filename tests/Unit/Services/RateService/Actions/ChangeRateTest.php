<?php

namespace Tests\Unit\Services\RateService\Actions;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use DevtoolsService;

class ChangeRateTest extends TestCase
{
    use DatabaseTransactions;

    // обновил тариф на лид магнит
    public function testChangeToLeadMagnet()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createLeads();
        $rateService = RateService::change($fakeData->company, 'lead');
        $this->assertTrue($rateService->isOk());
        $this->assertTrue($fakeData->reloadCompany()->account->isLeadMagnet());
    }

    // обновил тариф на квиз плюс
    public function testChangeToQuizPlus()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createLeads();
        $rateService = RateService::change($fakeData->company, 'quiz');
        $this->assertTrue($rateService->isOk());
        $this->assertTrue($fakeData->reloadCompany()->account->isQuizPlus());
    }
}
