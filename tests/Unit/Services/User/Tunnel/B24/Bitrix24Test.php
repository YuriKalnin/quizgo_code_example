<?php

namespace Tests\Unit\Services\User\Tunnel\B24;

use Tests\TestCase;
use App\Services\User\Tunnel\B24\Bitrix24;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class Bitrix24Test extends TestCase
{
    use DatabaseTransactions;

    public function testIsAuth()
    {
        $isAuth = app()->make(Bitrix24::class)->isAuth([
            'application_token' => config('quizgo.bitrix24.token')
        ]);

        $this->assertTrue($isAuth);
    }
}
