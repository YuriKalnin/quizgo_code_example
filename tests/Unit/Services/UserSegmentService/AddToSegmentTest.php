<?php

namespace Tests\Unit\Services\UserSegmentService;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use UserSegmentService;
use DevtoolsService;

class AddToSegmentTest extends TestCase
{
    use DatabaseTransactions;

    public function testAddSegment()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        $this->assertNotTrue(UserSegmentService::userInSegment($fakeData->user, 'did_not_visit_for_2_days'));

        $addedSegment = UserSegmentService::addToSegment($fakeData->user, 'did_not_visit_for_2_days');
        $this->assertTrue($addedSegment->isOk());

        $this->assertTrue(UserSegmentService::userInSegment($fakeData->user, 'did_not_visit_for_2_days'));

        $addedSegment = UserSegmentService::addToSegment($fakeData->user, 'did_not_visit_for_2_days');
        $this->assertTrue($addedSegment->isFail());

        $addedSegment = UserSegmentService::addToSegment($fakeData->user, 'did_not_visit_for_2_days', false);
        $this->assertTrue($addedSegment->isOk());
    }
}
