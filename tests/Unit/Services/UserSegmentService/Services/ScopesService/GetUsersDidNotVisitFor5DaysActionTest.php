<?php

namespace Tests\Unit\Services\UserSegmentService\Services\ScopesService;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use UserSegmentService;
use DevtoolsService;

class GetUsersDidNotVisitFor5DaysActionTest extends TestCase
{
    use DatabaseTransactions;

    public function testRegisterAndLastLogin5DaysAgo()
    {
        $this->registerAndLastLoginNDaysAgoTest([5, 5, 5], [5, 5, 5], 3);
        $this->registerAndLastLoginNDaysAgoTest([6, 5, 5], [5, 5, 5], 2);
        $this->registerAndLastLoginNDaysAgoTest([6, 4, 5], [5, 2, 5], 1);
    }

    public function registerAndLastLoginNDaysAgoTest($createAt, $lastLogin, $filterUserCount)
    {
        $fakeData1 = DevtoolsService::tests()->fakeData()->createCompany();
        $fakeData2 = DevtoolsService::tests()->fakeData()->createCompany();
        $fakeData3 = DevtoolsService::tests()->fakeData()->createCompany();

        $fakeData1->user->created_at = today()->subDays($createAt[0]);
        $fakeData1->user->last_login = today()->subDays($lastLogin[0]);
        $fakeData1->user->save();

        $fakeData2->user->created_at = today()->subDays($createAt[1]);
        $fakeData2->user->last_login = today()->subDays($lastLogin[1]);
        $fakeData2->user->save();

        $fakeData3->user->created_at = today()->subDays($createAt[2]);
        $fakeData3->user->last_login = today()->subDays($lastLogin[2]);
        $fakeData3->user->save();

        $userSegmentScope = UserSegmentService::scopes()->getUsersDidNotVisitForNDays(5, 5, 'did_not_visit_for_5_days');

        $users = $userSegmentScope->getUsers();

        $filterUser = $users->filter(function ($user) use ($fakeData1, $fakeData2, $fakeData3) {
            return ($user->id === $fakeData1->user->id || $user->id === $fakeData2->user->id || $user->id === $fakeData3->user->id);
        });

        $this->assertTrue($filterUser->count() === $filterUserCount);
    }
}
