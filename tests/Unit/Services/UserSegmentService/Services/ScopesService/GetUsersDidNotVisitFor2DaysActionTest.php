<?php

namespace Tests\Unit\Services\UserSegmentService\Services\ScopesService;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use UserSegmentService;
use DevtoolsService;

class GetUsersDidNotVisitFor2DaysActionTest extends TestCase
{
    use DatabaseTransactions;

    public function testRegisterAndLastLogin2DaysAgo()
    {
        $this->registerAndLastLoginNDaysAgoTest([2, 2, 2], [2, 2, 2], 3);
    }

    public function testRegisterAndLastLogin4DaysAgo()
    {
        $this->registerAndLastLoginNDaysAgoTest([4, 4, 4], [4, 4, 4], 0);
    }

    public function testRegisterAndLastLogin5DaysAgo()
    {
        $this->registerAndLastLoginNDaysAgoTest([5, 5, 5], [5, 5, 5], 0);
    }

    public function testRegisterAndLastLogin1DaysAgo()
    {
        $this->registerAndLastLoginNDaysAgoTest([1, 1, 1], [1, 1, 1], 0);
    }

    public function testRegister3DaysAgoLastLogin1DaysAgo()
    {
        $this->registerAndLastLoginNDaysAgoTest([3, 3, 3], [1, 1, 1], 0);
    }

    public function testOneUser()
    {
        $this->registerAndLastLoginNDaysAgoTest([10, 7, 4], [3, 2, 1], 0);
        $this->registerAndLastLoginNDaysAgoTest([10, 7, 2], [3, 2, 2], 1);
        $this->registerAndLastLoginNDaysAgoTest([10, 2, 1], [3, 1, 1], 0);
        $this->registerAndLastLoginNDaysAgoTest([2, 2, 1], [2, 2, 1], 2);
    }

    public function registerAndLastLoginNDaysAgoTest($createAt, $lastLogin, $filterUserCount)
    {
        $fakeData1 = DevtoolsService::tests()->fakeData()->createCompany();
        $fakeData2 = DevtoolsService::tests()->fakeData()->createCompany();
        $fakeData3 = DevtoolsService::tests()->fakeData()->createCompany();

        $fakeData1->user->created_at = today()->subDays($createAt[0]);
        $fakeData1->user->last_login = today()->subDays($lastLogin[0]);
        $fakeData1->user->save();

        $fakeData2->user->created_at = today()->subDays($createAt[1]);
        $fakeData2->user->last_login = today()->subDays($lastLogin[1]);
        $fakeData2->user->save();

        $fakeData3->user->created_at = today()->subDays($createAt[2]);
        $fakeData3->user->last_login = today()->subDays($lastLogin[2]);
        $fakeData3->user->save();

        $userSegmentScope = UserSegmentService::scopes()->getUsersDidNotVisitForNDays();

        $users = $userSegmentScope->getUsers();

        $filterUser = $users->filter(function ($user) use ($fakeData1, $fakeData2, $fakeData3) {
            return ($user->id === $fakeData1->user->id || $user->id === $fakeData2->user->id || $user->id === $fakeData3->user->id);
        });

        $this->assertTrue($filterUser->count() === $filterUserCount);
    }
}
