<?php

namespace Tests\Unit\Services\UserSegmentService\Services\ScopesService;

use App\Services\DevtoolsService\TestService\FakeRequest\Requests\SendLeadRequest;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use UserSegmentService;
use DevtoolsService;
use App\Models\Lead;

class CheckUserCreateQuizAndGetLeadActionTest extends TestCase
{
    use DatabaseTransactions;

    public function testCreateFirstLeadFail()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {
            $request->fakeData()->user->created_at = now()->subMonths(4);
            $request->fakeData()->user->save();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        $lead = Lead::find($result->responseJson()['lead']['lead']['id']);

        $check = UserSegmentService::scopes()->checkUserCreateQuizAndGetLead($lead);
        $this->assertTrue($check->isFail());
        $this->assertTrue($check->getStatus()->message === 'Пользователь зарегистрировался более 15 дней назад');
        $this->assertNotTrue(UserSegmentService::userInSegment($result->fakeData()->user, 'get_lead'));
    }

    public function testCreateFirstLeadOk()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        $lead = Lead::find($result->responseJson()['lead']['lead']['id']);

        $check = UserSegmentService::scopes()->checkUserCreateQuizAndGetLead($lead);

        $this->assertTrue($check->isOk());
        $this->assertTrue(UserSegmentService::userInSegment($result->fakeData()->user, 'get_lead'));
    }

    public function assertCreateLead($result) {

        $result->response()
            ->assertStatus(200)
            ->assertJsonStructure([
                'lead' => [
                    'lead' => [
                        'id'
                    ]
                ],
                'status',
                'response',
            ])->assertJson([
                'status' => 1
            ]);
    }
}
