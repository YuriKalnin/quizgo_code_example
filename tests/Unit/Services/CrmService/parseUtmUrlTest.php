<?php

namespace Tests\Unit\Services\CrmService;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use CrmService;

/**
 * Тестируем метод CrmService::parseUtmUrl()
 */
class parseUtmUrlTest extends TestCase
{
    use DatabaseTransactions;

    public function testPhoneTestWithCode()
    {
        $utm = CrmService::parseUtmUrl('');

        $this->assertTrue(
            $utm['source'] === ''
            && $utm['medium'] === ''
            && $utm['campaign'] === ''
            && $utm['content'] === ''
            && $utm['term'] === ''
        );

        $utm = CrmService::parseUtmUrl('http://quizgo-app.localhost/q/116');

        $this->assertTrue(
            $utm['source'] === ''
            && $utm['medium'] === ''
            && $utm['campaign'] === ''
            && $utm['content'] === ''
            && $utm['term'] === ''
        );

        $utm = CrmService::parseUtmUrl('http://quizgo-app.localhost/q/116?utm_source=google');

        $this->assertTrue(
            $utm['source'] === 'google'
            && $utm['medium'] === ''
            && $utm['campaign'] === ''
            && $utm['content'] === ''
            && $utm['term'] === ''
        );

        $utm = CrmService::parseUtmUrl('http://quizgo-app.localhost/q/116?utm_source=google&utm_medium=cpc');

        $this->assertTrue(
            $utm['source'] === 'google'
            && $utm['medium'] === 'cpc'
            && $utm['campaign'] === ''
            && $utm['content'] === ''
            && $utm['term'] === ''
        );

        $utm = CrmService::parseUtmUrl('http://quizgo-app.localhost/q/116?utm_source=google&utm_medium=cpc&utm_campaign={network}');

        $this->assertTrue(
            $utm['source'] === 'google'
            && $utm['medium'] === 'cpc'
            && $utm['campaign'] === '{network}'
            && $utm['content'] === ''
            && $utm['term'] === ''
        );

        $utm = CrmService::parseUtmUrl('http://quizgo-app.localhost/q/116?utm_source=google&utm_medium=cpc&utm_campaign={network}&utm_content={creative}');

        $this->assertTrue(
            $utm['source'] === 'google'
            && $utm['medium'] === 'cpc'
            && $utm['campaign'] === '{network}'
            && $utm['content'] === '{creative}'
            && $utm['term'] === ''
        );

        $utm = CrmService::parseUtmUrl('http://quizgo-app.localhost/q/116?utm_source=google&utm_medium=cpc&utm_campaign={network}&utm_content={creative}&utm_term={keyword}');

        $this->assertTrue(
            $utm['source'] === 'google'
            && $utm['medium'] === 'cpc'
            && $utm['campaign'] === '{network}'
            && $utm['content'] === '{creative}'
            && $utm['term'] === '{keyword}'
        );

    }
}
