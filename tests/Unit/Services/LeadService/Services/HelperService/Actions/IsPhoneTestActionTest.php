<?php

namespace Tests\Unit\Services\LeadService\Services\HelperService\Actions;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use LeadService;

/**
 * Тестируем метод LeadService::helpers()->IsPhoneTest()
 */
class IsPhoneTestActionTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Передали тестовый номер телефона с кодом страны
     * @return void
     */
    public function testPhoneTestWithCode()
    {
        $this->assertTrue(LeadService::helpers()->IsPhoneTest('+7 (999) 999-99-99'));
        $this->assertTrue(LeadService::helpers()->IsPhoneTest('(999) 999-99-99'));
        $this->assertTrue(LeadService::helpers()->IsPhoneTest('+1-999-999-9999'));
        $this->assertTrue(LeadService::helpers()->IsPhoneTest('+1 (999) 999-9999'));
        $this->assertTrue(LeadService::helpers()->IsPhoneTest('(999) 999-9999 x999'));
        $this->assertTrue(LeadService::helpers()->IsPhoneTest('+357 99 999-999'));

        $this->assertFalse(LeadService::helpers()->IsPhoneTest('+7 (999) 999-99-91'));
        $this->assertFalse(LeadService::helpers()->IsPhoneTest('(999) 999-99-91'));
        $this->assertFalse(LeadService::helpers()->IsPhoneTest('(918) 971-40-96'));
        $this->assertFalse(LeadService::helpers()->IsPhoneTest('+7 (918) 971-40-96'));
        $this->assertFalse(LeadService::helpers()->IsPhoneTest('+1-361-209-4861'));
        $this->assertFalse(LeadService::helpers()->IsPhoneTest('+1 (942) 596-2686'));
        $this->assertFalse(LeadService::helpers()->IsPhoneTest('(995) 717-8209 x509'));
    }
}
