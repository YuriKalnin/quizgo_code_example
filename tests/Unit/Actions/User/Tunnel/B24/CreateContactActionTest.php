<?php

namespace Tests\Unit\Actions\User\Tunnel\B24;

use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Actions\User\Tunnel\B24\CreateContactAction;
use App\Services\User\Tunnel\B24\Bitrix24;

/**
 * Тестируем action CreateUserTunnelAction
 */
class CreateContactActionTest extends TestCase
{
    use DatabaseTransactions;

    public function testCreateContactAction()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        $createContact = (new CreateContactAction())->handle($fakeData->user);
        $this->assertTrue($createContact->contact === $createContact->tunnel->bx24_contact);

        $createContact = (new CreateContactAction())->handle($fakeData->user);
        $this->assertTrue($createContact->message === 'Контакт с таким email адресом уже есть');

        $fakeData->reloadUser();

        $deleteContact = (new Bitrix24())->deleteContact($fakeData->user->tunnel->bx24_contact);
        $this->assertTrue($deleteContact->result);
    }
}
