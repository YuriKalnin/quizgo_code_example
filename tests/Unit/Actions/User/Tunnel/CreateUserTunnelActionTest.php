<?php

namespace Tests\Unit\Actions\User\Tunnel;

use App\Actions\User\Tunnel\CreateUserTunnelAction;
use Cache;
use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use function app;

/**
 * Тестируем action CreateUserTunnelAction
 */
class CreateUserTunnelActionTest extends TestCase
{
    use DatabaseTransactions;

    public function testCreateUserTunnelAction()
    {
        $fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany();

        Cache::forget('user_tunnel_state');

        $tunnel = app()->make(CreateUserTunnelAction::class)->handle($fakeData->user);
        $this->assertTrue($tunnel->tunnel->state === 'none');

        $tunnel = app()->make(CreateUserTunnelAction::class)->handle($fakeData->user);
        $this->assertTrue($tunnel->tunnel->state === 'none');

        $tunnel = app()->make(CreateUserTunnelAction::class)->handle($fakeData->user);
        $this->assertTrue($tunnel->tunnel->state === 'wait');

        $tunnel = app()->make(CreateUserTunnelAction::class)->handle($fakeData->user);
        $this->assertTrue($tunnel->tunnel->state === 'none');
    }
}
