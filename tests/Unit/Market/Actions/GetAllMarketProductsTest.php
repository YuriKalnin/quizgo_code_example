<?php

namespace Tests\Unit\Market\Actions;

use App\Models\Market\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GetAllMarketProductsTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetProducts()
    {
        $products = Product::allCache();

        foreach ($products as $product) {
            $this->assertArrayHasKey('id', $product);
            $this->assertArrayHasKey('name', $product);
            $this->assertArrayHasKey('preview_text', $product);
            $this->assertArrayHasKey('detail_text', $product);
            $this->assertArrayHasKey('price', $product);
            $this->assertArrayHasKey('sale', $product);
            $this->assertArrayHasKey('sale_type', $product);
            $this->assertArrayHasKey('preview_image', $product);
            $this->assertArrayHasKey('detail_image', $product);
            $this->assertArrayHasKey('count_installs', $product);
            $this->assertArrayHasKey('created_at', $product);
            $this->assertArrayHasKey('updated_at', $product);
        }

        $this->assertTrue(is_string($products->toJson()));
    }
}
