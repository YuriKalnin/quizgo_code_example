<?php

namespace Tests\Unit\Http\Controllers\Users\Tunnel;

use App\Services\DevtoolsService\TestService\FakeRequest\Requests\User\Tunnel\ConfirmTunnelBothelpRequest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use DevtoolsService;
use App\Actions\User\Tunnel\CreateUserTunnelAction;
use App\Services\User\Tunnel\B24\Bitrix24;

class BothelpControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testConfirm() {

        // Запрос на подтверждение профиля пользователя через мессенджер
        // tunnel/bothelp/confirm
        $request = new ConfirmTunnelBothelpRequest($this);

        // Тестируем акшн CreateUserTunnelAction
        $request->onBefore(function (ConfirmTunnelBothelpRequest $request) {
            app()->make(CreateUserTunnelAction::class)->handle($request->fakeData()->user);
        });

        // Выполняем запрос
        $result = DevtoolsService::tests()->request($request);

        // Проверка ответа от апи
        // Запрос должен запустить событие ConfirmUserTunnel
        // После запрос должна создастся сделка в битриксе и контакт
        $result->response()->assertJsonStructure([
            'valid',
            'demo',
            'company_name',
            'company_url',
            'company_url_payment',
            'status',
            'message',
        ]);

        $request->fakeData()->reloadCompany();
        $company = $request->fakeData()->company;
        $user = $request->fakeData()->user;

        // Проверяем создана ли сделка в Б24
        $this->checkDeal($user, $company);

        // Тестируем запрос tunnel/bothelp/template
        $deal = $this->testTunnelBothelpTemplate($user, $company);

        // Удалим сделку
        $this->deleteDeal($company);

        // Удалим воронку
        $this->deleteContact($deal);
    }

    public function deleteContact($deal) {
        $deleteContact = (new Bitrix24())->deleteContact($deal->result->CONTACT_ID);
        $this->assertTrue($deleteContact->result);
    }

    public function deleteDeal($company) {
        $deleteDeal = (new Bitrix24())->deleteDeal($company->b24_deal_id);
        $this->assertTrue($deleteDeal->result);
    }

    public function testTunnelBothelpTemplate($user, $company) {
        $data = ['email' => $user->email];
        $response = $this->post("/api/service/user/tunnel/bothelp/template", $data);
        $response->assertJson([
            'status' => true,
            'message' => 'OK',
        ]);
        $deal = (new Bitrix24())->getDeal($company->b24_deal_id);
        $this->assertTrue($deal->result->STAGE_ID == 2);
        return $deal;
    }

    public function checkDeal($user, $company) {
        $deal = (new Bitrix24())->getDeal($company->b24_deal_id);
        $this->assertTrue((int) $deal->result->ID === (int) $company->b24_deal_id);
        $this->assertTrue((int) $deal->result->CONTACT_ID === (int) $user->tunnel->bx24_contact);
    }
}
