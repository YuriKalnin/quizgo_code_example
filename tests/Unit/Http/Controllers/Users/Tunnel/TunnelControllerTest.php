<?php

namespace Tests\Unit\Http\Controllers\Users\Tunnel;

use App\Models\Market\Product;
use App\Services\DevtoolsService\TestService\FakeRequest\Requests\Rate\Coupon\MakeCouponRequest;
use App\Services\DevtoolsService\TestService\FakeRequest\Requests\User\Tunnel\TunnelStateRequest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use DevtoolsService;
use App\Actions\User\Tunnel\CreateUserTunnelAction;

class TunnelControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testState() {

        $request = new TunnelStateRequest($this);

        $request->onBefore(function (TunnelStateRequest $request) {
            app()->make(CreateUserTunnelAction::class)->handle($request->fakeData()->user);
        });

        $result = DevtoolsService::tests()->request($request);

        $result->response()->assertJsonStructure([
            'status',
            'tunnel' => [
                "id",
                "user_id",
                "state",
                "bx24_contact",
                "bh_id",
                "bh_cuid",
                "bh_bot_referral",
                "created_at",
                "updated_at",
            ],
            'qr',
        ]);
    }
}
