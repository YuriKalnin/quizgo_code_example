<?php

namespace Tests\Browser\Services\Rate;

use DevtoolsService;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

/**
 * Тестирование сообщения при недоступном
 * тарифе на странице crm системы
 */
class NotyRateDisableCrmPageTest extends DuskTestCase
{
    /**
     * Новый тариф квиз+. Тариф активен только положительным балансом. Сообщения быть не должно.
     * @return void
     * @throws \Throwable
     */
    public function testNoNotyQuizPlus()
    {
        $this->browse(function (Browser $browser) {

            $fakeData = DevtoolsService::tests()
                            ->fakeData()
                            ->createCompany();

            // переведем компанию в демо период
            // спишем дату активности и сделаем баланс отрицательным
            $fakeData->account->demo_date = now()->subDay();
            $fakeData->account->active_date = now()->subDay();
            $fakeData->account->balance = 100;
            $fakeData->account->save();

            $browser
                ->loginAs($fakeData->user)
                ->visit(route('crm.index', $fakeData->company, false))
                ->assertDontSee('В данный момент доступны только тестовые заявки. Чтобы снять это ограничение необходимо активировать тариф.')
                ->assertDontSee('У Вас есть')
                ->assertDontSee('Чтобы увидеть')
            ;

            $fakeData->delete();
        });
    }

    /**
     * Новый тариф квиз+. Тариф не активен. Сообщение должно быть.
     * сообщения быть должно
     * @return void
     * @throws \Throwable
     */
    public function testNotyRateQuizPlusDisable()
    {
        $this->browse(function (Browser $browser) {

            $fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany();

            // переведем компанию в демо период
            // спишем дату активности и сделаем баланс отрицательным
            $fakeData->account->demo_date = now()->subDay();
            $fakeData->account->active_date = now()->subDay();
            $fakeData->account->balance = -100;
            $fakeData->account->save();

            $browser
                ->loginAs($fakeData->user)
                ->visit(route('crm.index', $fakeData->company, false))
                ->assertSee('В данный момент доступны только тестовые заявки. Чтобы снять это ограничение необходимо активировать тариф.')
                ->assertDontSee('У Вас есть')
                ->assertDontSee('Чтобы увидеть')
            ;

            $fakeData->delete();
        });
    }

    /**
     * Новый тариф квиз+. Тариф активен. Есть заблокированные лиды. Сообщение должно быть.
     * @return void
     * @throws \Throwable
     */
    public function testNotyRateQuizPlusDisableLeads()
    {
        $this->browse(function (Browser $browser) {

            $fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createLeads();

            $fakeData->account->demo_date = today()->subMonth();
            $fakeData->account->active_date = today()->addMonth();
            $fakeData->account->balance = -100;
            $fakeData->account->save();

            $browser
                ->loginAs($fakeData->user)
                ->visit(route('crm.index', $fakeData->company, false))
                ->assertDontSee('В данный момент доступны только тестовые заявки. Чтобы снять это ограничение необходимо активировать тариф.')
                ->assertSee('У Вас есть')
                ->assertSee('Чтобы увидеть')
            ;

            $fakeData->delete();
        });
    }

    /**
     * Новый тариф лидмагнит+. Тариф активен. Сообщения быть не должно
     * @return void
     * @throws \Throwable
     */
    public function testNoNotyLeadMagnet()
    {
        $this->browse(function (Browser $browser) {

            $fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany();

            // переведем компанию в демо период
            // спишем дату активности и сделаем баланс отрицательным
            $fakeData->account->demo_date = now()->subMonth();
            $fakeData->account->active_date = now()->subMonth();
            $fakeData->account->balance = -100;
            $fakeData->account->lead_limit = 1;
            $fakeData->account->rate = 'lead';
            $fakeData->account->save();

            $browser
                ->loginAs($fakeData->user)
                ->visit(route('crm.index', $fakeData->company, false))
                ->assertDontSee('В данный момент доступны только тестовые заявки. Чтобы снять это ограничение необходимо активировать тариф.')
                ->assertDontSee('У Вас есть')
                ->assertDontSee('Чтобы увидеть')
            ;

            $fakeData->delete();
        });
    }

    /**
     * Новый тариф лидмагнит. Тариф не доступен. Должно быть сообщение.
     * @return void
     * @throws \Throwable
     */
    public function testNotyRateLeadMagnetDisable()
    {
        $this->browse(function (Browser $browser) {

            $fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany();

            $fakeData->account->demo_date = now()->subMonth();
            $fakeData->account->active_date = now()->subMonth();
            $fakeData->account->balance = -100;
            $fakeData->account->lead_limit = -1;
            $fakeData->account->rate = 'lead';
            $fakeData->account->save();

            $browser
                ->loginAs($fakeData->user)
                ->visit(route('crm.index', $fakeData->company, false))
                ->assertSee('В данный момент доступны только тестовые заявки. Чтобы снять это ограничение необходимо активировать тариф.')
                ->assertDontSee('У Вас есть')
                ->assertDontSee('Чтобы увидеть')
            ;

            $fakeData->delete();
        });
    }

    /**
     * Новый тариф ЛидМагнит. Тариф доступен, но есть заблокированные заявки.
     * @return void
     * @throws \Throwable
     */
    public function testNotyRateLeadMagnetDisableLeads()
    {
        $this->browse(function (Browser $browser) {

            $fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createLeads();

            $fakeData->account->demo_date = now()->subMonth();
            $fakeData->account->active_date = now()->subMonth();
            $fakeData->account->balance = -100;
            $fakeData->account->lead_limit = 1;
            $fakeData->account->rate = 'lead';
            $fakeData->account->save();

            $browser
                ->loginAs($fakeData->user)
                ->visit(route('crm.index', $fakeData->company, false))
                ->assertDontSee('В данный момент доступны только тестовые заявки. Чтобы снять это ограничение необходимо активировать тариф.')
                ->assertSee('У Вас есть')
                ->assertSee('Чтобы увидеть')
            ;

            $fakeData->delete();
        });
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
