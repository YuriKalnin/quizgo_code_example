<?php

namespace Tests\Browser\Services\Rate;

use DevtoolsService;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

/**
 * Применение купона
 * на странице тарифа
 */
class MakeCouponTest extends DuskTestCase
{
    public function testMakeCouponOk()
    {
        $this->browse(function (Browser $browser) {

            $fakeData = DevtoolsService::tests()
                            ->fakeData()
                            ->createCompany()
                            ->createCoupon();

            $browser
                ->loginAs($fakeData->user)
                ->visit("/")
                ->clickLink('Тариф')
                ->waitForText('Ввести промокод')
                ->clickLink('Ввести промокод')
                ->waitForText('Применение купона')
                ->waitForText('Введите купон')
                ->type('coupon', $fakeData->coupon->coupon)
                ->press('Применить')
                ->waitForText("Купон {$fakeData->coupon->coupon} успешно применен!")
                ->assertSee("Купон {$fakeData->coupon->coupon} успешно применен!")
                ->assertSee("Осталось 37 дней")
                ->screenshot(1)
            ;

            $fakeData->delete();
        });
    }

    public function testMakeCouponFail()
    {
        $this->browse(function (Browser $browser) {

            $fakeData = DevtoolsService::tests()
                            ->fakeData()
                            ->createCompany()
                            ->createCoupon();

            $browser
                ->loginAs($fakeData->user)
                ->visit("/")
                ->clickLink('Тариф')
                ->waitForText('Ввести промокод')
                ->clickLink('Ввести промокод')
                ->waitForText('Применение купона')
                ->waitForText('Введите купон')
                ->type('coupon', 'ШОЛУПОНЬ')
                ->press('Применить')
                ->pause(3000)
                ->assertSee("Купон ШОЛУПОНЬ не найден")
                ->screenshot(2)
            ;

            $fakeData->delete();
        });
    }


    public function tearDown(): void
    {
        parent::tearDown();
    }
}
