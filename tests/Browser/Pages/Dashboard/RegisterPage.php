<?php

namespace Tests\Browser\Pages\Dashboard;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;
use Tests\Browser\Components\Dashboard\Navbar;
use Tests\Browser\Components\Dashboard\Sidebar;

class RegisterPage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/register';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  \Laravel\Dusk\Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser
            ->type('@inputEmail', 'testClient1@mail.ru')
            ->type('password', 'password')
            ->press('Зарегистрироваться')
            ->assertSee('Отправка')
            ->within(new Navbar, function ($browser) { })
            ->within(new Sidebar, function ($browser) { });
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@inputEmail' => '[type="email"]',
        ];
    }
}
