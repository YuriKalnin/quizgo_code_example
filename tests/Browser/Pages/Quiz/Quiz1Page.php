<?php

namespace Tests\Browser\Pages\Quiz;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Page;

class Quiz1Page extends Page
{
    /**
     * Quiz1Page (Максимально полный функционал квиз опроса)
     * @return string
     */
    public function url()
    {
        return '/q/405';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  \Laravel\Dusk\Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser
            ->assertTitle('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Это подзаголовок квиза.')
            ->assertSee('ПРОЙТИ ОПРОС')
            ->assertSee('ИП Калнин Юрий Алексеевич')
            ->assertSee('+7 914 838 33 40')
            ->assertSee('ПН-ВТ 09:00-22:00')
            ->assertSee('БОНУСЫ ПОСЛЕ ПРОХОЖДЕНИЯ')
            ->assertSee('НАЗВАНИЕ БОНУСА 1')
            ->assertSee('НАЗВАНИЕ БОНУСА 2')
            ->assertVisible('@logo')
            ->assertVisible('@iconPhone')
            ->assertSourceHas('https://490119.selcdn.ru/quizgo-uploads/upload/CYUEcMhnz33gafnFG55coYvMArB9FEfOzfQIMTKJ.jpg')
            ->screenshot('testQuiz1Page1')
            ->click('@btnStartQuiz')

            ->assertSee('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Вопрос 1. Тип радио. Нет кастомного. Не обязательный. Без картинки.')
            ->assertSee('Готово:0%')
            ->assertSee('Вариант ответа 1')
            ->assertSee('Вариант ответа 2')
            ->assertSee('Вариант ответа 3')
            ->assertSee('Вариант ответа 4')
            ->assertSee('Вариант ответа 5')
            ->assertSee('Далее')
            ->assertSee('Ирина')
            ->assertSee('Администратор')
            ->assertSee('Подсказка для вопроса 1')
            ->assertSee('Пройдите опрос, чтобы получить')
            ->assertSee('НАЗВАНИЕ БОНУСА 1')
            ->assertSee('НАЗВАНИЕ БОНУСА 2')
            ->assertSourceHas('Сделано в&nbsp;<b>QuizGO</b>')
            ->screenshot('testQuiz1Page2')
            ->click('@btnNextQuestion')

            ->assertSee('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Вопрос 2. Тип радио. Есть кастомный. Обязательный. Есть картинка.')
            ->assertSee('Вариант ответа 11')
            ->assertSee('Вариант ответа 22')
            ->assertSee('Вариант ответа 33')
            ->assertSee('Вариант ответа 44')
            ->assertSee('Вариант ответа 55')
            ->assertSee('Далее')
            ->assertSee('Ирина')
            ->assertSee('Администратор')
            ->assertSee('Подсказка для вопроса 2')
            ->assertSee('Пройдите опрос, чтобы получить')
            ->assertSee('НАЗВАНИЕ БОНУСА 1')
            ->assertSee('НАЗВАНИЕ БОНУСА 2')
            ->assertSourceHas('Сделано в&nbsp;<b>QuizGO</b>')
            ->assertVisible('@customAnswerRadio')
            ->assertVisible('@questionRadioImage')
            ->assertVisible('@btnNextQuestionDisable')
            ->assertSourceHas('<img src="https://490119.selcdn.ru/quizgo-uploads/upload/6db1AXFIZEtaQ0kPeu4CUazPUI0jV4PC7QM6DdOT.jpg"')
            ->type('@customAnswerRadio', 'Кастомный вариант ответа')
            ->pause(1000)
            ->screenshot('testQuiz1Page3')
            ->click('@btnNextQuestion')

            ->assertSee('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Вопрос 3. Тип чекбокс. Нет кастомного. Необязательный. Нет картинка.')
            ->assertSee('Вариант ответа 111')
            ->assertSee('Вариант ответа 222')
            ->assertSee('Вариант ответа 333')
            ->assertSee('Вариант ответа 444')
            ->assertSee('Вариант ответа 555')
            ->assertSee('Далее')
            ->assertSee('Ирина')
            ->assertSee('Администратор')
            ->assertSee('Подсказка для вопроса 3')
            ->assertSee('Пройдите опрос, чтобы получить')
            ->assertSee('НАЗВАНИЕ БОНУСА 1')
            ->assertSee('НАЗВАНИЕ БОНУСА 2')
            ->assertVisible('@customAnswerCheckbox')
            ->assertSourceHas('Сделано в&nbsp;<b>QuizGO</b>')
            ->check('@customAnswerCheckbox')
            ->screenshot('testQuiz1Page4')
            ->click('@btnNextQuestion')

            ->assertSee('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Вопрос 4. Тип чекбокс. Есть кастомный. Обязательный. Есть картинка.')
            ->assertSee('Вариант ответа 1111')
            ->assertSee('Вариант ответа 2222')
            ->assertSee('Вариант ответа 3333')
            ->assertSee('Вариант ответа 4444')
            ->assertSee('Вариант ответа 5555')
            ->assertSee('Далее')
            ->assertSee('Ирина')
            ->assertSee('Администратор')
            ->assertSee('Подсказка для вопроса 4')
            ->assertSee('Пройдите опрос, чтобы получить')
            ->assertSee('НАЗВАНИЕ БОНУСА 1')
            ->assertSee('НАЗВАНИЕ БОНУСА 2')
            ->assertSourceHas('Сделано в&nbsp;<b>QuizGO</b>')
            ->assertVisible('@customAnswerCheckbox')
            ->assertVisible('@questionRadioImage')
            ->assertVisible('@btnNextQuestionDisable')
            ->assertSourceHas('<img src="https://490119.selcdn.ru/quizgo-uploads/upload/6db1AXFIZEtaQ0kPeu4CUazPUI0jV4PC7QM6DdOT.jpg"')
            ->check('@customAnswerCheckbox')
            ->pause(500)
            ->screenshot('testQuiz1Page5')
            ->click('@btnNextQuestion')

            ->assertSee('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Вопрос 5. Тип карточка с картинкой. Не кастомный. Обязательный.')
            ->assertSee('Вариант ответа карточка 1')
            ->assertSee('Вариант ответа карточка 2')
            ->assertSee('Вариант ответа карточка 3')
            ->assertSee('Вариант ответа карточка 4')
            ->assertSee('Вариант ответа карточка 5')
            ->assertSee('Далее')
            ->assertSee('Ирина')
            ->assertSee('Администратор')
            ->assertSee('Подсказка для вопроса 5')
            ->assertSee('Пройдите опрос, чтобы получить')
            ->assertSee('НАЗВАНИЕ БОНУСА 1')
            ->assertSee('НАЗВАНИЕ БОНУСА 2')
            ->assertSourceHas('Сделано в&nbsp;<b>QuizGO</b>')
            ->assertVisible('@btnNextQuestionDisable')
            ->assertVisible('@customAnswerCardRadio')
            ->screenshot('testQuiz1Page6')
            ->click('@customAnswerCardRadio')
            ->pause(500)

            ->assertSee('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Вопрос 6. Тип карточка с картинкой. Множественный. Обязательный.')
            ->assertSee('Вариант ответа карточка 11')
            ->assertSee('Вариант ответа карточка 22')
            ->assertSee('Вариант ответа карточка 33')
            ->assertSee('Вариант ответа карточка 44')
            ->assertSee('Вариант ответа карточка 55')
            ->assertSee('Далее')
            ->assertSee('Ирина')
            ->assertSee('Администратор')
            ->assertSee('Подсказка для вопроса 6')
            ->assertSee('Пройдите опрос, чтобы получить')
            ->assertSee('НАЗВАНИЕ БОНУСА 1')
            ->assertSee('НАЗВАНИЕ БОНУСА 2')
            ->assertSourceHas('Сделано в&nbsp;<b>QuizGO</b>')
            ->assertVisible('@btnNextQuestionDisable')
            ->assertVisible('@customAnswerCardCheckbox')
            ->click('@customAnswerCardCheckbox')
            ->screenshot('testQuiz1Page7')
            ->click('@btnNextQuestion')

            ->assertSee('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Вопрос 7. Тип текст. Необязательный. Однострочный.')
            ->assertSee('Далее')
            ->assertSee('Ирина')
            ->assertSee('Администратор')
            ->assertSee('Подсказка для вопроса 7')
            ->assertSee('Пройдите опрос, чтобы получить')
            ->assertSee('НАЗВАНИЕ БОНУСА 1')
            ->assertSee('НАЗВАНИЕ БОНУСА 2')
            ->assertSourceHas('placeholder="Пример ответа на вопрос 7"')
            ->assertSourceHas('Сделано в&nbsp;<b>QuizGO</b>')
            ->assertVisible('@btnNextQuestion')
            ->assertVisible('@customAnswerInput')
            ->type('@customAnswerInput', 'Ответ на вопрос 7')
            ->pause(500)
            ->screenshot('testQuiz1Page8')
            ->click('@btnNextQuestion')

            ->assertSee('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Вопрос 8. Тип текст. Обязательный. Многострочный.')
            ->assertSee('Далее')
            ->assertSee('Ирина')
            ->assertSee('Администратор')
            ->assertSee('Подсказка для вопроса 8')
            ->assertSee('Пройдите опрос, чтобы получить')
            ->assertSee('НАЗВАНИЕ БОНУСА 1')
            ->assertSee('НАЗВАНИЕ БОНУСА 2')
            ->assertSourceHas('placeholder="Пример ответа на вопрос 8"')
            ->assertSourceHas('Сделано в&nbsp;<b>QuizGO</b>')
            ->assertVisible('@btnNextQuestionDisable')
            ->assertVisible('@customAnswerInputTextarea')
            ->type('@customAnswerInputTextarea', 'Ответ на вопрос 8')
            ->pause(500)
            ->screenshot('testQuiz1Page9')
            ->click('@btnNextQuestion')

            ->assertSee('Узнайте на какую из фитнес программ вам лучше записаться?')
            ->assertSee('Вопрос 9. Тип выпадающий список. Обязательный.')
            ->assertSee('Далее')
            ->assertSee('Ирина')
            ->assertSee('Администратор')
            ->assertSee('Подсказка для вопроса 9')
            ->assertSee('Пройдите опрос, чтобы получить')
            ->assertSee('НАЗВАНИЕ БОНУСА 1')
            ->assertSee('НАЗВАНИЕ БОНУСА 2')
            ->assertSee('Пример ответа на вопрос 9')
            ->assertSourceHas('Сделано в&nbsp;<b>QuizGO</b>')
            ->assertVisible('@btnNextQuestionDisable')
            ->assertVisible('@customAnswerSelect')
            ->select('@customAnswerSelect', 'Вариант списка 5')
            ->pause(500)
            ->screenshot('testQuiz1Page10')
            ->click('@btnNextQuestion')
        ;
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@logo' => '.b-first-slide__image-company',
            '@iconPhone' => '.svg-phone',
            '@btnStartQuiz' => '.b-first-slide .b-quiz-btn',
            '@btnNextQuestion' => '.b-quiz-question-wrapper .b-quiz-question .b-quiz-btn',
            '@btnNextQuestionDisable' => '.b-quiz-question-wrapper .b-quiz-question .b-quiz-btn.b-btn-disable',
            '@customAnswerSelect' => '.b-quiz-question-wrapper .b-quiz-question .b-answer-select select',
            '@customAnswerInput' => '.b-quiz-question-wrapper .b-quiz-question .b-answer-input [type="text"]',
            '@customAnswerInputTextarea' => '.b-quiz-question-wrapper .b-quiz-question .b-answer-input textarea',
            '@customAnswerRadio' => '.b-quiz-question-wrapper .b-quiz-question .b-answer-radio__custom [type="text"]',
            '@customAnswerCheckbox' => '.b-quiz-question-wrapper .b-quiz-question .b-answer-checkbox',
            '@customAnswerCardRadio' => '.b-quiz-question-wrapper .b-quiz-question .b-answer-checkbox-image',
            '@customAnswerCardCheckbox' => '.b-quiz-question-wrapper .b-quiz-question .b-answer-checkbox-image.b-answer-checkbox-image--checkbox',
            '@questionRadioImage' => '.b-quiz-question-wrapper .b-quiz-question .b-answer-radio-group__img-wrp img',
        ];
    }
}
