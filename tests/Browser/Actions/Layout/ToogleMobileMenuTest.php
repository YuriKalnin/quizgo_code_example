<?php

namespace Tests\Browser\Actions\Layout;

use DevtoolsService;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

/**
 * Кликаем меню на мобильной версии
 */
class ToogleMobileMenuTest extends DuskTestCase
{
    public function testMakeCouponOk()
    {
        $this->browse(function (Browser $browser) {

            $fakeData = DevtoolsService::tests()
                            ->fakeData()
                            ->createCompany()
                            ->createCoupon();

            $browser
                ->loginAs($fakeData->user)
                ->resize(393, 851)
                ->visit("/")
                ->assertDontSee("Квизы")
                ->assertDontSee("Заявки")
                ->click(".b-navbar__brand--mobile")
                ->assertSee("Квизы")
                ->assertSee("Заявки")
                ->click(".b-navbar__brand--mobile")
                ->assertDontSee("Квизы")
                ->assertDontSee("Заявки")
            ;

            $fakeData->delete();
        });
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}
