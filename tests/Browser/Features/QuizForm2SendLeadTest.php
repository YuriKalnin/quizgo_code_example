<?php

namespace Tests\Browser\Features;

use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use DevtoolsService;
use App\Models\Lead;

class QuizForm2SendLeadTest extends DuskTestCase
{
    public $fakeData;
    public $quizJson;
    public Browser $browser;
    public $time;

    /**
     * Оставляем лид через форму
     * Все поля включены и обязательны
     * Политика конфиденциальности нужно принять
     * @return void
     * @throws \Throwable
     */
    public function testLeadContactForm()
    {
        $this->time = time();

        $this->browse(function (Browser $browser) {

            $this->browser = $browser;

            $this->fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createQuiz();

            $this->quizJson()
                ->quizJsonSetup()
                ->updateQuiz()
                ->visit()
            ;

            $this
                ->browser
                ->type('.b-form__input[name="name"]', 'Тестовый лид')
                ->type('.b-form__input[name="email"]', 'test@mail.ru')
                ->type('.b-form__input[name="phone"]', '9189714096')
                ->click('.b-form__confirmed-checkbox')
                ->click('.b-form__btn')
                ->assertSee('Отправка')
                ->pause(3000)
                ->assertSee('Спасибо за обращение!')
                ->assertSee('С вами свяжется менеджер и расскажет о бонусах для Вас!')
                ->assertSourceHas('https://mc.yandex.ru/metrika')
                ->screenshot('QuizForm2SendLead__LeadContactForm')
            ;
        });

        $lead = Lead::where('source', $this->time)->get()->first();

        $this->assertTrue($lead->name === 'Тестовый лид');
        $this->assertTrue($lead->email === 'test@mail.ru');
        $this->assertTrue($lead->phone === '+7 (918) 971-40-96');
        $this->assertTrue($lead->source == $this->time);
        $this->assertTrue($lead->medium === 'cpc');
        $this->assertTrue($lead->campaign === 'utm_campaign__test');
        $this->assertTrue($lead->content === 'utm_content__test');
        $this->assertTrue($lead->term === 'utm_term__test');
        $this->assertTrue($lead->sale_value == 1000);
        $this->assertTrue($lead->yclid === 'yclidtest');
        $this->assertTrue($lead->gclid === 'gclidtest');
        $this->assertTrue($lead->fbclid === 'fbclidtest');

        $lead->delete();
        $this->fakeData->delete();
    }

    /**
     * Оставляем лид и чекаем что произошел
     * редирект по указанной ссылке в настройках
     * @return void
     * @throws \Throwable
     */
    public function testLeadContactFormRedirectAfter()
    {
        $this->time = time();

        $this->browse(function (Browser $browser) {

            $this->browser = $browser;

            $this->fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createQuiz();

            $this->quizJson()
                ->quizJsonSetup()
            ;

            $this->quizJson->contact_form->redirect->enable = true;
            $this->quizJson->contact_form->redirect->url = "https://quizgo.ru?test={$this->time}";

            $this
                ->updateQuiz()
                ->visit()
            ;

            $this
                ->browser
                ->type('.b-form__input[name="name"]', 'Тестовый лид')
                ->type('.b-form__input[name="email"]', 'test@mail.ru')
                ->type('.b-form__input[name="phone"]', '9189714096')
                ->click('.b-form__confirmed-checkbox')
                ->click('.b-form__btn')
                ->assertSee('Отправка')
                ->pause(3000)
                ->assertQueryStringHas('test', $this->time)
                ->screenshot('QuizForm2SendLead__LeadContactFormRedirectAfter')
            ;
        });

        $this->fakeData->delete();
    }

    /**
     * Оставляем лид и результаты после формы контактов
     * @return void
     * @throws \Throwable
     */
    public function testLeadContactFormResultAfter()
    {
        $this->time = time();

        $this->browse(function (Browser $browser) {

            $this->browser = $browser;

            $this->fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createQuiz();

            $this->quizJsonWithResults()
                ->quizJsonSetup()
                ->updateQuiz()
                ->visit()
            ;

            $this
                ->browser
                ->type('.b-form__input[name="name"]', 'Тестовый лид')
                ->type('.b-form__input[name="email"]', 'test@mail.ru')
                ->type('.b-form__input[name="phone"]', '9189714096')
                ->click('.b-form__confirmed-checkbox')
                ->click('.b-form__btn')
                ->assertSee('Отправка')
                ->pause(3000)
                ->assertSee('Варианты кухонь которые вам подойдут')
                ->assertSee('Мы внимательно изучили ваши ответы и подготовили несколько вариантов')
                ->assertSee('Фабио')
                ->assertSee('Федерика')
                ->assertSee('Инклината')
                ->screenshot('QuizForm2SendLead__LeadContactFormResultAfter')
            ;
        });

        $this->fakeData->delete();
    }

    /**
     * Оставляем лид через мессенджер
     * Политика конфиденциальности нужно принять
     * @return void
     * @throws \Throwable
     */
    public function testLeadMessengerTg()
    {
        $this->leadMessenger('Телеграм', ['.b-form__input[name="phone_messenger"]', '9189714096', '+7 (918) 971-40-96'], 'Telegram');
    }

    public function testLeadMessengerVb()
    {
        $this->leadMessenger('Viber', ['.b-form__input[name="phone_messenger"]', '9189714096', '+7 (918) 971-40-96'], 'Viber');
    }

    public function testLeadMessengerWp()
    {
        $this->leadMessenger('WhatsApp', ['.b-form__input[name="phone_messenger"]', '9189714096', '+7 (918) 971-40-96'], 'WhatsApp');
    }

    public function testLeadMessengerVk()
    {
        $this->leadMessenger('Вконтакте', ['.b-form__input[name="messenger_value_link"]', 'https://vk.com/user_test', 'https://vk.com/user_test'], 'ВКонтакте');
    }

    public function testLeadMessengerFb()
    {
        $this->leadMessenger('Мессенджер', ['.b-form__input[name="messenger_value_link"]', 'https://m.com/user_test', 'https://m.com/user_test'], 'Messenger');
    }

    public function leadMessenger($messengerRu, $typeContact, $messengerEn) {

        $this->browse(function (Browser $browser) use ($messengerRu, $typeContact, $messengerEn) {

            $this->time = time();
            $this->browser = $browser;

            $this->fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createQuiz()
            ;

            $this->quizJson()
                ->quizJsonSetup()
                ->updateQuiz()
                ->visit()
            ;

            $this
                ->browser
                ->clickLink('Использовать мессенджер')
                ->clickLink($messengerRu)
                ->type($typeContact[0], $typeContact[1])
                ->click('.b-form__confirmed-checkbox')
                ->click('.b-form__btn')
                ->assertSee('Отправка')
                ->pause(3000)
                ->assertSee('Спасибо за обращение!')
                ->assertSee('С вами свяжется менеджер и расскажет о бонусах для Вас!')
                ->screenshot("QuizForm2SendLead__LeadMessenger{$messengerEn}")
            ;

            $lead = Lead::where('source', $this->time)->get()->first();

            $this->assertTrue($lead->source == $this->time);
            $this->assertTrue($lead->medium === 'cpc');
            $this->assertTrue($lead->campaign === 'utm_campaign__test');
            $this->assertTrue($lead->content === 'utm_content__test');
            $this->assertTrue($lead->term === 'utm_term__test');
            $this->assertTrue($lead->sale_value == 1000);
            $this->assertTrue($lead->yclid === 'yclidtest');
            $this->assertTrue($lead->gclid === 'gclidtest');
            $this->assertTrue($lead->fbclid === 'fbclidtest');
            $this->assertTrue($lead->messenger === $messengerEn);
            $this->assertTrue($lead->contact_messenger === $typeContact[2]);

            $lead->delete();

            $this->fakeData->delete();
        });
    }

    public function visit() {

        $url = "/q/{$this->fakeData->quiz->id}?";
        $url .= "utm_source={$this->time}&utm_medium=cpc&utm_campaign=utm_campaign__test&utm_content=utm_content__test&utm_term=utm_term__test";
        $url .= "&yclid=yclidtest&gclid=gclidtest&fbclid=fbclidtest";

        $this
            ->browser
            ->visit($url);
        return $this;
    }

    public function quizJsonSetup() {
        $this->quizJson->first_page->enable = false;
        $this->quizJson->updates->new_quiz_form = true;

        $this->quizJson->contact_form->name->enabled = true;
        $this->quizJson->contact_form->email->enabled = true;
        $this->quizJson->contact_form->phone->enabled = true;

        $this->quizJson->contact_form->policy->checked = false;

        $this->quizJson->messengers->enable = true;
        $this->quizJson->messengers->list->telegram->enable = true;
        $this->quizJson->messengers->list->messenger->enable = true;
        $this->quizJson->messengers->list->viber->enable = true;
        $this->quizJson->messengers->list->vk->enable = true;
        $this->quizJson->messengers->list->whatsapp->enable = true;

        foreach ($this->quizJson->questions as $question) {
            $question->questionView = 0;
        }

        return $this;
    }

    public function updateQuiz() {
        $this->fakeData->quiz->quiz_json = json_encode($this->quizJson);
        $this->fakeData->quiz->save();
        \CacheService::clearCacheQuiz($this->fakeData->quiz->id);
        return $this;
    }

    public function quizJsonWithResults() {
        $this->quizJson = json_decode(file_get_contents(__DIR__. "/QuizForm2Test/QuizJsonWithResults.json"));
        return $this;
    }

    public function quizJson() {
        $this->quizJson = json_decode(file_get_contents(__DIR__. "/QuizForm2Test/QuizJson.json"));
        return $this;
    }
}
