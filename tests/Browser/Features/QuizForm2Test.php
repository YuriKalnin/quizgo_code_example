<?php

namespace Tests\Browser\Features;

use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use DevtoolsService;

class QuizForm2Test extends DuskTestCase
{
    public $fakeData;
    public $quizJson;
    public $browser;

    /**
     * Проверка старой формы квиза
     * @return void
     * @throws \Throwable
     */
    public function testOldVersion()
    {
        $this->browse(function (Browser $browser) {

            $this->browser = $browser;

            $this->fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createQuiz();

            $this->quizJson()
                ->setNewQuizForm(false)
                ->updateQuiz()
                ->visit()
                ->goToQuestions()
                ->giveAnswers()
                ->assertTexts();

            $this->browser->assertVisible('.b-screen.b-quiz-form-wrapper')
                    ->assertMissing('.b-quizgo-widget-app--form')
                    ->screenshot('QuizForm2Test__OldVersion')
            ;

            $this->fakeData->delete();
        });
    }

    /**
     * Проверка на то что видим
     * новую форму
     * @return void
     * @throws \Throwable
     */
    public function testApplyUpdates()
    {
        $this->browse(function (Browser $browser) {

            $this->browser = $browser;

            $this->fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createQuiz();

            $this->quizJson()
                ->setNewQuizForm(true)
                ->updateQuiz()
                ->visit()
                ->goToQuestions()
                ->giveAnswers()
                ->assertTexts();

            $browser->assertMissing('.b-screen.b-quiz-form-wrapper')
                ->assertVisible('.b-quizgo-widget-app--form')
                ->screenshot('QuizForm2Test__ApplyUpdates')
            ;

            $this->fakeData->delete();
        });
    }

    /**
     * Если стартовая страница выключена,
     * должны включится сразу вопросы
     * @return void
     * @throws \Throwable
     */
    public function testStartPageOff()
    {
        $this->browse(function (Browser $browser) {

            $this->browser = $browser;

            $this->fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createQuiz();

            $this->quizJson()
                ->disableStartPage()
                ->updateQuiz()
                ->visit()
                ->giveAnswers()
                ->assertTexts();

            $this->fakeData->delete();
        });
    }

    /**
     * Если выключены вопросы,
     * должна включится сразу форма
     * @return void
     * @throws \Throwable
     */
    public function testQuestionOff()
    {
        $this->browse(function (Browser $browser) {

            $this->browser = $browser;
            $this->fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createQuiz();

            $this->quizJson()
                ->disableQuestions()
                ->updateQuiz()
                ->visit()
                ->goToQuestions()
                ->assertTexts();

            $browser->screenshot('QuizForm2Test__testQuestionOff');

            $this->fakeData->delete();
        });
    }

    /**
     * Нет стартовой и вопросов
     * @return void
     * @throws \Throwable
     */
    public function testStartQuestionOff()
    {
        $this->browse(function (Browser $browser) {

            $this->browser = $browser;
            $this->fakeData = DevtoolsService::tests()
                ->fakeData()
                ->createCompany()
                ->createQuiz();

            $this->quizJson()
                ->disableStartPage()
                ->disableQuestions()
                ->updateQuiz()
                ->visit()
                ->assertTexts();

            $browser->screenshot('QuizForm2Test__testStartQuestionOff');

            $this->fakeData->delete();
        });
    }

    /**
     * Отсутствие заголовка текста формы
     * @return void
     * @throws \Throwable
     */
    public function testWithoutTextTitle()
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->quizJson->last_page->title = '';
            $this->updateQuiz()->visit();
            $this->browser
                    ->assertDontSee('Заполните форму и получите персональную скидку')
                    ->assertMissing('.b-form__left h4')
                    ->screenshot('QuizForm2Test__WithoutElements_1');
        });

        $this->fakeData->delete();
    }

    /**
     * Отсутствие текста формы
     * @return void
     * @throws \Throwable
     */
    public function testWithoutTextForm()
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->quizJson->contact_form->text = '';
            $this->updateQuiz()->visit();
            $this->browser->assertDontSee('Мы подготовили эксклюзивные бонусы для Вас, оставьте заявку и получите дополнительную выгоду')
                    ->assertMissing('.b-form__text')
                    ->screenshot('QuizForm2Test__WithoutElements_2');
        });

        $this->fakeData->delete();
    }

    /**
     * Отсутствие текста формы и заголовка формы
     * @return void
     * @throws \Throwable
     */
    public function testWithoutTextAndTitleForm()
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->quizJson->contact_form->text = '';
            $this->quizJson->last_page->title = '';
            $this->updateQuiz()->visit();
            $this->browser
                ->assertDontSee('Мы подготовили эксклюзивные бонусы для Вас, оставьте заявку и получите дополнительную выгоду')
                ->assertMissing('.b-form__text')
                ->assertDontSee('Заполните форму и получите персональную скидку')
                ->assertSeeIn('h4.b-form__title-bonuses', 'Оставьте контакты, чтобы получить:')
                ->screenshot('QuizForm2Test__WithoutElements_3');
        });

        $this->fakeData->delete();
    }

    /**
     * Отсутствие текста формы, заголовка формы и бонусов
     * @return void
     * @throws \Throwable
     */
    public function testCentered()
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->quizJson->contact_form->text = '';
            $this->quizJson->last_page->title = '';
            $this->quizJson->bonus[0]->enable = false;
            $this->quizJson->bonus[1]->enable = false;
            $this->updateQuiz()->visit();
            $this->browser
                ->assertDontSee('ВЫГОДА ДО 1,1 МЛН ₽ В АПРЕЛЕ!')
                ->assertDontSee('ДАРИМ 60 000₽ НА ПОКУПКУ МЕБЕЛИ!')
                ->assertDontSee('Оставьте контакты, чтобы получить:')
                ->assertSourceHas('b-form--centered')
                ->screenshot('QuizForm2Test__Centered');
        });

        $this->fakeData->delete();
    }

    /**
     * Все поля формы выключены и мессенджеры тоже
     * @return void
     * @throws \Throwable
     */
    public function testFormFieldsOff()
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->quizJson->contact_form->name->enabled = false;
            $this->quizJson->contact_form->email->enabled = false;
            $this->quizJson->contact_form->phone->enabled = false;
            $this->quizJson->messengers->enable = false;
            $this->updateQuiz()->visit();
            $this->browser
                ->assertDontSee('Введите номер телефона')
                ->screenshot('QuizForm2Test__FormFieldsOff');
        });

        $this->fakeData->delete();
    }

    /**
     * Все поля формы выключены,
     * но мессенджеры включены
     * @return void
     * @throws \Throwable
     */
    public function testFormFieldsOffMessengersOn()
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->quizJson->contact_form->name->enabled = false;
            $this->quizJson->contact_form->email->enabled = false;
            $this->quizJson->contact_form->phone->enabled = false;
            $this->quizJson->messengers->enable = true;
            $this->updateQuiz()->visit();
            $this->browser
                ->assertSee('Куда присылать результат')
                ->assertSee('Телеграм')
                ->assertSee('Viber')
                ->assertSee('Вконтакте')
                ->assertSee('WhatsApp')
                ->assertSee('Мессенджер')
                ->screenshot('QuizForm2Test__FormFieldsOffMessengersOn');
        });

        $this->fakeData->delete();
    }

    /**
     * Все поля формы включены,
     * а мессенджеры выключены
     * @return void
     * @throws \Throwable
     */
    public function testFormFieldsOnMessengersOnf()
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->quizJson->contact_form->title = 'Заголовок формы';
            $this->quizJson->contact_form->name->enabled = true;
            $this->quizJson->contact_form->name->hint = "Подсказка для name";
            $this->quizJson->contact_form->email->enabled = true;
            $this->quizJson->contact_form->email->hint = "Подсказка для email";
            $this->quizJson->contact_form->phone->enabled = true;
            $this->quizJson->contact_form->phone->hint = "Подсказка для phone";
            $this->quizJson->last_page->button = "Текст на кнопке";
            $this->quizJson->messengers->enable = false;

            $this->updateQuiz()->visit();
            $this->browser
                ->assertSee('Заголовок формы')
                ->assertSee('Подсказка для name')
                ->assertSee('Подсказка для email')
                ->assertSee('Подсказка для phone')
                ->assertSee('Текст на кнопке')
                ->assertSourceHas('placeholder="Имя"')
                ->assertSourceHas('placeholder="example@mail.com"')
                ->assertSourceHas('placeholder="(000) 000-00-00"')
                ->assertDontSee('Использовать мессенджер')
                ->screenshot('QuizForm2Test__FormFieldsOnMessengersOnf');
        });

        $this->fakeData->delete();
    }

    /**
     * Все поля формы включены,
     * мессенджеры тоже включены,
     * а подсказок к полям нет
     * @return void
     * @throws \Throwable
     */
    public function testFormFieldsOnMessengersOn()
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->quizJson->contact_form->title = 'Заголовок формы';
            $this->quizJson->contact_form->name->enabled = true;
            $this->quizJson->contact_form->name->hint = "";
            $this->quizJson->contact_form->email->enabled = true;
            $this->quizJson->contact_form->email->hint = "";
            $this->quizJson->contact_form->phone->enabled = true;
            $this->quizJson->contact_form->phone->hint = "";
            $this->quizJson->last_page->button = "Текст на кнопке";
            $this->quizJson->messengers->enable = true;

            $this->updateQuiz()->visit();
            $this->browser
                ->assertSee('Заголовок формы')
                ->assertSee('Текст на кнопке')
                ->assertSee('Использовать мессенджер')
                ->assertDontSee('Подсказка для name')
                ->assertDontSee('Подсказка для email')
                ->assertDontSee('Подсказка для phone')
                ->assertSourceHas('placeholder="Имя"')
                ->assertSourceHas('placeholder="example@mail.com"')
                ->assertSourceHas('placeholder="(000) 000-00-00"')
                ->screenshot('QuizForm2Test__FormFieldsOnMessengersOn');
        });

        $this->fakeData->delete();
    }

    /**
     * Мессенджеры включены,
     * но недоступны
     * @return void
     * @throws \Throwable
     */
    public function testMessengersOnDisable()
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->quizJson->messengers->enable = true;
            $this->quizJson->messengers->list->telegram->enable = false;
            $this->quizJson->messengers->list->messenger->enable = false;
            $this->quizJson->messengers->list->viber->enable = false;
            $this->quizJson->messengers->list->vk->enable = false;
            $this->quizJson->messengers->list->whatsapp->enable = false;

            $this->updateQuiz()->visit();
            $this->browser
                ->assertSee('Использовать мессенджер')
                ->clickLink('Использовать мессенджер')
                ->assertSee('Куда присылать результат')
                ->assertDontSee('Телеграм')
                ->assertDontSee('Viber')
                ->assertDontSee('Вконтакте')
                ->assertDontSee('WhatsApp')
                ->assertDontSee('Мессенджер')
                ->screenshot('QuizForm2Test__MessengersOnDisable');
        });

        $this->fakeData->delete();
    }

    /**
     * Мессенджеры включены и доступны
     * проверка полей контакта мессенджера
     * @return void
     * @throws \Throwable
     */
    public function testCheckMessengersContactField()
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) {
            $this->browser = $browser;
            $this->quizJson->messengers->enable = true;
            $this->quizJson->messengers->list->telegram->enable = true;
            $this->quizJson->messengers->list->messenger->enable = true;
            $this->quizJson->messengers->list->viber->enable = true;
            $this->quizJson->messengers->list->vk->enable = true;
            $this->quizJson->messengers->list->whatsapp->enable = true;

            $this->updateQuiz()->visit();
            $this->browser
                ->assertSee('Использовать мессенджер')
                ->clickLink('Использовать мессенджер')

                ->assertSee('Куда присылать результат')
                ->assertSee('Телеграм')
                ->assertSee('Viber')
                ->assertSee('Вконтакте')
                ->assertSee('WhatsApp')
                ->assertSee('Мессенджер')
                ->clickLink('Телеграм')

                ->assertSee('Отправить в Telegram')
                ->assertSee('Введите номер Telegram')
                ->assertSourceHas('placeholder="(000) 000-00-00"')
                ->assertSee('Готово')
                ->screenshot('QuizForm2Test__CheckMessengersContactField_TG')
                ->clickLink('Выбрать другой мессенджер')

                ->clickLink('Viber')
                ->assertSee('Отправить в Viber')
                ->assertSee('Введите номер Viber')
                ->assertSourceHas('placeholder="(000) 000-00-00"')
                ->assertSee('Готово')
                ->screenshot('QuizForm2Test__CheckMessengersContactField_VB')
                ->clickLink('Выбрать другой мессенджер')

                ->clickLink('Вконтакте')
                ->assertSee('Отправить в ВКонтакте')
                ->assertSee('Укажите ссылку на Ваш аккаунт VK')
                ->assertSourceHas('placeholder="https://vk.com/id566311562"')
                ->assertSee('Готово')
                ->screenshot('QuizForm2Test__CheckMessengersContactField_VK')
                ->clickLink('Выбрать другой мессенджер')

                ->clickLink('WhatsApp')
                ->assertSee('Отправить в WhatsApp')
                ->assertSee('Введите номер Whatsapp')
                ->assertSourceHas('placeholder="(000) 000-00-00"')
                ->assertSee('Готово')
                ->screenshot('QuizForm2Test__CheckMessengersContactField_WP')
                ->clickLink('Выбрать другой мессенджер')

                ->clickLink('Мессенджер')
                ->assertSee('Отправить в Facebook')
                ->assertSee('Скопируйте ссылку messenger')
                ->assertSourceHas('https://m.me/userlogin')
                ->assertSee('Готово')
                ->screenshot('QuizForm2Test__CheckMessengersContactField_FB')
                ->clickLink('Выбрать другой мессенджер')
                ->screenshot('QuizForm2Test__CheckMessengersContactField')

                ->clickLink('Вернуться к вводу контактов')
                ->assertSee('Оставьте ваши данные')
                ->assertSee('Введите ваше имя')
                ->screenshot('QuizForm2Test__CheckMessengersContactField_FORM');
        });

        $this->fakeData->delete();
    }

    /**
     * Проверка обязательности полей формы
     * @return void
     * @throws \Throwable
     */
    public function testRequiredOnFormFields()
    {
        $this->requiredOffFormFieldsTest(true, function () {
            $this->browser
                ->assertVisible('.b-form__input[name=name][required]')
                ->assertVisible('.b-form__input[name=phone][required]')
                ->assertVisible('.b-form__input[name=email][required]');
        });
    }

    /**
     * Проверка не обязательности полей формы
     * @return void
     * @throws \Throwable
     */
    public function testRequiredOffFormFields()
    {
        $this->requiredOffFormFieldsTest(false, function () {
            $this->browser
                ->assertVisible('.b-form__input[name=name]')
                ->assertVisible('.b-form__input[name=phone]')
                ->assertVisible('.b-form__input[name=email]')
                ->assertMissing('.b-form__input[name=name][required]')
                ->assertMissing('.b-form__input[name=phone][required]')
                ->assertMissing('.b-form__input[name=email][required]');
        });
    }

    public function requiredOffFormFieldsTest($required, $assertsCallback)
    {
        $this->fakeData = DevtoolsService::tests()
            ->fakeData()
            ->createCompany()
            ->createQuiz();

        $this->quizJson()
            ->disableStartPage()
            ->disableQuestions()
        ;

        $this->browse(function (Browser $browser) use ($required, $assertsCallback) {
            $this->browser = $browser;

            $this->quizJson->contact_form->name->enabled = true;
            $this->quizJson->contact_form->email->enabled = true;
            $this->quizJson->contact_form->phone->enabled = true;

            $this->quizJson->contact_form->name->required = $required;
            $this->quizJson->contact_form->email->required = $required;
            $this->quizJson->contact_form->phone->required = $required;

            $this->updateQuiz()->visit();

            call_user_func($assertsCallback);
        });

        $this->fakeData->delete();
    }

    public function assertTexts() {
        $this->browser
            ->assertSee('Заполните форму и получите персональную скидку')
            ->assertSee('Для Вас сюрприз!')
            ->assertSee('Мы подготовили эксклюзивные бонусы для Вас, оставьте заявку и получите дополнительную выгоду')
            ->assertSee('ВЫГОДА ДО 1,1 МЛН ₽ В АПРЕЛЕ!')
            ->assertSee('ДАРИМ 60 000₽ НА ПОКУПКУ МЕБЕЛИ!')
            ->assertSee('Введите ваше имя')
            ->assertSee('Введите номер телефона')
            ->assertSee('Получить скидку');
        return $this;
    }

    public function visit() {
        $this->browser->loginAs($this->fakeData->user)
            ->visit("/q/{$this->fakeData->quiz->id}");
        return $this;
    }

    public function giveAnswers() {
        $this->browser
            ->click('.b-answer-radio')->pause(200)
            ->click('.b-answer-checkbox-image')->pause(200)
            ->click('.b-answer-radio')->pause(200);
        return $this;
    }

    public function goToQuestions() {
        $this->browser->clickLink('ВЫБРАТЬ СВОЙ ДОМ');
        return $this;
    }

    public function disableStartPage() {
        $this->quizJson->first_page->enable = false;
        return $this;
    }

    public function disableQuestions() {

        foreach ($this->quizJson->questions as $question) {
            $question->questionView = 0;
        }

        return $this;
    }

    public function setNewQuizForm($value) {
        $this->quizJson->updates->new_quiz_form = $value;
        return $this;
    }

    public function updateQuiz() {
        $this->fakeData->quiz->quiz_json = json_encode($this->quizJson);
        $this->fakeData->quiz->save();
        \CacheService::clearCacheQuiz($this->fakeData->quiz->id);
        return $this;
    }

    public function quizJson() {
        $this->quizJson = json_decode(file_get_contents(__DIR__. "/QuizForm2Test/QuizJson.json"));
        return $this;
    }
}
