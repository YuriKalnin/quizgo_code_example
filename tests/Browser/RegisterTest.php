<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Tests\Browser\Pages\Dashboard\RegisterPage;

use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

use App\Models\User;

class RegisterTest extends DuskTestCase
{
    public function testRegisterClient1()
    {
        $this->browse(function ($browser){
            $browser->visit(new RegisterPage());
            $browser->screenshot('testRegisterClient1');
        });
    }

    public function tearDown(): void
    {
        $userClient = User::where('email', '=', 'testClient1@mail.ru')->get()->first();

        if ($userClient) {
            $userClient->delete();
        }

        parent::tearDown();
    }

    /*public function testRegisterPartner1()
    {

        $this->browse(function ($browser){
            $browser->visit('/register/partner')
                ->type('name', 'testPartner1')
                ->type('email', 'testPartner1@mail.ru')
                ->type('password', 'password')
                ->type('password_confirmation', 'password')
                ->type('contact_phone', '+7 (000) 000-00-00')
                ->type('name_company', 'TestOrganizationName1')
                ->press('Создайте аккаунт')
                ->assertSee('Карточка партнера')
                ->click('#navbarDropdownMenuLink')
                ->click('.dropdown-menu-right.show a')
            ;

            if($this->screen) $browser->screenshot('testRegisterPartner1');
        });

    }

    public function testLogin()
    {
        $this->browse(function ($client, $partner){
            $client
                ->visit('/login')
                ->type('email', 'testClient1@mail.ru')
                ->type('password', 'password')
                ->press('Войти')
                ->visit('/dashboard/?change_company=y')
                ->assertSee('Создать проект');

            if($this->screen) $client->screenshot('testLoginClient');

            $partner
                ->visit('/login')
                ->type('email', 'testPartner1@mail.ru')
                ->type('password', 'password')
                ->press('Войти')
                ->visit('/dashboard/?change_company=y')
                ->assertSee('Создать проект');

            if($this->screen) $client->screenshot('testLoginPartner');
        });
    }

    public function testCompanyUpdate()
    {
        $this->browse(function ($client){
            $client
                ->assertSee('Настройки компании')
                ->clickLink('Настройки компании')
                ->type('name', 'CompanyClient1')
                ->type('site_url', 'http://ya.ru')
                ->type('contact_name', 'Client1')
                ->type('contact_phone', '+7 (111) 111-11-11')
                ->press('Сохранить')
                ->assertSee('Данные компании успешно обновлены');

            if($this->screen) $client->screenshot('testCompanyUpdate');
        });
    }

    public function testCompanyCreateAccess()
    {
        $this->browse(function ($client) {

            $client
                ->clickLink('Права доступа')
                ->type('email', 'testPartner1@mail.ru')
                ->press('Добавить')
                ->assertSee('testPartner1@mail.ru');

            if($this->screen) $client->screenshot('testCompanyCreateAccess');

        });
    }

    public function testCompanyUpdateAccess()
    {
        $this->browse(function ($client){
            $client
                ->clickLink('Настройки компании')
                ->clickLink('Права доступа')
                ->click('a[title="Редактировать"]')
                ->select('role', 'developer')
                ->press('Сохранить')
                ->assertSee('developer');

            if($this->screen) $client->screenshot('testCompanyUpdateAccess');
        });
    }

    public function testCreateProject()
    {


        $this->browse(function ($client){
            $client
                ->clickLink('Квизы')
                ->click('button[data-target="#newProject"]')
                ->pause(1300)
                ->type('name', 'Client1Project1')
                ->click('#newProject [type=submit]')
                ->assertSee('Client1Project1')
            ;

            if($this->screen) $client->screenshot('testCreateProject');
        });
    }

    public function testUpdateProject()
    {


        $this->browse(function ($client){
            $client
                ->clickLink('Квизы')
                ->click('.card-header:nth-child(1) a.editProject')
                ->pause(1300)
                ->value('#editProject input[name=name]', 'Client1Project1 UPDATED')
                ->click('#editProject [type=submit]')
                ->assertSee('Client1Project1 UPDATED')
            ;

            if($this->screen) $client->screenshot('testUpdateProject');
        });
    }

    public function testDeleteProject()
    {


        $this->browse(function ($client){
            $client
                ->clickLink('Квизы')
                ->click('.card-header:nth-child(1) a.deleteProject')
                ->pause(1300)
                ->click('#deleteProject [type=submit]')
                ->assertSee('Client1Project1')
            ;

            if($this->screen) $client->screenshot('testDeleteProject');
        });
    }*/
}
