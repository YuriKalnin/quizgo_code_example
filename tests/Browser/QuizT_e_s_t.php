<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Tests\Browser\Pages\Quiz\Quiz1Page;

class QuizTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testQuiz1Page()
    {
        $this->browse(function ($browser){
            $browser->visit(new Quiz1Page());
        });
    }

}
