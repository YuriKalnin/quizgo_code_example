<?php

namespace Tests\Browser\Components\Dashboard;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class Navbar extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '.b-navbar';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser
            ->waitFor($this->selector())
            ->assertVisible('@linkBrand')
            ->assertVisible('@selectCompany')
            ->assertVisible('@btnLogin')
            ->assertVisible($this->selector())
            ->assertSee('компания')
            ->assertSee('Моя компания')
            ->assertSee('testClient1@mail.ru')
            ->click('@btnLogin')
            ->clickLink('Переключить на Русский')
            ->pause(3000)
        ;
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@linkBrand' => '.b-navbar__brand',
            '@selectCompany' => '.b-navbar__company',
            '@btnLogin' => '.b-user-email-login',
        ];
    }
}
