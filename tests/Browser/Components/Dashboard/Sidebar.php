<?php

namespace Tests\Browser\Components\Dashboard;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Component as BaseComponent;

class Sidebar extends BaseComponent
{
    /**
     * Get the root selector for the component.
     *
     * @return string
     */
    public function selector()
    {
        return '.b-sidebar';
    }

    /**
     * Assert that the browser page contains the component.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser
            ->waitFor($this->selector())
            ->assertVisible($this->selector())
            ->assertSee('Квизы')
            ->assertSee('Заявки')
            ->assertSee('Аналитика')
            ->assertSee('Telegram Bot')
            ->assertSee('Тариф')
            ->assertSee('Магазин улучшений')
            ->assertSee('Карточка компании')
            ->assertSee('Права доступа')
            ->assertSee('Документация')
        ;
    }

    /**
     * Get the element shortcuts for the component.
     *
     * @return array
     */
    public function elements()
    {
        return [

        ];
    }
}
