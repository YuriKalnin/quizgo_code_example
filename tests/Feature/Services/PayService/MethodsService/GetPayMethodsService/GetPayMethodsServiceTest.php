<?php

namespace Tests\Feature\Services\PayService\MethodsService\GetPayMethodsService;

use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * @group rate
 * */
class GetPayMethodsServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function tedstGetFail404()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        $response = $this->actingAs($fakeData->user)->get("/service/pay/methods/get");

        $response->assertStatus(404);
    }

    public function testGetOk()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        $response = $this->actingAs($fakeData->user)->get("/service/pay/methods/get?company={$fakeData->company->id}");

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'methods' => [
                [
                    'values' => [
                        [
                            'value',
                            'text',
                        ],
                        [
                            'value',
                            'text',
                        ],
                    ],
                ]
            ]
        ]);

        $response->assertJson([
            'lastPayMethod' => 'card',
            'message' => 'OK',
            'status' => true,
        ]);
    }
}
