<?php

namespace Tests\Feature\Services\PayService\MethodsService\BillService\CreteBillService;

use App\Services\DevtoolsService\TestService\FakeRequest\Requests\CreateBillRequest;
use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use RateService;
use BillService;

/**
 * Тестирует оплату счета
 * @group rate
 * */
class PayBillServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Создание счета на оплату
     * @return void
     */
    public function testBillRateLeadMagnet()
    {
        // создали счет на оплату
        $result = DevtoolsService::tests()->request(new CreateBillRequest($this));

        // Поменяем тариф на лид мганит
        RateService::change($result->fakeData()->company, 'lead');

        // обновим фейковые данные
        $result->fakeData()->reloadCompany();

        // проверка то что у компании тариф лид магнит
        $this->assertTrue($result->fakeData()->account->isLeadMagnet());

        // получим счета на оплату компании
        $bills = $result->fakeData()->company->bills;
        $bill = $bills->first();

        $result->fakeData()->setBill($bill);

        // проверка на то что у компании есть счета
        $this->assertTrue($bills->count() > 0);

        // проверка на то что первый счет компании не оплачен
        $this->assertTrue(!$bill->payed);

        // создадим фейковые лиды
        $result->fakeData()->createLeads();

        BillService::payBill($bills->first());

        $result->fakeData()
            ->reloadBill()
            ->reloadCompany();

        // проверка на то счет компании оплачен
        $this->assertTrue($bill->payed === 1);

        // счет выставлялся на 5000, было куплено 400 заявок должно остаться 1400
        $this->assertTrue($result->fakeData()->account->balance == 1400);

        // у компании на счету должно быть 400 лидов, и 10 заблокированных лидов
        $this->assertTrue($result->fakeData()->account->lead_limit == 390);

        // в журнале о начилсении лидов должна быть запись о начислении 400 лидов
        $this->assertTrue(
    $result
                ->fakeData()
                ->account
                ->leadMagnetJournal()
                ->where('count', 400)
                ->where('name', 'Покупка лидов')
                ->get()
                ->count() > 0
        );

        // в журнале о транзакциях баланса компании должна быть запись о начислении 5000₽
        $this->assertTrue(
            $result
                ->fakeData()
                ->account
                ->quizPlusTransactions()
                ->where('cost', 5000)
                ->where('name', 'Оплата по счету')
                ->get()
                ->count() > 0
        );
    }

    public function tedstBillRateQuizPlus()
    {
        // создали счет на оплату
        $result = DevtoolsService::tests()->request(new CreateBillRequest($this));

        // проверка то что у компании тариф лид магнит
        $this->assertTrue($result->fakeData()->account->isQuizPlus());

        // получим счета на оплату компании
        $bills = $result->fakeData()->company->bills;
        $bill = $bills->first();

        $result->fakeData()->setBill($bill);

        // проверка на то что у компании есть счета
        $this->assertTrue($bills->count() > 0);

        // проверка на то что первый счет компании не оплачен
        $this->assertTrue(!$bill->payed);

        BillService::payBill($bills->first());

        $result->fakeData()
            ->reloadBill()
            ->reloadCompany();

        // проверка на то счет компании оплачен
        $this->assertTrue($bill->payed === 1);

        // счет выставлялся на 5000, у компании демо период должно быть на счету 5К
        $this->assertTrue($result->fakeData()->account->balance == 5000);

        // в журнале о транзакциях баланса компании должна быть запись о начислении 5000₽
        $this->assertTrue(
            $result
                ->fakeData()
                ->account
                ->quizPlusTransactions()
                ->where('cost', 5000)
                ->where('name', 'Оплата по счету')
                ->get()
                ->count() > 0
        );
    }
}
