<?php

namespace Tests\Feature\Services\PayService\MethodsService\BillService\CreteBillService;

use App\Services\DevtoolsService\TestService\FakeRequest\Requests\CreateBillRequest;
use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use RateService;
use BillService;

/**
 * После оплты счета должны были разблокироваться лиды у компании тестируем это
 * @group rate
 * */
class AutoUnableLeadsAfterPayBillTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * При оплате счета на тарифе лид магнит
     * @return void
     */
    public function testInLeadMagnet()
    {
        $this->checkAutoUnableLeadsAfterPayBill('lead');
    }

    public function testInLeadQuizPlus()
    {
        $this->checkAutoUnableLeadsAfterPayBill('quiz');
    }

    public function checkAutoUnableLeadsAfterPayBill($rate)
    {
        // создали счет на оплату
        $result = DevtoolsService::tests()->request(new CreateBillRequest($this));

        // Поменяем тариф на лид мганит
        RateService::change($result->fakeData()->company, $rate);

        // обновим фейковые данные
        $result->fakeData()->reloadCompany();

        // получим счета на оплату компании
        $bills = $result->fakeData()->company->bills;
        $bill = $bills->first();

        $result
            ->fakeData()
            ->setBill($bill)
            ->createLeads();

        // проверим что у компании есть заблокированные лиды
        $this->assertTrue(
            $result
                ->fakeData()
                ->company
                ->lead()
                ->where('enable', 0)
                ->get()
                ->count()
            > 0
        );

        BillService::payBill($bills->first());

        $result->fakeData()
            ->reloadBill()
            ->reloadCompany();

        // проверим что у компании нет заблокированные лиды
        $this->assertTrue(
            $result
                ->fakeData()
                ->company
                ->lead()
                ->where('enable', 0)
                ->get()
                ->count()
            < 1
        );
    }
}
