<?php

namespace Tests\Feature\Services\PayService\MethodsService\BillService\CreteBillService;

use App\Services\DevtoolsService\TestService\FakeRequest\Requests\CreateBillRequest;
use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Тестируем сохранение последнего способа оплаты после выставления счета
 * @group rate
 * */
class SaveLastPayMethodTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Когда выставили счет система должна запомнить последний способ оплаты
     * @return void
     */
    public function testAfterBillCreate()
    {
        $result = DevtoolsService::tests()->request(new CreateBillRequest($this));

        $response = $this->actingAs($result->fakeData()->user)->get("/service/pay/methods/get?company={$result->fakeData()->company->id}");

        $response->assertStatus(200);

        $counterAgents = $result->fakeData()->user->counterAgents->last()->id;

        $response->assertJson([
            'lastPayMethod' => "counter_agent_{$counterAgents}",
            'message' => 'OK',
            'status' => true,
        ]);
    }
}
