<?php

namespace Tests\Feature\Services\PayService\MethodsService\BillService\CreteBillService;

use App\Services\DevtoolsService\TestService\FakeRequest\Requests\CreateBillRequest;
use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Тестирует создание счета на оплату
 * @group rate
 * */
class CreateBillServiceTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Если не пердаем ид компании то получим ошибку 404
     * @return void
     */
    public function testCreateFail404()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        $response = $this->actingAs($fakeData->user)->post("/service/pay/methods/bill/create");

        $response->assertStatus(404);
    }

    /** Если не передаем данные пост запросом получаем ошибку валидации
     * @return void
     */
    public function testCreateFailInValid()
    {
        $fakeData = DevtoolsService::tests()->fakeData()->createCompany();

        $response = $this->actingAs($fakeData->user)->post("/service/pay/methods/bill/create", [
            'company' => $fakeData->company->id
        ]);

        $response->assertStatus(422);

        $response->assertJson([
            'errors' => [
                'full_name' => [
                    'Поле полное наименование организации обязательно для заполнения'
                ],
                'inn' => [
                    'Поле ИНН обязательно для заполнения'
                ],
                'address' => [
                    'Поле адрес обязательно для заполнения'
                ],
            ]
        ]);
    }

    /**
     * Успешное создание счета на оплату
     * @return void
     */
    public function testCreateOk()
    {
        $result = DevtoolsService::tests()->request(new CreateBillRequest($this));

        $result->response()->assertStatus(200);

        $result->response()->assertJson([
            'status' => true,
            'message' => 'OK',
        ]);

        $result->response()->assertJsonStructure([
            'link',
            'status',
            'message',
        ]);

        $url = $result->response()->decodeResponseJson()['link'];

        $this->assertTrue(filter_var($url, FILTER_VALIDATE_URL) !== false);
    }
}
