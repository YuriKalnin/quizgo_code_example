<?php

namespace Tests\Feature\Services\MarketService\Actions;

use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Services\DevtoolsService\TestService\FakeRequest\Requests\Market\GetQuizesOnProductsByIDRequest;
use App\Services\DevtoolsService\TestService\FakeRequest\Requests\Market\BindProductByCompanyRequest;

/**
 * Проверяем доступность квиз
 * @group rate
 * */
class GetQuizesOnProductsByIDTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetQuizForProducts()
    {
        $result = DevtoolsService::tests()->request(new GetQuizesOnProductsByIDRequest($this));

        $result
            ->response()
            ->assertStatus(200)
            ->assertJson([
                'status' => 1,
                'collection' => [[
                    'id' => $result->fakeData()->project->id,
                    'company_id' => $result->fakeData()->company->id,
                    'author_id' => $result->fakeData()->user->id,
                ]],
                'bindsProducts' => [
                    'collection' => [],
                ],
            ])
            ->assertJsonStructure([
                'status',
                'collection' => [
                    '*' => [
                        'id',
                        'company_id',
                        'author_id',
                        'name',
                        'created_at',
                        'updated_at',
                        'deleted_at',
                        'quiz' => [
                            '*' => [
                                'id',
                                'name',
                                'quiz_json',
                                'project_id',
                                'author_id',
                                'created_at',
                                'updated_at',
                                'template_category_id',
                                'white_label',
                                'deleted_at',
                                'blocked',
                                'blocked_text',
                                'css_theme',
                                'css_theme',
                            ]
                        ],
                    ]
                ],
                'bindsProducts' => [
                    'collection' => [
                        '*' => [
                            '*' => [
                                'id',
                                'quiz_id',
                                'project_id',
                                'company_id',
                                'product_id',
                                'price',
                                'active_to',
                                'created_at',
                                'updated_at',
                                'active_to_format',
                                'active_to_days_format',
                                'active_to_days_word',
                            ]
                        ]
                    ],
                    'daysOfMonth'
                ]
            ]);
    }

    public function testBindProductByCompanyRequest()
    {
        $result = DevtoolsService::tests()->request(new BindProductByCompanyRequest($this));

        $result
            ->response()
            ->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'messages',
                'data' => [
                    'bindProductsCollections' => [
                        '*' => [
                            'active_to',
                            'active_to_days_format',
                            'active_to_days_word',
                            'active_to_format',
                            'company_id',
                            'created_at',
                            'id',
                            'price',
                            'product_id',
                            'project_id',
                            'quiz_id',
                            'updated_at',
                        ]
                    ],
                    'bindProductsCollectionsDeleted' => [
                        'message',
                        'status',
                    ],
                    'companyBilling' => [
                        'balance',
                        'billing',
                        'email',
                    ],
                    'companyCollect' => [
                        'account' => [
                            'active_date',
                            'active_date_clips',
                            'auto_payment',
                            'balance',
                            'billing',
                            'billing_id',
                            'check_count_leads',
                            'company_id',
                            'created_at',
                            'deleted_at',
                            'demo_date',
                            'id',
                            'is_enable',
                            'is_lead_magnet',
                            'last_pay_method',
                            'lead_limit',
                            'lead_magnet_limit_format',
                            'leads_active_date',
                            'quiz_limit',
                            'quiz_prices',
                            'rate',
                            'rate_enable_string',
                            'rate_name',
                            'updated_at',
                        ],
                        'amocrm_contact_id',
                        'amocrm_customer_id',
                        'amocrm_lead_id',
                        'amocrm_lead_status',
                        'contact_name',
                        'contact_phone',
                        'created_at',
                        'deleted_at',
                        'file_id',
                        'id',
                        'name',
                        'partner_id',
                        'site_url',
                        'updated_at',
                        'utm_id',
                    ],
                    'payedProduct',
                    'productCollect' => [
                        'count_installs',
                        'count_installs_word',
                        'created_at',
                        'detail_image',
                        'detail_text',
                        'id',
                        'name',
                        'preview_image',
                        'preview_text',
                        'price',
                        'price_sale',
                        'sale',
                        'sale_type',
                        'show',
                        'updated_at',
                        'video_caption',
                        'video_link',
                    ],
                    'subscriptionService',
                    'totalCount',
                    'totalPrice',
                    'userCollect' => [
                        'amo_contact_id',
                        'blocked',
                        'confirm_phone',
                        'created_at',
                        'date_sync_amo',
                        'deleted_at',
                        'email',
                        'email_verified_at',
                        'facebook_user_id',
                        'file_id',
                        'google_id',
                        'id',
                        'last_login',
                        'lead_amo_id',
                        'locale',
                        'name',
                        'phone',
                        'sms_code',
                        'status_amo',
                        'updated_at',
                        'use_product',
                        'utm_campaign',
                        'utm_content',
                        'utm_http_referer',
                        'utm_http_target_referer',
                        'utm_id',
                        'utm_medium',
                        'utm_source',
                        'utm_term',
                        'vk_user_id',
                        'yandex_user_id',
                    ],
                ],
            ]);
    }
}
