<?php

namespace Tests\Feature\Services\AmoCrm3Service;

use AmoCrm3Service;
use Tests\TestCase;
use App\Models\Quiz;

class RefreshAccessTokenByRefreshTokenTest extends TestCase
{
    public function testRefresh() {

        if (env('SERVER_VM') !== 'yuri.quizgo-dev.ru') {
            $this->assertTrue(true);
            return true;
        }

        $quiz = Quiz::find(111);

        if (
            isset($quiz->amoCrm3->tokens->access_token)
            && $quiz->amoCrm3->tokens->access_token
            && $quiz->amoCrm3->tokens->refresh_token)
        {
            $nowToken = $quiz->amoCrm3->tokens->access_token;

            $quiz->amoCrm3->tokens->access_token = now()->subDays(7);
            $quiz->amoCrm3->tokens->save();

            $refreshService = AmoCrm3Service::http()->refreshAccessToken($quiz->amoCrm3->tokens);

            $this->assertTrue($nowToken !== $refreshService->access_token);
            $this->assertTrue($refreshService->expires->isFuture());
        }

        $this->assertTrue($quiz->amoCrm3->id > 0);
    }
}
