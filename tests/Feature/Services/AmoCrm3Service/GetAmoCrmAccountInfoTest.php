<?php

namespace Tests\Feature\Services\AmoCrm3Service;

use Tests\TestCase;
use App\Models\Quiz;
use App\Models\User;

/**
 * Тестирует получение данных для настроек интеграции
 * Получение данных аккаунта амо, связей полей и т.д
 */
class GetAmoCrmAccountInfoTest extends TestCase
{
    public function testGetAccount() {

        if (env('SERVER_VM') !== 'yuri.quizgo-dev.ru') {
            $this->assertTrue(true);
            return true;
        }

        $quiz = Quiz::find(111);
        $user = User::find(16);

        if (
            isset($quiz->amoCrm3->tokens->access_token)
            && $quiz->amoCrm3->tokens->access_token
            && $quiz->amoCrm3->tokens->refresh_token)
        {
            $response = $this->actingAs($user)->json('GET', '/service/amo/get-account?quiz_id=111');
            $response->assertStatus(200)
                    ->assertJson([
                        "status" => 1,
                        "message" => "",
                    ])
                    ->assertJsonStructure([
                        "status",
                        "message",
                        "integration" => [
                            'id',
                            'quiz_id',
                            'user_id',
                            'enable',
                            'pipeline_id',
                            'status_id',
                            'responsible_user_id',
                            'name_deal',
                            'tags',
                            'expires',
                            'base_domain',
                            'field_relations',
                            'created_at',
                            'updated_at',
                            'field_relations_json' => [
                                [
                                    'quizgo' => [
                                        'value'
                                    ],
                                    'amocrm' => [
                                        'value'
                                    ]
                                ]
                            ],
                            'tokens' => [
                                'id',
                                'base_domain',
                                'expires',
                                'created_at',
                                'updated_at'
                            ]
                        ],
                        'users' => [[
                            'id',
                            'name',
                            'email',
                        ]],
                        'pipelines' => [
                            [
                                'id',
                                'value',
                                'label',
                                'name',
                                'statuses' => [[
                                    'id',
                                    'name',
                                    'pipeline_id',
                                    'sort',
                                    'color',
                                    'editable',
                                    'type'
                                ]]
                            ]
                        ],
                        'fieldRelationsList' => [
                            'quizgo' => [[
                                'text',
                                'value',
                                'data'
                            ]],
                            'amocrm' => [[
                                'text',
                                'value',
                            ]]
                        ]
                    ]);
        } else {
            $this->assertTrue(true);
        }
    }
}
