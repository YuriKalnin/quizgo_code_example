<?php

namespace Tests\Feature\Services\PartnerRewardService\makeRewardFromCompany;

use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Проверяем начисление вознаграждения партнеру
 * @group rate
 * */
class MakeRewardFromCompanyTest extends TestCase
{
    use DatabaseTransactions;

    public function testRateQuizPlusDemo()
    {
        $fakeData = DevtoolsService::tests()
                        ->fakeData()
                        ->createQuizPlusTransaction('out');

        $makeRewardFromCompany = \PartnerRewardService::makeRewardFromCompany(
            $fakeData->quizPlusTransaction
        );

        $costRateNalog = $fakeData->quizPlusTransaction->cost - (($fakeData->quizPlusTransaction->cost * 9.7) / 100);
        $costRateSystem = $fakeData->quizPlusTransaction->cost * 2.7 / 100;

        $this->assertTrue((bool) $makeRewardFromCompany->status);
        $this->assertTrue($makeRewardFromCompany->reward->company_id === $fakeData->company->id);
        $this->assertTrue($makeRewardFromCompany->reward->transaction_id === 0);
        $this->assertTrue($makeRewardFromCompany->reward->company_transaction_id === $fakeData->quizPlusTransaction->id);
        $this->assertTrue($makeRewardFromCompany->reward->partner_id === $fakeData->partner->id);
        $this->assertTrue($makeRewardFromCompany->reward->cost === $fakeData->quizPlusTransaction->cost);
        $this->assertTrue($makeRewardFromCompany->reward->cost_rate_system === $costRateSystem);
        $this->assertTrue($makeRewardFromCompany->reward->cost_rate_nalog === $costRateNalog);
        $this->assertTrue($makeRewardFromCompany->reward->cost_payed === $costRateNalog * 30 / 100);
    }
}
