<?php

namespace Tests\Feature\Services\RateService;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * @group rate
 * */
class GetRateServiceTest extends TestCase
{
    use DatabaseTransactions;

    public function testGetRate()
    {
        $user = \App\Models\User::find(16);

        $response = $this->actingAs($user)->get("/service/rate?company=16");

        $response->assertStatus(200)->assertJsonStructure([
            'accountCollect' => [
                'id',
                'company_id',
                'active_date',
                'lead_limit',
                'quiz_limit',
                'rate',
                'demo_date',
                'created_at',
                'updated_at',
                'deleted_at',
                'billing_id',
                'active_date_clips',
                'balance',
                'quiz_prices',
                'leads_active_date',
                'rate_name',
                'is_enable',
                'rate_enable_string'
            ]
        ]);
    }

    public function testChangeRate()
    {
        $user = \App\Models\User::find(16);
        $company = \App\Models\Company::find(16);

        $response = $this->actingAs($user)->post( "/service/rate/change", [
            'company' => 16,
            'rate' => $company->account->is_quiz_plus ? 'lead' : 'quiz'
        ]);

        $response->assertStatus(200)->assertJsonStructure([
            'accountCollect' => [
                'id',
                'company_id',
                'active_date',
                'lead_limit',
                'quiz_limit',
                'rate',
                'demo_date',
                'created_at',
                'updated_at',
                'deleted_at',
                'billing_id',
                'active_date_clips',
                'balance',
                'quiz_prices',
                'leads_active_date',
                'rate_name',
                'is_enable',
                'rate_enable_string'
            ]
        ])->assertJson([
            'status' => true
        ]);
    }
}
