<?php

namespace Tests\Feature\Services\RateService;

use App\Models\Lead;
use App\Services\DevtoolsService\TestService\FakeRequest\Requests\SendLeadRequest;
use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use Tests\TestCase;
use function now;

/**
 * Проверяем доступность лида
 * после отправки на тарифе лид магнит
 * на старом тарифе со счетом пользоваетеля
 * @group rate
 * */
class CheckEnableLeadsRateLeadMagnetOldRateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Новый лид на тарифе лидмаггнит
     * в демо периоде
     * @return void
     */
    public function testDemo()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            // перейдем на тариф лид магнит
            $request->fakeData()->account->rate = 'lead';

            // спишем дату активности у компании
            $request->fakeData()->account->active_date = now()->subMonth();

            // поставим демо период
            $request->fakeData()->account->demo_date = now()->addMonth();

            // привяжем счет пользователя к аккаунту
            $request->fakeData()->account->billing_id = $request->fakeData()->user->billing->id;

            // накинем лидов на счет
            $request->fakeData()->account->lead_limit = 0;

            $request->fakeData()->account->save();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        // у компании был один лид на счету
        $this->assertLead($result, 1);

    }

    /**
     * Новый лид на тарифе лидмаггнит
     * остался один лид
     * @return void
     */
    public function testOneLead()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            // перейдем на тариф лид магнит
            $request->fakeData()->account->rate = 'lead';

            // спишем дату активности у компании
            $request->fakeData()->account->active_date = now()->subMonth();

            // поставим демо период
            $request->fakeData()->account->demo_date = now()->subMonth();

            // привяжем счет пользователя к аккаунту
            $request->fakeData()->account->billing_id = $request->fakeData()->user->billing->id;

            // накинем лидов на счет
            $request->fakeData()->account->lead_limit = 1;

            $request->fakeData()->account->save();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        // у компании был один лид на счету
        $this->assertLead($result, 1);
    }

    /**
     * Новый лид на тарифе лидмаггнит
     * не осталось лидов
     * @return void
     */
    public function testNotLead()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            // перейдем на тариф лид магнит
            $request->fakeData()->account->rate = 'lead';

            // спишем дату активности у компании
            $request->fakeData()->account->active_date = now()->subMonth();

            // поставим демо период
            $request->fakeData()->account->demo_date = now()->subMonth();

            // привяжем счет пользователя к аккаунту
            $request->fakeData()->account->billing_id = $request->fakeData()->user->billing->id;

            // накинем лидов на счет
            $request->fakeData()->account->lead_limit = 0;

            $request->fakeData()->account->save();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        // у компании был один лид на счету
        $this->assertLead($result, 0);

    }

    public function assertCreateLead($result) {

        $result->response()
            ->assertStatus(200)
            ->assertJsonStructure([
                'lead' => [
                    'lead' => [
                        'id'
                    ]
                ],
                'status',
                'response',
            ])->assertJson([
                'status' => 1
            ]);
    }

    public function assertLead($result, $enable) {
        $this->assertTrue(
            Lead::where('id', $result->getLeadID())
                ->where('enable', $enable)
                ->get()
                ->count()
                > 0
        );

        $this->assertTrue($result->fakeData()->account->lead_limit > $result->fakeData()->reloadCompany()->account->lead_limit);
    }
}
