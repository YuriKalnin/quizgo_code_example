<?php

namespace Tests\Feature\Services\RateService;

use App\Models\Lead;
use App\Services\DevtoolsService\TestService\FakeRequest\Requests\SendLeadRequest;
use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use Tests\TestCase;
use function now;

/**
 * Проверяем доступность лида
 * после отправки на тарифе лид магнит
 * @group rate
 * */
class CheckEnableLeadsRateLeadMagnetTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Новый лид на тарифе лидмаггнит
     * в демо периоде
     * @return void
     */
    public function testDemo()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            // перейдем на тариф лид магнит
            RateService::change($request->fakeData()->company, 'lead');

            // спишем дату активности у компании
            RateService::activeDate()->update($request->fakeData()->company, -10);

            // проверим что баланс у компании меньше одного рубля
            $this->assertTrue($request->fakeData()->account->balance < 1);

            // сделаем баланс отрицательным
            $rateService = RateService::balance()->spendMoney($request->fakeData()->company, 'тест', 100);
            $this->assertTrue($rateService->isOk());

            // поставим демо период
            $request->fakeData()->account->demo_date = now()->addMonth();
            $request->fakeData()->account->save();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        // у компании был один лид на счету
        $this->assertLead($result, 1);

    }

    /**
     * Новый лид на тарифе лидмаггнит остался один лид
     * @return void
     */
    public function testOneLead()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            // обнулим демо период у компании
            RateService::base()->resetDemo($request->fakeData()->company->account);

            // перейдем на тариф лид магнит
            RateService::change($request->fakeData()->company, 'lead');

            // накинем один лид на компанию
            RateService::leadMagnet()->limit()->update($request->fakeData()->company, 1);
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        // у компании был один лид на счету
        $this->assertLead($result, 1);

    }

    /**
     * Новый лид на тарифе лидмаггнит
     * не осталось лидов
     * @return void
     */
    public function testNotLead()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            // обнулим демо период у компании
            RateService::base()->resetDemo($request->fakeData()->company->account);

            // перейдем на тариф лид магнит
            RateService::change($request->fakeData()->company, 'lead');

            // спишем у компании лидов
            RateService::leadMagnet()->limit()->update($request->fakeData()->company, -10);

        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        // у компании был один лид на счету
        $this->assertLead($result, 0);

    }

    public function assertCreateLead($result) {

        $result->response()
            ->assertStatus(200)
            ->assertJsonStructure([
                'lead' => [
                    'lead' => [
                        'id'
                    ]
                ],
                'status',
                'response',
            ])->assertJson([
                'status' => 1
            ]);
    }

    public function assertLead($result, $enable) {
        $this->assertTrue(
            Lead::where('id', $result->getLeadID())
                ->where('enable', $enable)
                ->get()
                ->count()
                > 0
        );

        if ($result->fakeData()->account->demo_enable) {
            $this->assertTrue($result->fakeData()->account->lead_limit === $result->fakeData()->reloadCompany()->account->lead_limit);
        } else {
            $this->assertTrue($result->fakeData()->account->lead_limit > $result->fakeData()->reloadCompany()->account->lead_limit);
        }
    }
}
