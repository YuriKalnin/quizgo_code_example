<?php

namespace Tests\Feature\Services\RateService;

use App\Models\Lead;
use App\Services\DevtoolsService\TestService\FakeRequest\Requests\SendLeadRequest;
use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use Tests\TestCase;
use function now;

/**
 * Проверяем доступность лида
 * после отправки на тарифе квиз плюс
 * @group rate
 * */
class CheckEnableLeadsRateQuizPlusTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Новый лид в демо периоде
     * на тирфе квиз+
     * @return void
     */
    public function testDemoQuizPlus()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            // спишем дату активности у компании
            RateService::activeDate()->update($request->fakeData()->company, -10);

            // проверим что баланс у компании меньше одного рубля
            $this->assertTrue($request->fakeData()->account->balance < 1);

            // сделаем баланс отрицательным
            $rateService = RateService::balance()->spendMoney($request->fakeData()->company, 'тест', 100);
            $this->assertTrue($rateService->isOk());

            // поставим демо период
            $request->fakeData()->account->demo_date = now()->addMonth();
            $request->fakeData()->account->save();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        $this->assertLead($result, 1);
    }

    /**
     * Новый лид на тарифе квиз+
     * С доступной датой активности,
     * отрицательным балансом и не доступным
     * демо периодом
     * @return void
     */
    public function testRateQuizPlusActiveDateEnable()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            // сделаем дату активности доступной
            RateService::activeDate()->update($request->fakeData()->company, 10);

            // обнулим демо период у компании
            RateService::base()->resetDemo($request->fakeData()->company->account);

            // проверим что баланс у компании меньше одного рубля
            $this->assertTrue($request->fakeData()->account->balance < 1);

            // сделаем баланс отрицательным
            $rateService = RateService::balance()->spendMoney($request->fakeData()->company, 'тест', 100);
            $this->assertTrue($rateService->isOk());
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        $this->assertLead($result, 1);
    }

    /**
     * Новый лид на тарифе квиз+
     * С положительным балансом,
     * не доступным демо периодом,
     * не доступной датой активности
     * @return void
     */
    public function testRateQuizPlusBalanceEnable()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            // сделаем дату активности не доступной
            RateService::activeDate()->update($request->fakeData()->company, -10);

            // обнулим демо период у компании
            RateService::base()->resetDemo($request->fakeData()->company->account);

            // сделаем баланс положительным
            $request->fakeData()->account->balance = 1;
            $request->fakeData()->account->save();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        $this->assertLead($result, 1);
    }

    /**
     * Новый лид на недоступном тарифе квиз+
     * С недоступным демо периодом,
     * недоступной датой активности,
     * отрицательным балансом
     * @return void
     */
    public function testRateQuizPlusEnd()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            // спишем дату активности у компании
            RateService::activeDate()->update($request->fakeData()->company, -10);

            // обнулим демо период у компании
            RateService::base()->resetDemo($request->fakeData()->company->account);

            // проверим что баланс у компании меньше одного рубля
            $this->assertTrue($request->fakeData()->account->balance < 1);

            // сделаем баланс отрицательным
            $rateService = RateService::balance()->spendMoney($request->fakeData()->company, 'тест', 100);
            $this->assertTrue($rateService->isOk());
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        $this->assertLead($result, 0);
    }

    public function assertCreateLead($result) {

        $result->response()
            ->assertStatus(200)
            ->assertJsonStructure([
                'lead' => [
                    'lead' => [
                        'id'
                    ]
                ],
                'status',
                'response',
            ])->assertJson([
                'status' => 1
            ]);
    }

    public function assertLead($result, $enable) {
        $this->assertTrue(
            Lead::where('id', $result->getLeadID())
                ->where('enable', $enable)
                ->get()
                ->count()
                > 0
        );

        $this->assertTrue($result->fakeData()->account->lead_limit === $result->fakeData()->reloadCompany()->account->lead_limit);
    }
}
