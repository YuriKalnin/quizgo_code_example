<?php

namespace Tests\Feature\Services\RateService;

use App\Models\Lead;
use App\Services\DevtoolsService\TestService\FakeRequest\Requests\SendLeadRequest;
use DevtoolsService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use RateService;
use Tests\TestCase;
use function now;

/**
 * Проверяем доступность лида
 * после отправки на тарифе квиз плюс
 * Старой версии со счетом пользователя
 * @group rate
 * */
class CheckEnableLeadsRateQuizPlusOldRatesTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Новый лид в демо периоде
     * на тирфе квиз+
     * @return void
     */
    public function testDemoQuizPlusOldRate()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            $account = $request->fakeData()->account;

            // поставим демо период
            $account->demo_date = now()->addMonth();

            // спишем дату активности у компании
            $account->active_date = now()->subMonth();

            // привяжем счет пользователя к аккаунту
            $account->billing_id = $request->fakeData()->user->billing->id;

            $account->save();

            $request->fakeData()->reloadCompany();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        $this->assertLead($result, 1);
    }

    /**
     * Новый лид на тарифе квиз+
     * С доступной датой активности,
     * отрицательным балансом и не доступным
     * демо периодом
     * @return void
     */
    public function testRateQuizPlusActiveDateEnableOldRate()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            $request->fakeData()->account->demo_date = now()->subMonth();
            $request->fakeData()->account->active_date = now()->addMonth();
            $request->fakeData()->account->billing_id = $request->fakeData()->user->billing->id;
            $request->fakeData()->account->save();

            $request->fakeData()->billing->balance = -100;
            $request->fakeData()->billing->save();

            $request->fakeData()->reloadCompany();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        $this->assertLead($result, 1);
    }

    /**
     * Новый лид на тарифе квиз+
     * С положительным балансом,
     * не доступным демо периодом,
     * не доступной датой активности
     * лид должен быть не доступен,
     * пох что на балансе есть деньги
     * @return void
     */
    public function testRateQuizPlusBalanceEnableOldRate()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            $request->fakeData()->account->demo_date = now()->subMonth();
            $request->fakeData()->account->active_date = now()->subMonth();
            $request->fakeData()->account->billing_id = $request->fakeData()->user->billing->id;
            $request->fakeData()->account->save();

            $request->fakeData()->reloadCompany();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        $this->assertLead($result, 0);
    }

    /**
     * Новый лид на недоступном тарифе квиз+
     * С недоступным демо периодом,
     * недоступной датой активности,
     * отрицательным балансом
     * @return void
     */
    public function testRateQuizPlusEndOldRate()
    {
        $request = new SendLeadRequest($this);

        $request->onBefore(function (SendLeadRequest $request) {

            $request->fakeData()->account->demo_date = now()->subMonth();
            $request->fakeData()->account->active_date = now()->subMonth();
            $request->fakeData()->account->billing_id = $request->fakeData()->user->billing->id;
            $request->fakeData()->account->save();

            $request->fakeData()->billing->balance = -100;
            $request->fakeData()->billing->save();

            $request->fakeData()->reloadCompany();
        });

        $result = DevtoolsService::tests()->request($request);

        $this->assertCreateLead($result);

        $this->assertLead($result, 0);
    }

    public function assertCreateLead($result) {

        $result->response()
            ->assertStatus(200)
            ->assertJsonStructure([
                'lead' => [
                    'lead' => [
                        'id'
                    ]
                ],
                'status',
                'response',
            ])->assertJson([
                'status' => 1
            ]);
    }

    public function assertLead($result, $enable) {
        $this->assertTrue(
            Lead::where('id', $result->getLeadID())
                ->where('enable', $enable)
                ->get()
                ->count()
                > 0
        );

        $this->assertTrue($result->fakeData()->account->lead_limit === $result->fakeData()->reloadCompany()->account->lead_limit);
    }
}
