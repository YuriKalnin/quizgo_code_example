FROM ubuntu:20.04

ARG user
ARG uid
ARG queueConfPath

ARG DEBIAN_FRONTEND=noninteractive

# Install system dependencies
RUN apt-get update && apt-get install -y \
    nginx \
    php7.4-fpm \
    php7.4 \
    git \
    curl \
    mc \
    zip \
    unzip \
    vim \
    locales \
    redis-server \

    net-tools \
    htop \
    nano \
    tree \
    cron \
    dnsutils \
    supervisor \
    wget \
    php7.4-mysql \
    php7.4-cli \
    php7.4-xml \
    php7.4-curl \
    php7.4-mbstring \
    php7.4-gd \
    php7.4-zip \
    php7.4-intl \

    build-essential \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    libzip-dev \
    libfreetype6-dev \
    jpegoptim optipng pngquant gifsicle && \
    apt clean

RUN useradd -G www-data,root -u $uid -d /home/$user -s /bin/bash $user
RUN usermod -aG root,sudo,quizgo www-data
RUN usermod -aG quizgo root
RUN mkdir /home/$user && \
    mkdir /home/$user/.composer && \
    chown -R $user:$user /home/$user && \
    mkdir /root/.composer

RUN echo "root:123" | chpasswd && echo "$user:123" | chpasswd

RUN curl -sS https://getcomposer.org/installer -o /home/quizgo/composer-setup.php && \
    php /home/quizgo/composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
    export COMPOSER_ALLOW_SUPERUSER=1

RUN curl -sL https://deb.nodesource.com/setup_14.x -o /home/quizgo/nodesource_setup.sh && \
    bash /home/quizgo/nodesource_setup.sh && \
    apt-get install -y nodejs && \
    apt clean

RUN mkdir /home/$user/.mysql && \
    wget "https://storage.yandexcloud.net/cloud-certs/CA.pem" -O /home/$user/.mysql/root.crt && \
    chmod 0600 /home/$user/.mysql/root.crt && \
    chown $user:$user /home/$user/.mysql/root.crt

COPY /docker/nginx/snippets/blockips.conf /etc/nginx/snippets/blockips.conf
COPY /docker/nginx/snippets/quizgo.conf /etc/nginx/snippets/quizgo.conf
COPY /docker/nginx/nginx_4_proc.conf /etc/nginx/nginx.conf
COPY /docker/php-fpm/pool.d/www.conf /etc/php/7.4/fpm/pool.d/
COPY /docker/php-fpm/php.ini /etc/php/7.4/fpm/

COPY /docker/user/quizgo/.bashrc /home/quizgo/
RUN chmod 744 /home/quizgo/.bashrc && chown $user:$user /home/quizgo/.bashrc

COPY /docker/user/quizgo/.profile /home/quizgo/
RUN chmod 744 /home/quizgo/.profile && chown $user:$user /home/quizgo/.profile

COPY /docker/git/.gitconfig /home/quizgo/
RUN chmod 644 /home/quizgo/.gitconfig && chown $user:$user /home/quizgo/.gitconfig

COPY /docker/cron/crontab /etc/crontab
COPY /docker/supervisor/supervisord.conf /etc/supervisor/supervisord.conf
COPY /docker/supervisor/$queueConfPath /etc/supervisor/conf.d
COPY /docker/redis/redis.conf /etc/redis/redis.conf
RUN chmod 644 /etc/redis/redis.conf

COPY /docker/init.sh /home/quizgo/
RUN chmod 774 /home/quizgo/init.sh

RUN wget https://storage.dbaas.selcloud.ru/CA.pem -O /home/quizgo/.mysql/root-slct.crt && \
    chmod 0600 /home/$user/.mysql/root-slct.crt && \
    chown $user:$user /home/$user/.mysql/root-slct.crt

# configure Fail2ban
RUN apt-get install -y fail2ban
COPY /docker/fail2ban/jail.local /etc/fail2ban/jail.local

# configure sudo
RUN apt-get update && apt-get install -y sudo
RUN adduser quizgo sudo

# configure iptables
RUN apt-get install -y iptables

# configure timizone
RUN apt-get install -yq tzdata && \
    ln -fs /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata

# configure laravel echo server
RUN npm install -g laravel-echo-server
